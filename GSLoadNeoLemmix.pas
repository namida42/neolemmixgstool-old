unit GSLoadNeoLemmix;

// Loads a NeoLemmix graphic set into the editor's internal format. To use,
// create a TNeoLemmixGraphicSet object, then call:
//
// <TBaseGraphicSet> := <TNeoLemmixGraphicSet>.LoadGraphicSet(<file>);
//
// The TBaseGraphicSet object is created during TNeoLemmixGraphicSet.LoadGraphicSet, so you should
// not use <TBaseGraphicSet> := TBaseGraphicSet.Create; beforehand.
//
// Once TNeoLemmixGraphicSet.LoadGraphicSet has been allowed to run, there is no need to keep the
// TNeoLemmixGraphicSet object around, it can be freed.

// Saving NeoLemmix graphic sets is very similar, but use this instead:
//
// <TNeoLemmixGraphicSet>.SaveGraphicSet(<file>, <TBaseGraphicSet>);

// NeoLemmix-format graphic sets always use 24-bit + 8-bit alpha images. Current versions of
// NeoLemmix do not support alpha values other than 0 or 255 (anything non-zero will be treated
// as 255), but nonetheless this unit can save and load them with values inbetween.

// Proper specification of this format requires that the header is the first non-comment section.
// This unit will follow this specification when saving; however it can load files that do not have
// the header first, as long as the header is present somewhere.

// Graphics in a NeoLemmix graphic set are almost just an array of ARGB pixels. However, if the A
// is zero, then the RGB is omitted and the next pixel is started immediately.

// The encode graphic function in this unit is non-destructive, so it is safe to pass images to it
// directly without an intermediary TBitmap32 object.

interface

uses
  Dialogs, PngInterface {for LeadZeroStr},
  Classes, SysUtils, LemGraphicSet, LemDosCmp, GR32;

const
  gsVersionNumber = 0;

  dtSecMarker = $FF;

  dtEof = $00;      //not strictly required
  dtComment = $01;
  dtHeader = $02;
  dtObject = $03;
  dtTerrain = $04;
  dtSound = $05;
  dtLemming = $06;
  dtNames = $07;
  dtOffsets = $08;

type

  ColorEntry = packed record
    case Byte of
      0: (A, R, G, B: Byte); // don't think this structure is ever needed, but nice to have just in case
      1: (ARGB: TColor32);
  end;

  NeoLemmixHeader = packed record
    VersionNumber: Byte;
    Resolution: Byte;
    Updated: Byte;
    Reserved: Array[0..12] of Byte;
    KeyColors: Array[0..7] of ColorEntry;
  end;

  NeoLemmixObjectData = packed record
    ObjectFlags: Word;
    FrameCount: Word;
    PreviewFrame: Word;
    KeyFrame: Word;
    BaseLoc: LongWord;
    TriggerEff: Byte;
    TriggerSound: Byte;
    PTriggerX: SmallInt;
    PTriggerY: SmallInt;
    PTriggerW: SmallInt;
    PTriggerH: SmallInt;
    Reserved: Array[0..1] of Byte;
    STriggerX: SmallInt;
    STriggerY: SmallInt;
    STriggerW: SmallInt;
    STriggerH: SmallInt;
    Resize: Byte;
    Reserved2: Array[0..6] of Byte;
  end;

  NeoLemmixTerrainData = packed record
    TerrainFlags: Word;
    BaseLoc: LongWord;
    Reserved: Array[0..9] of Byte;
  end;

  NeoLemmixSoundData = packed record
    SoundID: Byte;
    SoundLoc: LongWord;
  end;

  TNeoLemmixGraphicSetClass = class of TNeoLemmixGraphicSet;
  TNeoLemmixGraphicSet = class(TComponent)
    private
      procedure FixBmpSize(aBmp: TBitmap32; aSrcRes: Integer);
      procedure FixObject(aObject: TMetaObject; aSrcRes: Integer);
    public
      //constructor Create(aOwner: TComponent); override;
      //destructor Destroy; override;
      procedure SaveGraphicSet(fn: String; GS: TBaseGraphicSet);
      procedure DefSection(secCode: Byte; aStream: TStream);
      procedure EncodeGraphic(aBitmap: TBitmap32; aStream: TStream);
      function LoadGraphicSet(fn: String): TBaseGraphicSet;
      function LoadHeader(aStream: TStream): NeoLemmixHeader;
      function GetNextSection(srcStream: TStream; dstStream: TStream): Byte;
      function DecodeGraphic(aStream: TStream; Loc: LongWord): TBitmap32; overload;
      function DecodeGraphic(aStream: TStream): TBitmap32; overload;
      function DecodeSound(aStream: TStream; Loc: LongWord): TMemoryStream;
  end;

implementation

uses
  GSToolMain; // for version number

{constructor TNeoLemmixGraphicSet.Create(aOwner: TComponent);
begin
  inherited;
end;}

{destructor TNeoLemmixGraphicSet.Destroy;
begin
  inherited;
end;}

procedure TNeoLemmixGraphicSet.FixBmpSize(aBmp: TBitmap32; aSrcRes: Integer);
var
  TempBMP: TBitmap32;
begin
  TempBMP := TBitmap32.Create;
  TempBMP.Assign(aBmp);
  aBmp.SetSize(aBmp.Width * 8 div aSrcRes, aBmp.Height * 8 div aSrcRes);
  aBmp.Clear(0);
  TempBMP.DrawTo(aBmp, aBmp.BoundsRect, TempBMP.BoundsRect);
  TempBMP.Free;
end;

procedure TNeoLemmixGraphicSet.FixObject(aObject: TMetaObject; aSrcRes: Integer);
  function Fix(aValue: Integer): Integer;
  begin
    Result := aValue * 8 div aSrcRes;
  end;
begin
  with aObject do
  begin
    PTriggerX := Fix(PTriggerX);
    PTriggerY := Fix(PTriggerY);
    PTriggerW := Fix(PTriggerW);
    PTriggerH := Fix(PTriggerH);
    STriggerX := Fix(STriggerX);
    STriggerY := Fix(STriggerY);
    STriggerW := Fix(STriggerW);
    STriggerH := Fix(STriggerH);
  end;
end;

function TNeoLemmixGraphicSet.LoadGraphicSet(fn: String): TBaseGraphicSet;
var
  MetaInfoStream, GfxStream: TMemoryStream;
  CompressedStream: TMemoryStream;
  TempStream: TMemoryStream;
  Decompressor: TDosDatDecompressor;
  TempHeader: NeoLemmixHeader;
  TempTerrain: NeoLemmixTerrainData;
  TempObject: NeoLemmixObjectData;
  TempSound: NeoLemmixSoundData;

  TempGSTerrain: TMetaTerrain;
  TempGSObject: TMetaObject;
  TempGSSound: TMemoryStream; //since the GS editor doesn't actually handle sounds in any special way
  TempBMP: TBitmap32;
  TempBMPs: TBitmaps;

  i: Integer;
  li: Integer;
  b: Byte;
  s: String;

  FlushColors: Boolean;
  SrcRes: Integer;
begin
  Result := TBaseGraphicSet.Create;
  MetaInfoStream := TMemoryStream.Create;
  GfxStream := TMemoryStream.Create;
  CompressedStream := TMemoryStream.Create;
  Decompressor := TDosDatDecompressor.Create;
  TempStream := TMemoryStream.Create;

  CompressedStream.LoadFromFile(fn);

  MetaInfoStream.Seek(0, soFromBeginning);
  GfxStream.Seek(0, soFromBeginning);
  CompressedStream.Seek(0, soFromBeginning);
  TempStream.Seek(0, soFromBeginning);

  Decompressor.DecompressSection(CompressedStream, MetaInfoStream);
  Decompressor.DecompressSection(CompressedStream, GfxStream);

  Decompressor.Free;
  CompressedStream.Free;

  TempHeader := LoadHeader(MetaInfoStream);
  SrcRes := TempHeader.Resolution;
  Result.Resolution := 8;

  for i := 0 to 7 do
    Result.KeyColors[i] := TempHeader.KeyColors[i].ARGB;

  if not (TempHeader.Updated = 1) then
  begin
    FlushColors := true;
    for i := 3 to 7 do
      Result.KeyColors[i] := 0;
  end else
    FlushColors := false;

  MetaInfoStream.Seek(0, soFromBeginning);

  b := GetNextSection(MetaInfoStream, TempStream);
  while b <> dtEof do
  begin
    TempStream.Seek(0, soFromBeginning);
    case b of
      dtTerrain: begin
                   TempStream.Read(TempTerrain, SizeOf(TempTerrain));
                   TempGSTerrain := TMetaTerrain.Create;
                   TempGSTerrain.Steel := ((TempTerrain.TerrainFlags and $0001) <> 0);
                   TempGSTerrain.Name := 'T' + IntToStr(Result.MetaTerrains.Count);
                   TempBMP := DecodeGraphic(GfxStream, TempTerrain.BaseLoc);
                   Result.MetaTerrains.Add(TempGSTerrain);
                   Result.TerrainImages.Add(TempBMP);
                   FixBmpSize(TempBMP, SrcRes);
                 end;
      dtObject:  begin
                   TempStream.Read(TempObject, SizeOf(TempObject));
                   TempGSObject := TMetaObject.Create;
                   TempGSObject.Name := 'O' + IntToStr(Result.MetaObjects.Count);
                   TempGSObject.PreviewFrame := TempObject.PreviewFrame;
                   TempGSObject.KeyFrame := TempObject.KeyFrame;
                   TempGSObject.TriggerAnim := ((TempObject.ObjectFlags and $0001) <> 0);
                   TempGSObject.TriggerType := TempObject.TriggerEff;
                   TempGSObject.TriggerSound := TempObject.TriggerSound;
                   TempGSObject.PTriggerX := TempObject.PTriggerX;
                   TempGSObject.PTriggerY := TempObject.PTriggerY;
                   TempGSObject.PTriggerW := TempObject.PTriggerW;
                   TempGSObject.PTriggerH := TempObject.PTriggerH;
                   TempGSObject.STriggerX := TempObject.STriggerX;
                   TempGSObject.STriggerY := TempObject.STriggerY;
                   TempGSObject.STriggerW := TempObject.STriggerW;
                   TempGSObject.STriggerH := TempObject.STriggerH;
                   TempGSObject.RandomFrame := ((TempObject.ObjectFlags and $0002) <> 0);
                   TempGSObject.ResizeHorizontal := ((TempObject.Resize and $01) <> 0);
                   TempGSObject.ResizeVertical := ((TempObject.Resize and $02) <> 0);

                   TempBMPs := TBitmaps.Create;
                   for i := 0 to TempObject.FrameCount-1 do
                   begin
                     if i = 0 then
                       TempBMP := DecodeGraphic(GfxStream, TempObject.BaseLoc)
                     else
                       TempBMP := DecodeGraphic(GfxStream);
                     TempBMPs.Add(TempBmp);
                     FixBmpSize(TempBmp, SrcRes);
                   end;

                   FixObject(TempGSObject, SrcRes);

                   Result.ObjectImages.Add(TempBMPs);
                   Result.MetaObjects.Add(TempGSObject);

                 end;
      dtSound:  begin
                  TempStream.Read(TempSound, SizeOf(TempSound));
                  TempGSSound := DecodeSound(GfxStream, TempSound.SoundLoc);
                  if Result.Sounds[TempSound.SoundID] <> nil then Result.Sounds[TempSound.SoundID].Free;
                  Result.Sounds[TempSound.SoundID] := TempGSSound;
                end;
      dtLemming: begin
                   s := '';
                   repeat
                     MetaInfoStream.Read(b, 1);
                     if b <> 0 then s := s + Chr(b);
                   until b = 0;
                   Result.LemmingSprites := s;
                 end;
      dtNames: begin
                 FlushColors := false;
                 for i := 0 to Result.MetaObjects.Count-1 do
                 begin
                   s := '';
                   repeat
                     MetaInfoStream.Read(b, 1);
                     if b <> 0 then s := s + Chr(b);
                   until b = 0;
                   if s = '' then s := 'object_' + LeadZeroStr(i, 2);
                   Result.MetaObjects[i].Name := s;
                   MetaInfoStream.Read(li, 4);
                   Result.MetaObjects[i].OffsetL := li;
                   MetaInfoStream.Read(li, 4);
                   Result.MetaObjects[i].OffsetT := li;
                   MetaInfoStream.Read(li, 4);
                   Result.MetaObjects[i].OffsetR := li;
                   MetaInfoStream.Read(li, 4);
                   Result.MetaObjects[i].OffsetB := li;
                   MetaInfoStream.Read(b, 1);
                   Result.MetaObjects[i].ConvertFlip := (b and $01) <> 0;
                   Result.MetaObjects[i].ConvertInvert := (b and $02) <> 0;
                   for li := 17 to 31 do
                     MetaInfoStream.Read(b, 1);
                 end;

                 for i := 0 to Result.MetaTerrains.Count-1 do
                 begin
                   s := '';
                   repeat
                     MetaInfoStream.Read(b, 1);
                     if b <> 0 then s := s + Chr(b);
                   until b = 0;
                   if s = '' then s := 'terrain_' + LeadZeroStr(i, 2);
                   Result.MetaTerrains[i].Name := s;
                   MetaInfoStream.Read(li, 4);
                   Result.MetaTerrains[i].OffsetL := li;
                   MetaInfoStream.Read(li, 4);
                   Result.MetaTerrains[i].OffsetT := li;
                   MetaInfoStream.Read(li, 4);
                   Result.MetaTerrains[i].OffsetR := li;
                   MetaInfoStream.Read(li, 4);
                   Result.MetaTerrains[i].OffsetB := li;
                   MetaInfoStream.Read(b, 1);
                   Result.MetaTerrains[i].ConvertFlip := (b and $01) <> 0;
                   Result.MetaTerrains[i].ConvertInvert := (b and $02) <> 0;
                   Result.MetaTerrains[i].ConvertRotate := (b and $04) <> 0;
                   for li := 17 to 31 do
                     MetaInfoStream.Read(b, 1);
                 end;
               end;
    end;
    TempStream.Seek(0, soFromBeginning);
    b := GetNextSection(MetaInfoStream, TempStream);
  end;

  if FlushColors then
  begin
    Result.KeyColors[1] := Result.KeyColors[0];
    for i := 2 to 7 do
      Result.KeyColors[i] := 0;
  end;

  GfxStream.Free;
  MetaInfoStream.Free;
  TempStream.Free;

end;

function TNeoLemmixGraphicSet.DecodeSound(aStream: TStream; Loc: LongWord): TMemoryStream;
var
  lw: LongWord;
begin
  aStream.Seek(Loc, soFromBeginning);
  aStream.Read(lw, 4);
  Result := TMemoryStream.Create;
  Result.Seek(0, soFromBeginning);
  Result.CopyFrom(aStream, lw);
end;

function TNeoLemmixGraphicSet.DecodeGraphic(aStream: TStream; Loc: LongWord): TBitmap32;
begin
  aStream.Seek(Loc, soFromBeginning);
  Result := DecodeGraphic(aStream);
end;

function TNeoLemmixGraphicSet.DecodeGraphic(aStream: TStream): TBitmap32;
var
  x, y, W, H: LongWord;
  a, r, g, b: Byte;
begin
  Result := TBitmap32.Create;
  aStream.Read(W, 4);
  aStream.Read(H, 4);
  Result.SetSize(W, H);
  for y := 0 to H-1 do
    for x := 0 to W-1 do
    begin
      aStream.Read(a, 1);
      if a = 0 then
        Result.Pixel[x, y] := 0
      else begin
        aStream.Read(r, 1);
        aStream.Read(g, 1);
        aStream.Read(b, 1);
        Result.Pixel[x, y] := (a shl 24) + (r shl 16) + (g shl 8) + b;
      end;
    end;
end;

function TNeoLemmixGraphicSet.LoadHeader(aStream: TStream): NeoLemmixHeader;
var
  b: Byte;
  TempStream: TMemoryStream;
begin
  aStream.Seek(0, soFromBeginning);
  TempStream := TMemoryStream.Create;
  repeat
    TempStream.Seek(0, soFromBeginning);
    b := GetNextSection(aStream, TempStream);
  until b in [dtHeader, dtEof];
  TempStream.Seek(0, soFromBeginning);
  if b = dtHeader then TempStream.Read(Result, SizeOf(Result));
  TempStream.Free;
end;

function TNeoLemmixGraphicSet.GetNextSection(srcStream: TStream; dstStream: TStream): Byte;
var
  b: Byte;
  i: Integer;
begin
  b := 0;
  i := 0;
  while b <> $FF do
    srcStream.Read(b, 1);
  srcStream.Read(Result, 1);
  case Result of
    dtEof: Exit;
    dtComment: while true do
               begin
                 srcStream.Read(b, 1);
                 if b = $FF then
                 begin
                   srcStream.Position := srcStream.Position - 1;
                   Exit;
                 end;
                 dstStream.Write(b, 1);
               end;
    dtHeader: i := SizeOf(NeoLemmixHeader);
    dtObject: i := SizeOf(NeoLemmixObjectData);
    dtTerrain: i := SizeOf(NeoLemmixTerrainData);
    dtSound: i := SizeOf(NeoLemmixSoundData);
  end;
  while i > 0 do
  begin
    srcStream.Read(b, 1);
    dstStream.Write(b, 1);
    i := i - 1;
  end;
end;

procedure TNeoLemmixGraphicSet.SaveGraphicSet(fn: String; GS: TBaseGraphicSet);
var
  MetaInfoStream, GfxStream: TMemoryStream;
  CompressStream: TMemoryStream;
  Compressor: TDosDatCompressor;
  TempHeader: NeoLemmixHeader;
  TempObject: NeoLemmixObjectData;
  TempTerrain: NeoLemmixTerrainData;
  TempSound: NeoLemmixSoundData;
  MO: TMetaObject;

  i, i2: Integer;
  s: String;
  b: Byte;
  mw, mh: Integer;
  lw: LongWord;
  TempBMP: TBitmap32;

  procedure ClearObjectRecord;
  var
    cPoint: ^Byte;
    i: Integer;
  begin
    cPoint := @TempObject;
    for i := 0 to SizeOf(TempObject)-1 do
    begin
      cPoint^ := 0;
      Inc(cPoint);
    end;
  end;

begin

  MetaInfoStream := TMemoryStream.Create;
  GfxStream := TMemoryStream.Create;
  CompressStream := TMemoryStream.Create;
  MetaInfoStream.Seek(0, soFromBeginning);
  GfxStream.Seek(0, soFromBeginning);
  CompressStream.Seek(0, soFromBeginning);
  Compressor := TDosDatCompressor.Create;

  // Comment to identify creation tool
  s := 'NeoLemmix Graphic Set Tool V' + TVersion;
  DefSection(dtComment, MetaInfoStream);
  MetaInfoStream.Write(s[1], Length(s));

  // Header
  TempHeader.VersionNumber := gsVersionNumber;
  TempHeader.Resolution := GS.Resolution;
  if TempHeader.Resolution = 0 then TempHeader.Resolution := 8;
  for i := 0 to 7 do
    TempHeader.KeyColors[i].ARGB := GS.KeyColors[i];
  TempHeader.Updated := 1;

  DefSection(dtHeader, MetaInfoStream);
  MetaInfoStream.Write(TempHeader, SizeOf(TempHeader));

  for i := 0 to GS.MetaTerrains.Count-1 do
    with TempTerrain do
    begin
      TerrainFlags := 0;
      if GS.MetaTerrains[i].Steel then
        TerrainFlags := TerrainFlags or $0001;
      BaseLoc := GfxStream.Position;
      EncodeGraphic(GS.TerrainImages[i], GfxStream);
      DefSection(dtTerrain, MetaInfoStream);
      MetaInfoStream.Write(TempTerrain, SizeOf(TempTerrain));
    end;

  TempBMP := TBitmap32.Create; // used to equalize the size of all frames in objects

  for i := 0 to GS.MetaObjects.Count-1 do
    with TempObject do
    begin
      ClearObjectRecord;
      MO := GS.MetaObjects[i]; //it's referred to a lot, so shortcut it...
      if MO.TriggerAnim then ObjectFlags := ObjectFlags or $0001;
      FrameCount := GS.ObjectImages[i].Count;
      PreviewFrame := MO.PreviewFrame;
      KeyFrame := MO.KeyFrame;
      BaseLoc := GfxStream.Position;
      TriggerEff := MO.TriggerType;
      TriggerSound := MO.TriggerSound;
      PTriggerX := MO.PTriggerX;
      PTriggerY := MO.PTriggerY;
      if MO.TriggerType = 12 {receiver} then
      begin
        PTriggerW := 1;
        PTriggerH := 1;
        // we need to write this, if we preserve the old value weird stuff will happen
      end else begin
        PTriggerW := MO.PTriggerW;
        PTriggerH := MO.PTriggerH;
      end;
      STriggerX := MO.STriggerX;
      STriggerY := MO.STriggerY;
      STriggerW := MO.STriggerW;
      STriggerH := MO.STriggerH;
      if MO.RandomFrame then ObjectFlags := ObjectFlags or $0002;
      if MO.ResizeHorizontal then Resize := Resize or $01;
      if MO.ResizeVertical then Resize := Resize or $02;

      mw := 0;
      mh := 0;
      for i2 := 0 to GS.ObjectImages[i].Count-1 do
      begin
        if GS.ObjectImages[i][i2].Width > mw then mw := GS.ObjectImages[i][i2].Width;
        if GS.ObjectImages[i][i2].Height > mh then mh := GS.ObjectImages[i][i2].Height;
      end;
      TempBMP.SetSize(mw, mh);

      for i2 := 0 to GS.ObjectImages[i].Count-1 do
      begin
        TempBMP.Clear(0);
        GS.ObjectImages[i][i2].DrawTo(TempBMP, 0, 0);
        EncodeGraphic(TempBMP, GfxStream);
      end;
      DefSection(dtObject, MetaInfoStream);
      MetaInfoStream.Write(TempObject, SizeOf(TempObject));
    end;

  for i := 0 to 255 do
    if GS.Sounds[i] <> nil then
    begin
      GS.Sounds[i].Seek(0, soFromBeginning);
      TempSound.SoundID := i;
      TempSound.SoundLoc := GfxStream.Position;
      lw := GS.Sounds[i].Size;
      GfxStream.Write(lw, 4);
      GfxStream.CopyFrom(GS.Sounds[i], GS.Sounds[i].Size);
      DefSection(dtSound, MetaInfoStream);
      MetaInfoStream.Write(TempSound, SizeOf(TempSound));
    end;

  s := GS.LemmingSprites;
  DefSection(dtLemming, MetaInfoStream);
  MetaInfoStream.Write(s[1], Length(s));
  b := 0;
  MetaInfoStream.Write(b, 1);

  DefSection(dtNames, MetaInfoStream);

  for i := 0 to GS.MetaObjects.Count-1 do
  begin
    s := GS.MetaObjects[i].Name;
    MetaInfoStream.Write(s[1], Length(s));
    b := 0;
    MetaInfoStream.Write(b, 1);
    i2 := GS.MetaObjects[i].OffsetL;
    MetaInfoStream.Write(i2, 4);
    i2 := GS.MetaObjects[i].OffsetT;
    MetaInfoStream.Write(i2, 4);
    i2 := GS.MetaObjects[i].OffsetR;
    MetaInfoStream.Write(i2, 4);
    i2 := GS.MetaObjects[i].OffsetB;
    MetaInfoStream.Write(i2, 4);
    b := 0;
    if GS.MetaObjects[i].ConvertFlip then
      b := b or $01;
    if GS.MetaObjects[i].ConvertInvert then
      b := b or $02;
    MetaInfoStream.Write(b, 1);
    b := 0;
    for i2 := 17 to 31 do // CHANGE THIS LINE IF EXTRA BYTES USED
      MetaInfoStream.Write(b, 1);
  end;

  for i := 0 to GS.MetaTerrains.Count-1 do
  begin
    s := GS.MetaTerrains[i].Name;
    MetaInfoStream.Write(s[1], Length(s));
    b := 0;
    MetaInfoStream.Write(b, 1);
    i2 := GS.MetaTerrains[i].OffsetL;
    MetaInfoStream.Write(i2, 4);
    i2 := GS.MetaTerrains[i].OffsetT;
    MetaInfoStream.Write(i2, 4);
    i2 := GS.MetaTerrains[i].OffsetR;
    MetaInfoStream.Write(i2, 4);
    i2 := GS.MetaTerrains[i].OffsetB;
    MetaInfoStream.Write(i2, 4);
    b := 0;
    if GS.MetaTerrains[i].ConvertFlip then
      b := b or $01;
    if GS.MetaTerrains[i].ConvertInvert then
      b := b or $02;
    if GS.MetaTerrains[i].ConvertRotate then
      b := b or $04;
    MetaInfoStream.Write(b, 1);
    b := 0;
    for i2 := 17 to 31 do // CHANGE THIS LINE IF EXTRA BYTES USED
      MetaInfoStream.Write(b, 1);
  end;

  DefSection(dtEof, MetaInfoStream);

  MetaInfoStream.Seek(0, soFromBeginning);
  GfxStream.Seek(0, soFromBeginning);

  Compressor.Compress(MetaInfoStream, CompressStream);
  Compressor.Compress(GfxStream, CompressStream);

  CompressStream.SaveToFile(fn);

  MetaInfoStream.Free;
  GfxStream.Free;
  CompressStream.Free;
  Compressor.Free;

end;

procedure TNeoLemmixGraphicSet.EncodeGraphic(aBitmap: TBitmap32; aStream: TStream);
var
  x, y: Integer;
  a, r, g, b: Byte;
begin
  x := aBitmap.Width;
  y := aBitmap.Height;
  aStream.Write(x, 4);
  aStream.Write(y, 4);

  for y := 0 to aBitmap.Height-1 do
    for x := 0 to aBitmap.Width-1 do
    begin
      a := (aBitmap.Pixel[x, y] and $FF000000) shr 24;
      r := (aBitmap.Pixel[x, y] and $FF0000) shr 16;
      g := (aBitmap.Pixel[x, y] and $FF00) shr 8;
      b := (aBitmap.Pixel[x, y] and $FF);
      aStream.Write(a, 1);
      if a = 0 then Continue;
      aStream.Write(r, 1);
      aStream.Write(g, 1);
      aStream.Write(b, 1);
    end;

end;

procedure TNeoLemmixGraphicSet.DefSection(secCode: Byte; aStream: TStream);
var
  tb: Byte; //because apparently, you can't write a fixed value to a stream. The fuck is this? xD
begin
  tb := dtSecMarker;
  aStream.Write(tb, 1);
  aStream.Write(secCode, 1);
end;

end.
