unit GSLoadDOS;

// Loads a DOS or Lemmix (or old-format NeoLemmix) graphic set into the editor's
// internal format. To use, create a TDosGraphicSet object, then call:
//
// <TBaseGraphicSet> := <TDosGraphicSet>.LoadGraphicSet(<metainfo file>, <graphic file>);
//
// The TBaseGraphicSet object is created during TDosGraphicSet.LoadGraphicSet, so you should
// not use <TBaseGraphicSet> := TBaseGraphicSet.Create; beforehand.
//
// Once TDosGraphicSet.LoadGraphicSet has been allowed to run, there is no need to keep the
// TDosGraphicSet object around, it can be freed.

// Saving old-format NeoLemmix graphic sets is very similar, but use this instead:
//
// <TDosGraphicSet>.SaveGraphicSet(<metainfo file>, <graphic file>, <TBaseGraphicSet>);
//
// Note that only NeoLemmix-format is supported for saving; DOS / traditional Lemmix formats
// are not supported. Graphic sets will always be saved as 19-bit graphics, even if a lower
// bit depth could be used. Theoretically, these graphic sets should work fine with newer
// versions of traditional Lemmix as long as no NeoLemmix-exclusive features are used, such
// as non-multiple-of-4 trigger area coordinates/sizes; but they will not work with DOS.
// The extra info used only by CheapoCon and not by the NeoLemmix player is indeed saved
// to the output file.

// IMPORTANT WARNING: If modifying this class and using the SaveImage routine, DO NOT SEND
//                    BITMAPS TO IT THAT YOU PLAN TO USE ELSEWHERE AFTERWARDS. This routine
//                    modifies the bitmap internally. Assign the image to a temporary TBitmap32,
//                    and send *that* to SaveImage.

interface

uses
  Dialogs,
  Classes, SysUtils, LemGraphicSet, LemDosCmp, GR32;

const
  FixedDosPalette: Array[0..7] of TColor32 =
  (
    $FF000000,
    $FF4040E0,
    $FF00B000,
    $FFF0D0D0,
    $FFB0B000,
    $FFF02020,
    $FF808080,
    $FFFFFFFF
  );

  FixedDosXmasPalette: Array[0..7] of TColor32 =
  (
    $FF000000,
    $FFD02020,
    $FF00B000,
    $FFF0D0D0,
    $FFF0F000,
    $FF4040F0,
    $FF808080,
    $FFFFFFFF
  );

type
  TSettingsFlag =
    ( dsf5Bit, dsfTriggerAdjust, dsfSteelMark, dsfXmasPal,
      dsfProtected, dsfExtLocs, dsf19Bit, dsfMultiWindow );
  TSettingsFlags = set of TSettingsFlag;

  DosObjectData = packed record
     AnimFlags:       Byte;
     GsOptions:       Byte;
     StartFrame:      Byte;
     FrameCount:      Byte;
     ObjWidth:        Byte;
     ObjHeight:       Byte;
     NextFrameOffset: Word;
     MaskOffset:      Word;
     BaseLocHigh:     Word;
     Unknown1:        Word;
     PTriggerX:       Byte;
     STriggerX:       Byte;
     PTriggerY:       Byte;
     STriggerY:       Byte;
     PTriggerW:       Byte;
     PTriggerH:       Byte;
     TriggerEff:      Byte;
     BaseLocLow:      Word;
     PreviewLoc:      Word;
     TriggerAdj:      Byte;
     KeyFrame:        Byte;
     TriggerSound:    Byte;
  end;

  DosTerrainData = packed record
     TerWidth:        Byte;
     TerHeight:       Byte;
     BaseLocLow:      Word;
     MaskLocLow:      Word;
     BaseLocHigh:     Byte; //also contains autosteel flag
     MaskLocHigh:     Byte;
  end;

  DosPaletteVgaEntry = packed record
     R: Byte;
     G: Byte;
     B: Byte;
  end;

  DosPaletteData = packed record
     EgaPal: Array[0..23] of byte;
     VgaPal: Array[0..23] of DosPaletteVgaEntry;
  end;

  DosMetaInfoFile = packed record
     MetaObjects: Array[0..15] of DosObjectData;
     MetaTerrains: Array[0..63] of DosTerrainData;
     Palettes: DosPaletteData;
  end;

  DosVgaspecPaletteData = packed record
     VgaPal: Array[0..7] of DosPaletteVgaEntry;
     EgaPal: Array[0..7] of Byte;
     UnknownPal: Array[0..7] of Byte;
  end;

  TDosGraphicSetClass = class of TDosGraphicSet;
  TDosGraphicSet = class(TComponent)
    private
      fMetaInfo: Array[0..1] of DosMetaInfoFile;
      fMetaInfoFileStream: TMemoryStream;
      fObjectGfxSection: TMemoryStream;
      fTerrainGfxSection: TMemoryStream;
      fCompressedDataFile: TMemoryStream;
      fDecompressor: TDosDatDecompressor;
      fCompressor: TDosDatCompressor;
      fSettingsFlags: TSettingsFlags;
      procedure LoadMetaData(Target: TBaseGraphicSet);
      procedure LoadImages(Target: TBaseGraphicSet);
      function LoadImage(Loc: Integer; MaskLoc: Integer; Width: Integer; Height: Integer; BPP: Byte; Src: TMemoryStream): TBitmap32;
      function SaveImage(Width: Integer; Height: Integer; Src: TBitmap32; Dst: TMemoryStream): Integer;
      procedure RemoveRLE(Src: TMemoryStream; Dst: TMemoryStream);
    public
      constructor Create(aOwner: TComponent); override;
      destructor Destroy; override;
      function LoadGraphicSet(MetaInfoFile: String; GraphicFile: String): TBaseGraphicSet;
      function LoadVgaspec(SpecFile: String): TBaseGraphicSet;
      procedure SaveGraphicSet(MetaInfoFile: String; GraphicFile: String; GS: TBaseGraphicSet);
  end;


implementation

constructor TDosGraphicSet.Create(aOwner: TComponent);
begin
  inherited;
  fMetaInfoFileStream := TMemoryStream.Create;
  fObjectGfxSection := TMemoryStream.Create;
  fTerrainGfxSection := TMemoryStream.Create;
  fCompressedDataFile := TMemoryStream.Create;
  fDecompressor := TDosDatDecompressor.Create;
  fCompressor := TDosDatCompressor.Create;
end;

destructor TDosGraphicSet.Destroy;
begin
  fMetaInfoFileStream.Free;
  fObjectGfxSection.Free;
  fTerrainGfxSection.Free;
  fCompressedDataFile.Free;
  fDecompressor.Free;
  fCompressor.Free;
  inherited;
end;

procedure TDosGraphicSet.SaveGraphicSet(MetaInfoFile: String; GraphicFile: String; GS: TBaseGraphicSet);
var
  i, i2: Integer;
  mis, mip: Integer;
  mw, mh: Integer;
  TempBMP: TBitmap32;
  TL: Integer;
begin

  for i := 0 to GS.TerrainImages.Count-1 do
    if (GS.TerrainImages[i].Width > 248) or (GS.TerrainImages[i].Height > 255) then
    begin
      ShowMessage('Error: GROUND/VGAGR format does not support pieces with dimensions exceeding' + #13 +
                  'widths of 248 or heights of 255. (Triggered on Terrain ' + IntToStr(i) + ')');
      Exit;
    end;

  for i := 0 to GS.ObjectImages.Count-1 do
    for i2 := 0 to GS.ObjectImages[i].Count-1 do
      if (GS.ObjectImages[i][i2].Width > 248) or (GS.ObjectImages[i][i2].Height > 255) then
      begin
        ShowMessage('Error: GROUND/VGAGR format does not support pieces with dimensions exceeding' + #13 +
                    'widths of 248 or heights of 255. (Triggered on Object ' + IntToStr(i) + ', Frame ' + IntToStr(i2) + ')');
        Exit;
      end;

  fMetaInfo[0].MetaObjects[0].GsOptions := $E6;
  TempBMP := TBitmap32.Create; // Used variously throughout both upcoming sections

  fTerrainGfxSection.Seek(0, soFromBeginning);
  for i := 0 to GS.MetaTerrains.Count-1 do
  begin
    mis := i div 64;
    mip := i mod 64;
    with fMetaInfo[mis].MetaTerrains[mip] do
    begin
      if GS.MetaTerrains[i].Steel then BaseLocHigh := 1;
      TempBmp.Assign(GS.TerrainImages[i]);
      TL := SaveImage(TempBmp.Width, TempBmp.Height, TempBmp, fTerrainGfxSection);
      BaseLocLow := (TL mod 65536);
      BaseLocHigh := BaseLocHigh + ((TL div 65536) shl 1);
      TerWidth := (((TempBmp.Width - 1) div 8) * 8) + 8;
      TerHeight := TempBmp.Height;
    end;
  end;

  fObjectGfxSection.Seek(0, soFromBeginning);
  for i := 0 to GS.MetaObjects.Count-1 do
  begin
    mis := i div 16;
    mip := i mod 16;
    with fMetaInfo[mis].MetaObjects[mip] do
    begin
      mw := 0;
      mh := 0;
      for i2 := 0 to GS.ObjectImages[i].Count-1 do
        with GS.ObjectImages[i][i2] do
        begin
          if Width > mw then mw := Width;
          if Height > mh then mh := Height;
        end;
      for i2 := 0 to GS.ObjectImages[i].Count-1 do
      begin
        TempBmp.Assign(GS.ObjectImages[i][i2]);
        TL := SaveImage(mw, mh, TempBmp, fObjectGfxSection);
        if i2 = 0 then
        begin
          BaseLocLow := TL mod 65536;
          BaseLocHigh := TL div 65536;
          NextFrameOffset := fObjectGfxSection.Position - TL;
          PreviewLoc := TL + (NextFrameOffset * GS.MetaObjects[i].PreviewFrame);
        end;
      end;

      FrameCount := GS.ObjectImages[i].Count;
      ObjWidth := (mw + 7) and not 7;
      ObjHeight := mh;
      Unknown1 := ObjWidth - mw;

      TriggerEff := GS.MetaObjects[i].TriggerType;
      TriggerSound := GS.MetaObjects[i].TriggerSound;
      KeyFrame := GS.MetaObjects[i].KeyFrame;
      PTriggerX := ((GS.MetaObjects[i].PTriggerX + 3) and not 3) div 4;
      PTriggerY := (((GS.MetaObjects[i].PTriggerY + 3) and not 3) div 4) + 1; //format oddity
      PTriggerW := ((GS.MetaObjects[i].PTriggerW + 3) and not 3) div 4;
      PTriggerH := ((GS.MetaObjects[i].PTriggerH + 3) and not 3) div 4;
      STriggerX := GS.MetaObjects[i].STriggerX;
      STriggerY := GS.MetaObjects[i].STriggerY;
      TriggerAdj := (((4 - (GS.MetaObjects[i].PTriggerX mod 4)) mod 4) shl 6)
                  + (((4 - (GS.MetaObjects[i].PTriggerY mod 4)) mod 4) shl 4)
                  + (((4 - (GS.MetaObjects[i].PTriggerW mod 4)) mod 4) shl 2)
                  + (((4 - (GS.MetaObjects[i].PTriggerH mod 4)) mod 4));

      if TriggerEff = 23 then
      begin
        AnimFlags := 3;
        StartFrame := 1;
      end else if (TriggerEff = 1) and GS.MetaObjects[i].TriggerAnim then
        AnimFlags := 1
      else if TriggerEff in [4, 11, 12, 15, 17, 21, 23, 24, 28, 29] then
        AnimFlags := 1
      else if FrameCount > 1 then
        AnimFlags := 2
      else
        AnimFlags := 0;

    end;
  end;

  TempBmp.Free; // We don't need you anymore!

  // Let's not forget about palettes...
  for i := 0 to 7 do
    with fMetaInfo[0].Palettes.VgaPal[i] do
    begin
      R := (GS.KeyColors[i] and $FF0000) shr 18;
      G := (GS.KeyColors[i] and $FF00) shr 10;
      B := (GS.KeyColors[i] and $FF) shr 2;
    end;

  // We don't really need to copy the fixed palette into here.
  // But let's do it anyway just to be safe. Some tools read it
  // from the file rather than hardcode it. This can be especially
  // important for color #7 (or color #15 in data file order).
  for i := 0 to 6 do
    with fMetaInfo[0].Palettes.VgaPal[i + 8] do
    begin
      R := (FixedDosPalette[i] and $FF0000) shr 18;
      G := (FixedDosPalette[i] and $FF00) shr 10;
      B := (FixedDosPalette[i] and $FF) shr 2;
    end;
  fMetaInfo[0].Palettes.VgaPal[15] := fMetaInfo[0].Palettes.VgaPal[0];

  fMetaInfoFileStream.WriteBuffer(fMetaInfo[0], SizeOf(fMetaInfo[0]));
  if (GS.MetaTerrains.Count > 64) or (GS.MetaObjects.Count > 16) then
    fMetaInfoFileStream.WriteBuffer(fMetaInfo[1], SizeOf(fMetaInfo[1]));

  fCompressedDataFile.Seek(0, soFromBeginning);
  fTerrainGfxSection.Seek(0, soFromBeginning);
  fObjectGfxSection.Seek(0, soFromBeginning);

  fCompressor.Compress(fTerrainGfxSection, fCompressedDataFile);
  fCompressor.Compress(fObjectGfxSection, fCompressedDataFile);

  fMetaInfoFileStream.SaveToFile(MetaInfoFile);
  fCompressedDataFile.SaveToFile(GraphicFile);

end;

function TDosGraphicSet.SaveImage(Width: Integer; Height: Integer; Src: TBitmap32; Dst: TMemoryStream): Integer;
// WARNING: DESTROYS SOURCE IMAGE! ASSIGN TO A TEMPORARY TBITMAP32 FIRST!!
var
  x, y, b: Integer;
  cb: Byte;
  CPix: TColor32;
begin
  Width := (Width + 7) and not 7; //long-winded way of rounding up to next multiple of 8, but I recognise this easily :P
  Result := Dst.Position;

  for y := 0 to Src.Height-1 do
    for x := 0 to Src.Width-1 do
    begin
      CPix := Src.Pixel[x, y];
      if (CPix and $FF000000) = 0 then
        CPix := 0
      else
        CPix := ((CPix and $FC0000) shr 6)
              + ((CPix and $FC00) shr 4)
              + ((CPix and $FC) shr 2)
              + $40000;
      Src.Pixel[x, y] := CPix;
    end;

  for b := 0 to 18 do
    for y := 0 to Height-1 do
      for x := 0 to Width-1 do
      begin
        if (x < Src.Width) and (y < Src.Height) then
          CPix := Src.Pixel[x, y]
        else
          CPix := 0;
        if (CPix and (1 shl b)) <> 0 then
          cb := cb + (1 shl (7 - (x mod 8)));
        if x mod 8 = 7 then
        begin
          Dst.Write(cb, 1);
          cb := 0;
        end;
      end;
end;

function TDosGraphicSet.LoadVgaspec(SpecFile: String): TBaseGraphicSet;
var
  i: Integer;
  SpecPal: DosVgaspecPaletteData;
  BPP18, CustSize: Boolean;
  W, H: Integer;
  MaxIter, BPP: Integer;
  TempBMP, TempBMP2: TBitmap32;
begin
  fCompressedDataFile.LoadFromFile(SpecFile);
  fCompressedDataFile.Seek(0, soFromBeginning);
  fDecompressor.DecompressSection(fCompressedDataFile, fTerrainGfxSection);

  Result := TBaseGraphicSet.Create;
  Result.Resolution := 8;

  if fCompressedDataFile.Position < fCompressedDataFile.Size then
  begin
    ShowMessage('This appears to be a new-format VGASPEC file. You can load it via File -> Load.');
    Exit;
  end;

  fTerrainGfxSection.Seek(0, soFromBeginning);
  fTerrainGfxSection.Read(SpecPal, SizeOf(SpecPal));
  BPP18 := (SpecPal.VgaPal[1].R = 255);
  if not BPP18 then
  begin
    BPP := 3;
    for i := 0 to 7 do
    begin
      fMetaInfo[0].Palettes.VgaPal[i].R := SpecPal.VgaPal[i].R;
      fMetaInfo[0].Palettes.VgaPal[i].G := SpecPal.VgaPal[i].G;
      fMetaInfo[0].Palettes.VgaPal[i].B := SpecPal.VgaPal[i].B;
    end;
  end else begin
    BPP := 18;
  end;

  CustSize := (SpecPal.EgaPal[1] = 255);
  if CustSize then
  begin
    W := (SpecPal.UnknownPal[0] shl 8) + SpecPal.UnknownPal[1];
    H := (SpecPal.UnknownPal[2] shl 8) + SpecPal.UnknownPal[3];
    MaxIter := 1;
  end else begin
    W := 960;
    H := 160;
    MaxIter := 4;
  end;

  TempBMP := TBitmap32.Create;
  TempBMP.SetSize(W, H);
  TempBMP.Clear(0);

  fObjectGfxSection.Seek(0, soFromBeginning);
  RemoveRLE(fTerrainGfxSection, fObjectGfxSection);

  fObjectGfxSection.Seek(0, soFromBeginning);

  for i := 0 to MaxIter-1 do
  begin
    TempBMP2 := LoadImage(-1, -1, W, H div MaxIter, BPP, fObjectGfxSection);
    TempBMP2.DrawTo(TempBMP, 0, 40*i);
    TempBMP2.Free;
  end;

  Result.MetaTerrains.Add(TMetaTerrain.Create);
  Result.TerrainImages.Add(TempBMP);
end;

procedure TDosGraphicSet.RemoveRLE(Src: TMemoryStream; Dst: TMemoryStream);
var
  b, c: Byte;
  i: Integer;
begin
  while true do
  begin
    Src.Read(c, 1);
    if Src.Position >= Src.Size then Exit;
    if c < $80 then
      for i := 0 to c do
      begin
        Src.Read(b, 1);
        Dst.Write(b, 1);
      end
    else if c > $80 then
    begin
      Src.Read(b, 1);
      for i := 256 downto c do
        Dst.Write(b, 1);
    end;
  end;
end;

function TDosGraphicSet.LoadGraphicSet(MetaInfoFile: String; GraphicFile: String): TBaseGraphicSet;
begin
  fMetaInfoFileStream.LoadFromFile(MetaInfoFile);
  fCompressedDataFile.LoadFromFile(GraphicFile);

  fMetaInfoFileStream.Seek(0, soFromBeginning);
  fCompressedDataFile.Seek(0, soFromBeginning);

  fDecompressor.DecompressSection(fCompressedDataFile, fTerrainGfxSection);
  fDecompressor.DecompressSection(fCompressedDataFile, fObjectGfxSection);

  Result := TBaseGraphicSet.Create;
  Result.Resolution := 8;
  LoadMetaData(Result);
  LoadImages(Result);
end;

procedure TDosGraphicSet.LoadMetaData(Target: TBaseGraphicSet);
var
  i: Integer;
  mis, mip: Integer;
  TempObj: TMetaObject;
  TempTer: TMetaTerrain;
  PvLoc, BaseLoc: Integer;
begin
  fMetaInfoFileStream.ReadBuffer(fMetaInfo[0], SizeOf(fMetaInfo[0]));
  if fMetaInfoFileStream.Size > 1056 then
    fMetaInfoFileStream.ReadBuffer(fMetaInfo[1], SizeOf(fMetaInfo[1]));

  for i := 0 to 7 do
    Target.KeyColors[i] := (fMetaInfo[0].Palettes.VgaPal[i].R shl 18)
                         + (fMetaInfo[0].Palettes.VgaPal[i].G shl 10)
                         + (fMetaInfo[0].Palettes.VgaPal[i].B shl 2)
                         + $FF000000;

  fSettingsFlags := TSettingsFlags(fMetaInfo[0].MetaObjects[0].GsOptions);

  for i := 0 to 31 do
  begin
    mis := i div 16;
    mip := i mod 16;
    with fMetaInfo[mis].MetaObjects[mip] do
    begin
      if ObjWidth = 0 then Break;

      //Direct copy-and-paste properties
      TempObj := TMetaObject.Create;
      TempObj.TriggerType := TriggerEff;
      TempObj.STriggerX := STriggerX;
      TempObj.STriggerY := STriggerY;
      TempObj.STriggerW := 1;
      TempObj.STriggerH := 1;
      TempObj.TriggerSound := TriggerSound;
      TempObj.KeyFrame := KeyFrame;

      //Special treatment for object #1 if needed
      if (not (dsfMultiWindow in fSettingsFlags)) and (i = 1) then
        TempObj.TriggerType := 23;

      //TempObj Primary Trigger Positions / Sizes
      TempObj.PTriggerX := PTriggerX * 4;
      TempObj.PTriggerY := PTriggerY * 4 - 4;
      TempObj.PTriggerW := PTriggerW * 4;
      TempObj.PTriggerH := PTriggerH * 4;
      if (dsfTriggerAdjust in fSettingsFlags) then
      begin
        TempObj.PTriggerX := TempObj.PTriggerX - (TriggerAdj shr 6);
        TempObj.PTriggerY := TempObj.PTriggerY - ((TriggerAdj shr 4) mod 4);
        TempObj.PTriggerW := TempObj.PTriggerW - ((TriggerAdj shr 2) mod 4);
        TempObj.PTriggerH := TempObj.PTriggerH - (TriggerAdj mod 4);
      end;

      //TempObj.TriggerAnim
      if FrameCount = 1 then
        TempObj.TriggerAnim := false
      else if (AnimFlags and 1) = 0 then
        TempObj.TriggerAnim := false
      else
        TempObj.TriggerAnim := true;

      //TempObj.PreviewFrame
      BaseLoc := BaseLocLow;
      if (dsfExtLocs in fSettingsFlags) then
        BaseLoc := BaseLoc + (BaseLocHigh shl 16);
      PvLoc := PreviewLoc;
      if PreviewLoc < BaseLoc then
        TempObj.PreviewFrame := 0
      else begin
        PvLoc := PvLoc - BaseLoc;
        TempObj.PreviewFrame := PvLoc div NextFrameOffset;
      end;

      //Special treatment of window trigger area if needed
      if (TempObj.TriggerType = 23) and (TempObj.PTriggerX = 0) and
         ((TempObj.PTriggerY = -4) or (TempObj.PTriggerY = 0)) then
      begin
        TempObj.PTriggerX := 24;
        TempObj.PTriggerY := 13;
        TempObj.PTriggerW := 1;
        TempObj.PTriggerH := 1;
      end;

      Target.MetaObjects.Add(TempObj);
    end;
  end;

  for i := 0 to 127 do
  begin
    mis := i div 64;
    mip := i mod 64;
    with fMetaInfo[mis].MetaTerrains[mip] do
    begin
      if TerWidth = 0 then Break;

      TempTer := TMetaTerrain.Create;
      TempTer.Steel := (((BaseLocHigh and 1) <> 0) and (dsfSteelMark in fSettingsFlags));

      Target.MetaTerrains.Add(TempTer);
    end;
  end;
end;

procedure TDosGraphicSet.LoadImages(Target: TBaseGraphicSet);
var
  i, i2: Integer;
  mis, mip: Integer;
  TempBitmap: TBitmap32;
  TempBitmapSet: TBitmaps;
  Loc, MaskLoc: Integer;
  BPP: Byte;
begin

  if (dsf19Bit in fSettingsFlags) then
    BPP := 19
  else if (dsf5Bit in fSettingsFlags) then
    BPP := 5
  else
    BPP := 4;

  for i := 0 to Target.MetaTerrains.Count-1 do
  begin
    mis := i div 64;
    mip := i mod 64;
    Loc := fMetaInfo[mis].MetaTerrains[mip].BaseLocLow;
    MaskLoc := fMetaInfo[mis].MetaTerrains[mip].MaskLocLow;
    if (dsfExtLocs in fSettingsFlags) then
    begin
      Loc := Loc + ((fMetaInfo[mis].MetaTerrains[mip].BaseLocHigh and $FE) shl 15);
      MaskLoc := MaskLoc + (fMetaInfo[mis].MetaTerrains[mip].MaskLocHigh shl 16);
    end;
    TempBitmap := LoadImage(Loc, MaskLoc, fMetaInfo[mis].MetaTerrains[mip].TerWidth, fMetaInfo[mis].MetaTerrains[mip].TerHeight, BPP, fTerrainGfxSection);
    Target.TerrainImages.Add(TempBitmap);
  end;

  for i := 0 to Target.MetaObjects.Count-1 do
  begin
    mis := i div 16;
    mip := i mod 16;
    Loc := fMetaInfo[mis].MetaObjects[mip].BaseLocLow;
    if (dsfExtLocs in fSettingsFlags) then
      Loc := Loc + (fMetaInfo[mis].MetaObjects[mip].BaseLocHigh shl 16);
    TempBitmapSet := TBitmaps.Create;
    for i2 := 0 to fMetaInfo[mis].MetaObjects[mip].FrameCount-1 do
    begin
      MaskLoc := Loc + fMetaInfo[mis].MetaObjects[mip].MaskOffset;
      TempBitmap := LoadImage(Loc, MaskLoc, fMetaInfo[mis].MetaObjects[mip].ObjWidth, fMetaInfo[mis].MetaObjects[mip].ObjHeight, BPP, fObjectGfxSection);
      TempBitmapSet.Add(TempBitmap);
      Loc := Loc + fMetaInfo[mis].MetaObjects[mip].NextFrameOffset;
    end;
    Target.ObjectImages.Add(TempBitmapSet);
  end;
end;

function TDosGraphicSet.LoadImage(Loc: Integer; MaskLoc: Integer; Width: Integer; Height: Integer; BPP: Byte; Src: TMemoryStream): TBitmap32;
var
  x, y, i: Integer;
  palindex: Byte;
  cb: Byte;
begin
  Result := TBitmap32.Create;
  Result.SetSize(Width, Height);
  Result.Clear(0);

  // First, let's just get the data out. We can remap palettes / apply transparency later.
  if Loc <> -1 then
    Src.Seek(Loc, soFromBeginning);

  for i := 0 to BPP-1 do
    for y := 0 to Height-1 do
      for x := 0 to Width-1 do
      begin
        if x mod 8 = 0 then Src.Read(cb, 1);
        if (cb and (1 shl (7 - (x mod 8)))) <> 0 then Result.Pixel[x, y] := Result.Pixel[x, y] + (1 shl i);
      end;

  // Now, de-palettize it and apply the mask if it's a 4 or 5 bit image
  if BPP in [3, 4, 5] then
  begin

    if MaskLoc <> -1 then
      Src.Seek(MaskLoc, soFromBeginning);

    cb := 255;

    for y := 0 to Height-1 do
      for x := 0 to Width-1 do
      begin
        if MaskLoc <> -1 then
          if x mod 8 = 0 then Src.Read(cb, 1);

        palindex := Result.Pixel[x, y];
        if (palindex = 7) and (BPP <> 3) then palindex := 8;
        if (palindex < 8) then
        begin
          if BPP = 3 then
            Result.Pixel[x, y] := (fMetaInfo[0].Palettes.VgaPal[palindex].R shl 2 shl 16)
                                + (fMetaInfo[0].Palettes.VgaPal[palindex].G shl 2 shl 8)
                                + (fMetaInfo[0].Palettes.VgaPal[palindex].B shl 2)
                                + $FF000000
          else if (dsfXmasPal in fSettingsFlags) then
            Result.Pixel[x, y] := FixedDosXmasPalette[palindex]
          else
            Result.Pixel[x, y] := FixedDosPalette[palindex];
        end else
          Result.Pixel[x, y] := (fMetaInfo[0].Palettes.VgaPal[palindex-8].R shl 2 shl 16)
                              + (fMetaInfo[0].Palettes.VgaPal[palindex-8].G shl 2 shl 8)
                              + (fMetaInfo[0].Palettes.VgaPal[palindex-8].B shl 2)
                              + $FF000000;
        if BPP = 3 then
        begin
          if (Result.Pixel[x, y] and $FFFFFF) = 0 then Result.Pixel[x, y] := 0;
        end else
          if (cb and (1 shl (7 - (x mod 8)))) = 0 then Result.Pixel[x, y] := 0;
      end;
  end;

  // If it's a 18-bit or 19-bit, this works a bit differently...
    if BPP in [18, 19] then
  begin
    for y := 0 to Height-1 do
      for x := 0 to Width-1 do
      begin
        if ((Result.Pixel[x, y] and $40000) = 0) and (BPP = 19) then
          Result.Pixel[x, y] := 0
        else begin
          Result.Pixel[x, y] := $FF000000
                              + ((Result.Pixel[x, y] and $3F000) shl 6)
                              + ((Result.Pixel[x, y] and $FC0) shl 4)
                              + ((Result.Pixel[x, y] and $3F) shl 2);
        end;
        if ((Result.Pixel[x, y] and $FFFFFF) = 0) and (BPP = 18) then
          Result.Pixel[x, y] := 0;
      end;
  end;
  
end;

end.