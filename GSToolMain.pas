unit GSToolMain;

// If you get errors about missing form components when opening this file in
// the Delphi IDE, make sure you have installed the Graphics32 design-time
// components. No other special design-type components are nessecary.
//
// This tool was designed in Delphi 7, and compatibility with other releases
// is not guaranteed.

interface

uses
  LemGraphicSet, // Internal graphic set format

  GSLoadExperimental,
  GSLoadDOS, GSLoadCheapo, GSLoadLemset, GSLoadNeoLemmix, GSLoadLemmini, // Graphic set loaders

  PngInterface, // Yep, seems it's needed in the main form too

  FImportStrip, FSoundMgr, FObjectAnimPreview, OpenSaveDlgs, FMassName,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, // Delphi libraries
  Dialogs, StdCtrls, ExtCtrls, ToolWin, Menus, GR32_Image, GR32, StrUtils;

const
  TVersion: String = '10.13.14:2465bbb';  // update in GSLoadNeoLemmix no longer needed

type
  TF_GSTool = class(TForm)
    lbTerrains: TListBox;
    Label1: TLabel;
    lbObjects: TListBox;
    Label2: TLabel;
    Label3: TLabel;
    btnAddTerrain: TButton;
    btnDelTerrain: TButton;
    btnAddObject: TButton;
    btnDelObject: TButton;
    cbSteel: TCheckBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    cmbObjectType: TComboBox;
    btnPrevFrame: TButton;
    btnNextFrame: TButton;
    btnDelFrame: TButton;
    btnAddFrame: TButton;
    txtFrame: TLabel;
    btnLoadFrame: TButton;
    btnSaveFrame: TButton;
    Label7: TLabel;
    cbDisplayTrigger: TCheckBox;
    Label9: TLabel;
    ebPtrigX: TEdit;
    ebPtrigY: TEdit;
    Label10: TLabel;
    Label11: TLabel;
    ebPtrigW: TEdit;
    ebPtrigH: TEdit;
    Label12: TLabel;
    Label18: TLabel;
    ebPreviewFrame: TEdit;
    Label19: TLabel;
    Label20: TLabel;
    ebKeyFrame: TEdit;
    btnPrevFrameCurrent: TButton;
    btnKeyframeCurrent: TButton;
    Label21: TLabel;
    cmbSoundEffect: TComboBox;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    New1: TMenuItem;
    Load1: TMenuItem;
    SaveAs1: TMenuItem;
    Exit1: TMenuItem;
    N1: TMenuItem;
    ools1: TMenuItem;
    ImportFromOldFormat1: TMenuItem;
    Label22: TLabel;
    SuperLemmini1: TMenuItem;
    Export1: TMenuItem;
    LemSet2: TMenuItem;
    imgPiece: TImage32;
    Full1: TMenuItem;
    Help1: TMenuItem;
    About1: TMenuItem;
    Label24: TLabel;
    cmbKeyColors: TComboBox;
    imgKeyColor: TImage32;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    ebKCR: TEdit;
    ebKCG: TEdit;
    ebKCB: TEdit;
    cbBlackBg: TCheckBox;
    btnInsTerrain: TButton;
    btnInsertObject: TButton;
    btnInsFrame: TButton;
    btnTerUp: TButton;
    btnTerDown: TButton;
    btnObjUp: TButton;
    btnObjDown: TButton;
    cbZoomImage: TCheckBox;
    btnImportStrip: TButton;
    btnExportStrip: TButton;
    VGASPEC1: TMenuItem;
    cbRandomFrame: TCheckBox;
    ools2: TMenuItem;
    CustomSoundManager1: TMenuItem;
    CheapoSTYfile1: TMenuItem;
    btnAnimPreview: TButton;
    Label28: TLabel;
    Label29: TLabel;
    ebPieceName: TEdit;
    ExperimentalFormat1: TMenuItem;
    MassStuffDoer1: TMenuItem;
    ExperimentalVGASPEC1: TMenuItem;
    Save1: TMenuItem;
    cbResizeHorz: TCheckBox;
    cbResizeVert: TCheckBox;
    MassRename1: TMenuItem;
    Label8: TLabel;
    cbLemmingSprites: TComboBox;
    N2: TMenuItem;
    StyleNames1: TMenuItem;
    procedure ImportFromOldFormat1Click(Sender: TObject);
    procedure lbTerrainsClick(Sender: TObject);
    procedure lbObjectsClick(Sender: TObject);
    procedure About1Click(Sender: TObject);
    procedure LemSet2Click(Sender: TObject);
    procedure New1Click(Sender: TObject);
    procedure Full1Click(Sender: TObject);
    procedure cbSteelClick(Sender: TObject);
    procedure cmbObjectTypeChange(Sender: TObject);
    procedure rbAnimSetting1Click(Sender: TObject);
    procedure rbAnimSetting2Click(Sender: TObject);
    procedure cmbSoundEffectChange(Sender: TObject);
    procedure ebPtrigXChange(Sender: TObject);
    procedure ebPtrigYChange(Sender: TObject);
    procedure ebPtrigWChange(Sender: TObject);
    procedure ebPtrigHChange(Sender: TObject);
    procedure ebPreviewFrameChange(Sender: TObject);
    procedure ebKeyFrameChange(Sender: TObject);
    procedure cmbKeyColorsChange(Sender: TObject);
    procedure ebKCxChange(Sender: TObject);
    procedure cbDisplayTriggerClick(Sender: TObject);
    procedure cbBlackBgClick(Sender: TObject);
    procedure btnPrevFrameClick(Sender: TObject);
    procedure btnNextFrameClick(Sender: TObject);
    procedure btnPrevFrameCurrentClick(Sender: TObject);
    procedure btnKeyframeCurrentClick(Sender: TObject);
    procedure btnLoadFrameClick(Sender: TObject);
    procedure btnSaveFrameClick(Sender: TObject);
    procedure btnDelTerrainClick(Sender: TObject);
    procedure btnAddTerrainClick(Sender: TObject);
    procedure btnInsTerrainClick(Sender: TObject);
    procedure btnAddObjectClick(Sender: TObject);
    procedure btnInsertObjectClick(Sender: TObject);
    procedure btnDelObjectClick(Sender: TObject);
    procedure btnDelFrameClick(Sender: TObject);
    procedure btnAddFrameClick(Sender: TObject);
    procedure btnInsFrameClick(Sender: TObject);
    procedure btnTerUpClick(Sender: TObject);
    procedure btnTerDownClick(Sender: TObject);
    procedure btnObjUpClick(Sender: TObject);
    procedure btnObjDownClick(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure SaveAs1Click(Sender: TObject);
    procedure Load1Click(Sender: TObject);
    procedure btnImportStripClick(Sender: TObject);
    procedure btnExportStripClick(Sender: TObject);
    procedure VGASPEC1Click(Sender: TObject);
    procedure SuperLemmini1Click(Sender: TObject);
    procedure SuperLemmini2Click(Sender: TObject);
    procedure cbRandomFrameClick(Sender: TObject);
    procedure CustomSoundManager1Click(Sender: TObject);
    procedure CheapoSTYfile1Click(Sender: TObject);
    procedure btnAnimPreviewClick(Sender: TObject);
    procedure ebPieceNameChange(Sender: TObject);
    procedure ExperimentalFormat1Click(Sender: TObject);
    procedure MassStuffDoer1Click(Sender: TObject);
    procedure ExperimentalVGASPEC1Click(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure cbResizeHorzClick(Sender: TObject);
    procedure cbResizeVertClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure MassRename1Click(Sender: TObject);
    procedure cbLemmingSpritesChange(Sender: TObject);
    procedure cbLemmingSpritesExit(Sender: TObject);
    procedure StyleNames1Click(Sender: TObject);
  private
    fSaveCheckStream: TMemoryStream;
    fFilename: String;
    fLastDirectory: String;
    fSoundPanel: TForm_SoundMgr;
    fLemminiWarning: Boolean;
    FrameNumber: Integer;
    GraphicSet: TBaseGraphicSet;
    procedure DoMassStuff(aFile: String);
    procedure DoMassConvert(aFile: String);
    procedure UpdateNewGS;
    procedure UpdateLists(FullRefresh: Boolean = true); 
    procedure ClearDisableAll;
    procedure RefreshObjectSettings;
    procedure SetRGBTexts;
    procedure DisplayObjectImage;
    procedure ScaleDisplayImage;
    procedure AddTerrain(loc: Integer);
    procedure AddObject(loc: Integer);
    procedure AddFrame(loc: Integer);
    function QuickVgagrName(fn: String): String;
    function SoundToIndex(aSound: Byte): Byte;
    function IndexToSound(aIndex: Byte): Byte;
    function SelectedTerrain: TMetaTerrain;
    function SelectedObject: TMetaObject;
    function SelectedTerrainNumber: Integer;
    function SelectedObjectNumber: Integer;
    procedure AfterGSLoad;
    function CheckIfSaved: Boolean;
    function AskToSave: Boolean; // true = continue with action, false = abort action
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure OnHideSoundForm(Sender: TObject);
  end;

  procedure DeleteDirectory(const Name: string);

var
  F_GSTool: TF_GSTool;

implementation

{$R *.dfm}

constructor TF_GSTool.Create(AOwner: TComponent);
begin
  inherited;
  GraphicSet := TBaseGraphicSet.Create;
  imgKeyColor.Bitmap.SetSize(imgKeyColor.Width, imgKeyColor.Height);
  fLemminiWarning := false;
  fSoundPanel := TForm_SoundMgr.Create(self);
  fSoundPanel.GS := GraphicSet;
  fSoundPanel.OnHide := OnHideSoundForm;
  fLastDirectory := ExtractFilePath(ParamStr(0));
  fFilename := '';
  fSaveCheckStream := TMemoryStream.Create;
  UpdateNewGS;
end;

destructor TF_GSTool.Destroy;
begin
  GraphicSet.Free;
  fSoundPanel.Free;
  fSaveCheckStream.Free;
  inherited;
end;

function TF_GSTool.CheckIfSaved;
var
  TempStream: TMemoryStream;

  function CompareStreams: Boolean;
  var
    b1, b2: Byte;
  begin
    Result := false;

    if TempStream.Size <> fSaveCheckStream.Size then
      Exit;

    TempStream.Position := 0;
    fSaveCheckStream.Position := 0;

    while (TempStream.Position < TempStream.Size) and (fSaveCheckStream.Position < fSaveCheckStream.Size) do // check both just in case
    begin
      TempStream.Read(b1, 1);
      fSaveCheckStream.Read(b2, 1);
      if b1 <> b2 then Exit;
    end;

    Result := true;
  end;
begin
  if (GraphicSet.MetaTerrains.Count = 0) and (GraphicSet.MetaObjects.Count = 0) then
  begin
    Result := true;
    Exit;
  end;

  if fFilename = '' then
  begin
    Result := false;
    Exit;
  end;

  TempStream := TMemoryStream.Create;
  TempStream.Position := 0;

  GraphicSet.CreateSaveTest(TempStream);

  Result := CompareStreams;

  TempStream.Free;
end;

procedure TF_GSTool.ScaleDisplayImage;
var
  TempBmp: TBitmap32;
  x, y, w, h: Integer;
  xx, yy: Integer;
begin
  TempBmp := TBitmap32.Create;
  TempBmp.Assign(imgPiece.Bitmap);
  w := imgPiece.Bitmap.Width;
  h := imgPiece.Bitmap.Height;
  while (w > imgPiece.Width)
     or (h > imgPiece.Height) do
  begin
    w := w div 2;
    h := h div 2;
  end;

  if cbZoomImage.Checked then
    while (w * 2 < imgPiece.Width)
      and (h * 2 < imgPiece.Height) do
    begin
      w := w * 2;
      h := h * 2;
    end;

  imgPiece.Bitmap.SetSize(imgPiece.Width, imgPiece.Height);
  imgPiece.Bitmap.Clear(ColorToRGB(imgPiece.Color));

  x := (imgPiece.Bitmap.Width - w) div 2;
  y := (imgPiece.Bitmap.Height - h) div 2;

  if not cbBlackBg.Checked then
    for yy := 0 to TempBmp.Height-1 do
      for xx := 0 to TempBmp.Width-1 do
        if (TempBmp.Pixel[xx, yy] and $FF000000) = 0 then
          TempBmp.Pixel[xx, yy] := ColorToRGB(imgPiece.Color);


  TempBmp.DrawTo(imgPiece.Bitmap, Rect(x, y, x+w, y+h));
  imgPiece.Update;
  TempBmp.Free;
end;

procedure TF_GSTool.UpdateNewGS;
begin
  AfterGSLoad;
  if Lowercase(GraphicSet.LemmingSprites) = 'lemming' then GraphicSet.LemmingSprites := 'default';
  if Lowercase(GraphicSet.LemmingSprites) = 'xlemming' then GraphicSet.LemmingSprites := 'xmas';
  cbLemmingSprites.Text := GraphicSet.LemmingSprites;
  cmbKeyColors.ItemIndex := 0;
  imgKeyColor.Bitmap.Clear(GraphicSet.KeyColors[0]);
  SetRGBTexts;
  UpdateLists;
  fSoundPanel.GS := GraphicSet;
  fSaveCheckStream.Clear;
  GraphicSet.CreateSaveTest(fSaveCheckStream);
end;

procedure TF_GSTool.UpdateLists(FullRefresh: Boolean = true);
var
  i, i2: Integer;
  s: String;
  mw, mh: Integer;
begin

  if FullRefresh then
  begin
    lbObjects.ItemIndex := -1;
    lbTerrains.ItemIndex := -1;
    lbTerrains.Items.Clear;
  end;

  for i := 0 to GraphicSet.MetaTerrains.Count-1 do
  begin
    if GraphicSet.MetaTerrains[i].Name = '' then
      s := 'Piece ' + IntToStr(i)
    else
      s := GraphicSet.MetaTerrains[i].Name;
    s := s +
         ' (' + IntToStr(GraphicSet.TerrainImages[i].Width) + 'x' + IntToStr(GraphicSet.TerrainImages[i].Height);
    if GraphicSet.MetaTerrains[i].Steel then s := s + ', Steel';
    s := s + ')';
    if FullRefresh then
      lbTerrains.Items.Add(s)
      else
      lbTerrains.Items[i] := s;
  end;

  if FullRefresh then lbObjects.Items.Clear;

  for i := 0 to GraphicSet.MetaObjects.Count-1 do
  begin
    mh := 0;
    mw := 0;
    for i2 := 0 to GraphicSet.ObjectImages[i].Count-1 do
    begin
      if GraphicSet.ObjectImages[i][i2].Width > mw then mw := GraphicSet.ObjectImages[i][i2].Width;
      if GraphicSet.ObjectImages[i][i2].Height > mh then mh := GraphicSet.ObjectImages[i][i2].Height;
    end;

    if GraphicSet.MetaObjects[i].Name = '' then
      s := '#' + IntToStr(i) + ', ' + cmbObjectType.Items[GraphicSet.MetaObjects[i].TriggerType]
    else
      s := GraphicSet.MetaObjects[i].Name;
    s := s +
         ' (' + IntToStr(mw) + 'x' + IntToStr(mh) + ', ' + IntToStr(GraphicSet.ObjectImages[i].Count) + 'f)';
    if FullRefresh then
      lbObjects.Items.Add(s)
      else
      lbObjects.Items[i] := s;
  end;

  if FullRefresh then ClearDisableAll;
  
end;

procedure TF_GSTool.ClearDisableAll;
begin
  txtFrame.Caption := '';
  ebPieceName.Text := '';
  ebPieceName.Enabled := false;
  cbSteel.Checked := false;
  cbSteel.Enabled := false;
  cmbObjectType.ItemIndex := -1;
  cmbObjectType.Enabled := false;
  cbResizeHorz.Checked := false;
  cbResizeHorz.Enabled := false;
  cbResizeVert.Checked := false;
  cbResizeVert.Enabled := false;
  ebPtrigX.Text := '0';
  ebPtrigX.Enabled := false;
  ebPtrigY.Text := '0';
  ebPtrigY.Enabled := false;
  ebPtrigW.Text := '0';
  ebPtrigW.Enabled := false;
  ebPtrigH.Text := '0';
  ebPtrigH.Enabled := false;
  ebPreviewFrame.Text := '1';
  ebPreviewFrame.Enabled := false;
  ebKeyFrame.Text := '1';
  ebKeyFrame.Enabled := false;
  cmbSoundEffect.ItemIndex := -1;
  cmbSoundEffect.Enabled := false;
  btnPrevFrameCurrent.Enabled := false;
  btnKeyFrameCurrent.Enabled := false;
  btnNextFrame.Enabled := false;
  btnPrevFrame.Enabled := false;
  btnAddFrame.Enabled := false;
  btnInsFrame.Enabled := false;
  btnDelFrame.Enabled := false;
  btnLoadFrame.Enabled := false;
  btnSaveFrame.Enabled := false;
  btnImportStrip.Enabled := false;
  btnExportStrip.Enabled := false;
  btnDelTerrain.Enabled := false;
  btnDelObject.Enabled := false;
  btnTerUp.Enabled := false;
  btnTerDown.Enabled := false;
  btnObjUp.Enabled := false;
  btnObjDown.Enabled := false;
  btnAnimPreview.Enabled := false;
  cbRandomFrame.Checked := false;
  cbRandomFrame.Enabled := false;
  imgPiece.Bitmap.Clear(ColorToRGB(imgPiece.Color));
end;

procedure TF_GSTool.ImportFromOldFormat1Click(Sender: TObject);
var
  OpenDlg: TOpenDialog;
  grFn, vgFn: String;
  DosGFXLoader: TDosGraphicSet;
begin
  if not AskToSave then Exit;

  OpenDlg := TOpenDialog.Create(self);
  OpenDlg.Options := [ofFileMustExist];

  OpenDlg.Title := 'Select GROUNDxO / G_xxxx File';
  OpenDlg.Filter := 'Graphic Set Metainfo File|g_*.dat;ground*o.dat';
  if fLastDirectory <> '' then OpenDlg.InitialDir := fLastDirectory;
  if not OpenDlg.Execute then
  begin
    OpenDlg.Free;
    Exit;
  end;
  fLastDirectory := ExtractFilePath(OpenDlg.FileName);
  grFn := OpenDlg.FileName;

  OpenDlg.FileName := QuickVgagrName(grFn);

  OpenDlg.Title := 'Select VGAGRx / V_xxxx File';
  OpenDlg.Filter := 'Graphic Set Images File|v_*.dat;vgagr*.dat';
  if fLastDirectory <> '' then OpenDlg.InitialDir := fLastDirectory; // don't really need it for this one but why not
  if not OpenDlg.Execute then
  begin
    OpenDlg.Free;
    Exit;
  end;
  fLastDirectory := ExtractFilePath(OpenDlg.FileName);
  vgFn := OpenDlg.FileName;

  OpenDlg.Free;

  DosGFXLoader := TDosGraphicSet.Create(self);

  GraphicSet.Free;
  GraphicSet := DosGFXLoader.LoadGraphicSet(grFn, vgFn);

  fFilename := '';

  DosGFXLoader.Free;
  UpdateNewGS;
end;

procedure TF_GSTool.lbTerrainsClick(Sender: TObject);
var
  T: TMetaTerrain;
  Inv, Flip, Rotate: Boolean;
begin
  txtFrame.Caption := '';
  lbObjects.ItemIndex := -1;
  T := GraphicSet.MetaTerrains[lbTerrains.ItemIndex];
  ebPieceName.Text := T.Name;
  ebPieceName.Enabled := true;
  cbSteel.Checked := T.Steel;
  cbSteel.Enabled := true;
  cmbObjectType.ItemIndex := -1;
  cmbObjectType.Enabled := false;
  cbResizeHorz.Checked := false;
  cbResizeHorz.Enabled := false;
  cbResizeVert.Enabled := false;
  cbResizeVert.Checked := false;
  ebPtrigX.Text := '0';
  ebPtrigX.Enabled := false;
  ebPtrigY.Text := '0';
  ebPtrigY.Enabled := false;
  ebPtrigW.Text := '0';
  ebPtrigW.Enabled := false;
  ebPtrigH.Text := '0';
  ebPtrigH.Enabled := false;
  ebPreviewFrame.Text := '1';
  ebPreviewFrame.Enabled := false;
  ebKeyFrame.Text := '1';
  ebKeyFrame.Enabled := false;
  cbRandomFrame.Checked := false;
  cbRandomFrame.Enabled := false;
  cmbSoundEffect.ItemIndex := -1;
  cmbSoundEffect.Enabled := false;
  btnPrevFrameCurrent.Enabled := false;
  btnKeyFrameCurrent.Enabled := false;
  btnNextFrame.Enabled := false;
  btnPrevFrame.Enabled := false;
  btnAddFrame.Enabled := false;
  btnInsFrame.Enabled := false;
  btnDelFrame.Enabled := false;
  btnLoadFrame.Enabled := true;
  btnSaveFrame.Enabled := true;
  btnImportStrip.Enabled := false;
  btnExportStrip.Enabled := false;
  btnDelTerrain.Enabled := true;
  btnDelObject.Enabled := false;
  btnTerUp.Enabled := (SelectedTerrainNumber > 0);
  btnTerDown.Enabled := (SelectedTerrainNumber < GraphicSet.MetaTerrains.Count-1);
  btnObjUp.Enabled := false;
  btnObjDown.Enabled := false;
  btnAnimPreview.Enabled := false;
  imgPiece.Bitmap.Assign(GraphicSet.TerrainImages[SelectedTerrainNumber]);
  ScaleDisplayImage;
end;

procedure TF_GSTool.lbObjectsClick(Sender: TObject);
begin
  FrameNumber := 0;

  lbTerrains.ItemIndex := -1;
  cbSteel.Checked := false;
  cbSteel.Enabled := false;
  btnDelTerrain.Enabled := false;

  RefreshObjectSettings;

  btnAddFrame.Enabled := true;
  btnInsFrame.Enabled := true;
  btnLoadFrame.Enabled := true;
  btnSaveFrame.Enabled := true;
  btnImportStrip.Enabled := true;
  btnExportStrip.Enabled := true;
  btnDelTerrain.Enabled := false;
  btnDelObject.Enabled := true;
  btnAnimPreview.Enabled := true;
  btnTerUp.Enabled := false;
  btnTerDown.Enabled := false;
  DisplayObjectImage;
end;

procedure TF_GSTool.DisplayObjectImage;
var
  TempBMP: TBitmap32;
begin
  txtFrame.Caption := IntToStr(FrameNumber+1) + '/'
                    + IntToStr(GraphicSet.ObjectImages[SelectedObjectNumber].Count);

  imgPiece.Bitmap.Assign(GraphicSet.ObjectImages[SelectedObjectNumber][FrameNumber]);
  if cbDisplayTrigger.Checked then
  begin
    TempBmp := TBitmap32.Create;
    TempBmp.DrawMode := dmBlend;
    TempBmp.CombineMode := cmMerge;

    if not (SelectedObject.TriggerType in [0, 25, 30, 32]) then
    begin
      if not (SelectedObject.TriggerType in [12, 13, 23]) then
        TempBmp.SetSize(SelectedObject.PTriggerW, SelectedObject.PTriggerH)
      else
        TempBmp.SetSize(1, 1);
      TempBmp.Clear($80FF00FF);
      TempBmp.DrawTo(imgPiece.Bitmap, SelectedObject.PTriggerX, SelectedObject.PTriggerY);
    end;

    TempBmp.Free;
  end;
  ScaleDisplayImage;
end;

procedure TF_GSTool.RefreshObjectSettings;
var
  MO: TMetaObject;
  Flip, Inv: Boolean;
begin
  MO := SelectedObject;

  cmbObjectType.ItemIndex := MO.TriggerType;
  cmbObjectType.Enabled := true;

  cbResizeHorz.Enabled := true;
  cbResizeHorz.Checked := MO.ResizeHorizontal;

  cbResizeVert.Enabled := true;
  cbResizeVert.Checked := MO.ResizeVertical;

  if not (MO.TriggerType in [0, 25, 30, 32]) then
  begin
    ebPtrigX.Text := IntToStr(MO.PTriggerX);
    ebPtrigX.Enabled := true;
    ebPtrigY.Text := IntToStr(MO.PTriggerY);
    ebPtrigY.Enabled := true;
  end else begin
    ebPtrigX.Text := '0';
    ebPtrigX.Enabled := false;
    ebPtrigY.Text := '0';
    ebPtrigY.Enabled := false;
  end;

  if not (MO.TriggerType in [0, 12, 13, 23, 25, 30, 32]) then
  begin
    ebPtrigW.Text := IntToStr(MO.PTriggerW);
    ebPtrigW.Enabled := true;
    ebPtrigH.Text := IntToStr(MO.PTriggerH);
    ebPtrigH.Enabled := true;
  end else begin
    ebPtrigW.Text := '0';
    ebPtrigW.Enabled := false;
    ebPtrigH.Text := '0';
    ebPtrigH.Enabled := false;
  end;

  if not (MO.TriggerType in [13, 16, 25, 32]) then
  begin
    ebPreviewFrame.Text := IntToStr(MO.PreviewFrame + 1);
    ebPreviewFrame.Enabled := true;
    btnPrevFrameCurrent.Enabled := true;
  end else begin
    ebPreviewFrame.Text := '1';
    ebPreviewFrame.Enabled := false;
    btnPrevFrameCurrent.Enabled := false;
  end;

  if (MO.TriggerType in [11, 12, 28, 29]) then
  begin
    ebKeyFrame.Text := IntToStr(MO.KeyFrame + 1);
    ebKeyFrame.Enabled := true;
    btnKeyFrameCurrent.Enabled := true;
  end else begin
    ebKeyFrame.Text := '1';
    ebKeyFrame.Enabled := false;
    btnKeyFrameCurrent.Enabled := false;
  end;

  if (MO.TriggerType in [4, 11, 15, 17, 24, 28, 29, 31]) then
  begin
    cmbSoundEffect.ItemIndex := SoundToIndex(MO.TriggerSound);
    cmbSoundEffect.Enabled := true;
  end else begin
    cmbSoundEffect.ItemIndex := -1;
    cmbSoundEffect.Enabled := false;
  end;

  if (MO.TriggerType in [0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 18, 19, 20, 22, 25, 26, 27, 30]) then
  begin
    cbRandomFrame.Enabled := true;
    cbRandomFrame.Checked := MO.RandomFrame;
  end else begin
    cbRandomFrame.Enabled := false;
    cbRandomFrame.Checked := false;
  end;

  ebPieceName.Text := MO.Name;
  ebPieceName.Enabled := true;

  btnNextFrame.Enabled := (FrameNumber < GraphicSet.ObjectImages[SelectedObjectNumber].Count-1);
  btnPrevFrame.Enabled := (FrameNumber > 0);
  btnDelFrame.Enabled := (GraphicSet.ObjectImages[SelectedObjectNumber].Count > 1);
  btnObjUp.Enabled := (SelectedObjectNumber > 0);
  btnObjDown.Enabled := (SelectedObjectNumber < GraphicSet.MetaObjects.Count-1);
end;

function TF_GSTool.SoundToIndex(aSound: Byte): Byte;
begin
  case aSound of
     9: Result := 1;
     7: Result := 2;
    14: Result := 3;
    15: Result := 4;
     6: Result := 5;
    13: Result := 6;
    20: Result := 7;
    21: Result := 8;
    22: Result := 9;
    else if (aSound > 22) and (aSound < 33) then
           Result := aSound - 13
         else
           Result := 0;
  end;
end;

function TF_GSTool.IndexToSound(aIndex: Byte): Byte;
begin
  case aIndex of
    1: Result := 9;
    2: Result := 7;
    3: Result := 14;
    4: Result := 15;
    5: Result := 6;
    6: Result := 13;
    7: Result := 20;
    8: Result := 21;
    9: Result := 22;
    else if (aIndex > 9) and (aIndex < 20) then
           Result := aIndex + 13
         else
           Result := 0;
  end;
end;

procedure TF_GSTool.About1Click(Sender: TObject);
var
  S: String;
begin
  // "About" Dialog
  S :=
       'NeoLemmix Graphic Set Tool V' + TVersion + #13 +
       'by namida' + #13 +
       #13 +
       'Thanks to...' + #13 +
       'All developers / contributors of Graphics32 library' + #13 +
       'Anders Melander for GIFImage library' + #13 +
       'Brent Sherwood for Delphi zLib library' + #13 +
       'ccexplore for documentation of DOS Lemmings formats' + #13 +
       'EricLang and Mindless for DOS .DAT file routines' + #13 +
       'Gustavo Huffenbacher Daud for PngDelphi library' + #13 +
       'Stefan Haymann for XML Parser library';
  ShowMessage(S);
end;

{procedure TF_GSTool.CheapoExtracted1Click(Sender: TObject);
var
  tarPath: String;
  OpenDlg: TOpenDialog;
  CheapoLoader: TCheapoGraphicSet;
begin
  OpenDlg := TOpenDialog.Create(self);
  OpenDlg.Title := 'Select terrain or objects XML file';
  OpenDlg.Filter := 'Cheapo Style XML File|graphics.sprites.xml;objects.sprites.xml;objects.objects.xml;';
  if not OpenDlg.Execute then
  begin
    OpenDlg.Free;
    Exit;
  end;
  tarPath := OpenDlg.FileName;

  CheapoLoader := TCheapoGraphicSet.Create(self);

  GraphicSet.Free;
  GraphicSet := CheapoLoader.LoadGraphicSet(tarPath);

  UpdateNewGS;
  CheapoLoader.Free;
end;}

procedure TF_GSTool.LemSet2Click(Sender: TObject);
var
  tarPath: String;
  OpenDlg: TOpenDialog;
  LemsetLoader: TLemsetGraphicSet;
begin
  if not AskToSave then Exit;
  
  OpenDlg := TOpenDialog.Create(self);
  OpenDlg.Title := 'Select style.ini file';
  OpenDlg.Filter := 'style.ini File|style.ini;';
  if fLastDirectory <> '' then OpenDlg.InitialDir := fLastDirectory;
  if not OpenDlg.Execute then
  begin
    OpenDlg.Free;
    Exit;
  end;
  fLastDirectory := ExtractFilePath(OpenDlg.FileName);
  tarPath := OpenDlg.FileName;

  LemsetLoader := TLemsetGraphicSet.Create(self);

  GraphicSet.Free;
  GraphicSet := LemsetLoader.LoadGraphicSet(tarPath);

  fFilename := '';

  UpdateNewGS;
  LemsetLoader.Free;
end;

procedure TF_GSTool.New1Click(Sender: TObject);
begin
  if not AskToSave then Exit;
  GraphicSet.Free;
  GraphicSet := TBaseGraphicSet.Create;
  fFilename := '';
  UpdateNewGS;
end;

procedure TF_GSTool.Full1Click(Sender: TObject);
var
  tarPath: String;
  SaveDlg: TSaveDialog;
  LemsetLoader: TLemsetGraphicSet;
begin
  SaveDlg := TSaveDialog.Create(self);
  SaveDlg.Title := 'Choose location for style.ini file';
  SaveDlg.Filter := 'style.ini File|style.ini';
  if fLastDirectory <> '' then SaveDlg.InitialDir := fLastDirectory;
  if not SaveDlg.Execute then
  begin
    SaveDlg.Free;
    Exit;
  end;
  fLastDirectory := ExtractFilePath(SaveDlg.FileName);
  tarPath := SaveDlg.FileName;

  LemsetLoader := TLemsetGraphicSet.Create(self);
  LemsetLoader.SaveGraphicSet(tarPath, GraphicSet);
  LemsetLoader.Free;
end;

function TF_GSTool.QuickVgagrName(fn: String): String;
begin
  fn := ExtractFileName(fn);
  if LowerCase(LeftStr(fn, 2)) = 'g_' then
    Result := StringReplace(fn, 'g_', 'v_', [rfIgnoreCase])
  else if LowerCase(LeftStr(fn, 6)) = 'ground' then
  begin
    Result := StringReplace(fn, 'ground', 'vgagr', [rfIgnoreCase]);
    Result := StringReplace(Result, 'o.dat', '.dat', [rfIgnoreCase]);
  end else
    Result := '';
end;

function TF_GSTool.SelectedTerrainNumber: Integer;
begin
  Result := lbTerrains.ItemIndex;
end;

function TF_GSTool.SelectedObjectNumber: Integer;
begin
  Result := lbObjects.ItemIndex;
end;

function TF_GSTool.SelectedTerrain: TMetaTerrain;
begin
  if SelectedTerrainNumber = -1 then
    Result := nil
    else
    Result := GraphicSet.MetaTerrains[SelectedTerrainNumber];
end;

function TF_GSTool.SelectedObject: TMetaObject;
begin
  if SelectedObjectNumber = -1 then
    Result := nil
    else
    Result := GraphicSet.MetaObjects[SelectedObjectNumber];
end;

procedure TF_GSTool.cbSteelClick(Sender: TObject);
begin
  if SelectedTerrain = nil then Exit;
  SelectedTerrain.Steel := cbSteel.Checked;
  UpdateLists(false);
end;

procedure TF_GSTool.cmbObjectTypeChange(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  SelectedObject.TriggerType := cmbObjectType.ItemIndex;
  UpdateLists(false);
  RefreshObjectSettings;
end;

procedure TF_GSTool.rbAnimSetting1Click(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  SelectedObject.TriggerAnim := false;
  RefreshObjectSettings;
end;

procedure TF_GSTool.rbAnimSetting2Click(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  SelectedObject.TriggerAnim := true;
  RefreshObjectSettings;
end;

procedure TF_GSTool.cmbSoundEffectChange(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  SelectedObject.TriggerSound := IndexToSound(cmbSoundEffect.ItemIndex);
end;

procedure TF_GSTool.ebPtrigXChange(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  with SelectedObject do
  begin
    PTriggerX := StrToIntDef(ebPtrigX.Text, PTriggerX);
    ebPtrigX.Text := IntToStr(PTriggerX);
  end;
  if cbDisplayTrigger.Checked then DisplayObjectImage;
end;

procedure TF_GSTool.ebPtrigYChange(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  with SelectedObject do
  begin
    PTriggerY := StrToIntDef(ebPtrigY.Text, PTriggerY);
    ebPtrigY.Text := IntToStr(PTriggerY);
  end;
  if cbDisplayTrigger.Checked then DisplayObjectImage;
end;

procedure TF_GSTool.ebPtrigWChange(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  with SelectedObject do
  begin
    PTriggerW := StrToIntDef(ebPtrigW.Text, PTriggerW);
    ebPtrigW.Text := IntToStr(PTriggerW);
  end;
  if cbDisplayTrigger.Checked then DisplayObjectImage;
end;

procedure TF_GSTool.ebPtrigHChange(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  with SelectedObject do
  begin
    PTriggerH := StrToIntDef(ebPtrigH.Text, PTriggerH);
    ebPtrigH.Text := IntToStr(PTriggerH);
  end;
  if cbDisplayTrigger.Checked then DisplayObjectImage;
end;

procedure TF_GSTool.ebPreviewFrameChange(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  with SelectedObject do
  begin
    PreviewFrame := StrToIntDef(ebPreviewFrame.Text, PreviewFrame+1) - 1;
    ebPreviewFrame.Text := IntToStr(PreviewFrame+1);
  end;
end;

procedure TF_GSTool.ebKeyFrameChange(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  with SelectedObject do
  begin
    KeyFrame := StrToIntDef(ebKeyFrame.Text, KeyFrame+1) - 1;
    ebKeyFrame.Text := IntToStr(KeyFrame+1);
  end;
end;

procedure TF_GSTool.cmbKeyColorsChange(Sender: TObject);
begin
  imgKeyColor.Bitmap.Clear(GraphicSet.KeyColors[cmbKeyColors.ItemIndex]);
  imgKeyColor.Update;
  SetRGBTexts;
end;

procedure TF_GSTool.ebKCxChange(Sender: TObject);
var
  r, g, b: Byte;
  c: TColor32;
begin
  c := GraphicSet.KeyColors[cmbKeyColors.ItemIndex];
  r := (c and $FF0000) shr 16;
  g := (c and $FF00) shr 8;
  b := (c and $FF);
  r := StrToIntDef(ebKCR.Text, r);
  g := StrToIntDef(ebKCG.Text, g);
  b := StrToIntDef(ebKCB.Text, b);
  c := $FF shl 24
     + r shl 16
     + g shl 8
     + b;
  GraphicSet.KeyColors[cmbKeyColors.ItemIndex] := c;
  imgKeyColor.Bitmap.Clear(c);
  imgKeyColor.Update;
  SetRGBTexts;
end;

procedure TF_GSTool.SetRGBTexts;
var
  r, g, b: Byte;
  c: TColor32;
begin
  c := GraphicSet.KeyColors[cmbKeyColors.ItemIndex];
  r := (c and $FF0000) shr 16;
  g := (c and $FF00) shr 8;
  b := (c and $FF);
  ebKCR.Text := IntToStr(r);
  ebKCG.Text := IntToStr(g);
  ebKCB.Text := IntToStr(b);
end;

procedure TF_GSTool.cbDisplayTriggerClick(Sender: TObject);
begin
  if SelectedObject <> nil then DisplayObjectImage;
end;

procedure TF_GSTool.cbBlackBgClick(Sender: TObject);
begin
  if SelectedObject <> nil then
    DisplayObjectImage; // Easier to just redo the whole thing than try to retroactively apply it
                        // to an already-drawn image.
  if SelectedTerrain <> nil then
  begin
    imgPiece.Bitmap.Assign(GraphicSet.TerrainImages[SelectedTerrainNumber]);
    ScaleDisplayImage;
  end; // This does mean having to handle terrain or objects seperately, though.
end;

procedure TF_GSTool.btnPrevFrameClick(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  FrameNumber := FrameNumber - 1;
  DisplayObjectImage;
  btnNextFrame.Enabled := true;
  if FrameNumber = 0 then btnPrevFrame.Enabled := false;
end;

procedure TF_GSTool.btnNextFrameClick(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  FrameNumber := FrameNumber + 1;
  DisplayObjectImage;
  btnPrevFrame.Enabled := true;
  if FrameNumber = GraphicSet.ObjectImages[SelectedObjectNumber].Count-1 then btnNextFrame.Enabled := false;
end;

procedure TF_GSTool.btnPrevFrameCurrentClick(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  SelectedObject.PreviewFrame := FrameNumber;
  ebPreviewFrame.Text := IntToStr(FrameNumber + 1);
end;

procedure TF_GSTool.btnKeyframeCurrentClick(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  SelectedObject.KeyFrame := FrameNumber;
  ebKeyFrame.Text := IntToStr(FrameNumber + 1);     
end;

procedure TF_GSTool.btnLoadFrameClick(Sender: TObject);
var
  SubForm: TF_ImportStrip;
begin
  if (SelectedObject = nil) and (SelectedTerrain = nil) then Exit;
  SubForm := TF_ImportStrip.Create(self);
  SubForm.AllowFrames := false;
  if SubForm.ImportStrip then
  begin
    if IsPositiveResult(SubForm.ShowModal) then
    begin
      if SelectedTerrainNumber <> -1 then
      begin
        GraphicSet.TerrainImages[SelectedTerrainNumber].Assign(SubForm.BitmapSet[0]);
        imgPiece.Bitmap.Assign(GraphicSet.TerrainImages[SelectedTerrainNumber]);
        ScaleDisplayImage;
      end;

      if SelectedObjectNumber <> -1 then
      begin
        GraphicSet.ObjectImages[SelectedObjectNumber][FrameNumber].Assign(SubForm.BitmapSet[0]);
        RefreshObjectSettings;
        DisplayObjectImage;
      end;

      UpdateLists(false);
    end;
  end;
  SubForm.Free;
end;

{procedure TF_GSTool.btnLoadFrameClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
  TempBMP: TBitmap32;
begin
  OpenDlg := TOpenDialog.Create(self);
  OpenDlg.Options := [ofFileMustExist];
  OpenDlg.Title := 'Select image file...';
  OpenDlg.Filter := 'PNG Image|*.png';
  if not OpenDlg.Execute then
  begin
    OpenDlg.Free;
    Exit;
  end;

  TempBMP := LoadPngFile(OpenDlg.FileName);

  if SelectedTerrainNumber <> -1 then
    GraphicSet.TerrainImages[SelectedTerrainNumber].Assign(TempBMP);

  if SelectedObjectNumber <> -1 then
    GraphicSet.ObjectImages[SelectedObjectNumber][FrameNumber].Assign(TempBMP);

  TempBMP.Free;
  OpenDlg.Free;
  DisplayObjectImage;
  UpdateLists(false);
end;}

procedure TF_GSTool.btnSaveFrameClick(Sender: TObject);
var
  SaveDlg: TSaveDialog;
  TempBMP: TBitmap32;
  TCol: TColor32;
  FoundTrans: Boolean;
  x, y: Integer;
  i, i2: Integer;
begin
  TempBMP := TBitmap32.Create;

  if SelectedTerrainNumber <> -1 then
    TempBMP.Assign(GraphicSet.TerrainImages[SelectedTerrainNumber]);

  if SelectedObjectNumber <> -1 then
    TempBMP.Assign(GraphicSet.ObjectImages[SelectedObjectNumber][FrameNumber]);

  SaveDlg := TSaveDialog.Create(self);
  SaveDlg.Title := 'Select image file...';
  SaveDlg.Filter := 'PNG Image|*.png|BMP Image|*.bmp';
  SaveDlg.DefaultExt := '.png';
  if fLastDirectory <> '' then SaveDlg.InitialDir := fLastDirectory;
  if not SaveDlg.Execute then
  begin
    SaveDlg.Free;
    Exit;
  end;
  fLastDirectory := ExtractFilePath(SaveDlg.FileName);

  if SaveDlg.FilterIndex = 2 then
  begin
    FoundTrans := false;
    TCol := $FFFF00FF;
    i := 0;
    repeat
      if (i = 256) then
      begin
        i2 := MessageDlg('No suitable transparent color found yet. Keep trying?', mtcustom, [mbYes, mbNo], 0);
        if i2 = 7 then
        begin
          TempBMP.Free;
          SaveDlg.Free;
          Exit;
        end;
        i := 0;
      end;
      with TempBMP do
        for y := 0 to Height-1 do
          for x := 0 to Width-1 do
          begin
            if (Pixel[x, y] and $FFFFFF) = (TCol and $FFFFFF) then
              begin
                if TCol = $FFFF00FF then
                  TCol := $FF000000
                else begin
                  RandSeed := ((x+1) * (y+1) * (i+1) * (x+y+i+1)) + RandSeed; //the +1 is to avoid the result being zero
                  TCol := (Random(256) shl 16) + (Random(256) shl 8) + (Random(256) shl 2);
                  TCol := TCol + $FF000000;
                  Inc(i);
                end;
              end;
          end;
    until FoundTrans = false;

    for y := 0 to TempBMP.Height-1 do
      for x := 0 to TempBMP.Width-1 do
        if (TempBMP.Pixel[x, y] and $FF000000) = 0 then
          TempBMP.Pixel[x, y] := TCol;
    TempBMP.SaveToFile(SaveDlg.FileName);
  end else
    SavePngFile(SaveDlg.FileName, TempBMP);

  SaveDlg.Free;
  TempBMP.Free;
end;

procedure TF_GSTool.btnDelTerrainClick(Sender: TObject);
begin
  if SelectedTerrainNumber = -1 then Exit;
  GraphicSet.TerrainImages.Delete(SelectedTerrainNumber);
  GraphicSet.MetaTerrains.Delete(SelectedTerrainNumber);
  UpdateLists;
end;

procedure TF_GSTool.btnAddTerrainClick(Sender: TObject);
begin
  AddTerrain(-1);
end;

procedure TF_GSTool.btnInsTerrainClick(Sender: TObject);
begin
  AddTerrain(SelectedTerrainNumber);
end;

procedure TF_GSTool.AddTerrain(loc: Integer);
var
  TempTer: TMetaTerrain;
  TempBMP: TBitmap32;
begin
  TempTer := TMetaTerrain.Create;
  TempBMP := TBitmap32.Create;
  TempBMP.SetSize(8, 8);
  TempBMP.Clear(0);

  if loc = -1 then
  begin
    GraphicSet.MetaTerrains.Add(TempTer);
    GraphicSet.TerrainImages.Add(TempBMP);
    loc := GraphicSet.MetaTerrains.Count-1;
  end else begin
    GraphicSet.MetaTerrains.Insert(loc, TempTer);
    GraphicSet.TerrainImages.Insert(loc, TempBMP);
  end;

  UpdateLists;
  lbTerrains.ItemIndex := loc;
  lbTerrainsClick(lbTerrains); // Why the hell not? It works! :D
end;

procedure TF_GSTool.btnAddObjectClick(Sender: TObject);
begin
  AddObject(-1);
end;

procedure TF_GSTool.btnInsertObjectClick(Sender: TObject);
begin
  AddObject(SelectedObjectNumber);
end;

procedure TF_GSTool.btnDelObjectClick(Sender: TObject);
begin
  if SelectedObjectNumber = -1 then Exit;
  GraphicSet.MetaObjects.Delete(SelectedObjectNumber);
  GraphicSet.ObjectImages.Delete(SelectedObjectNumber);
  UpdateLists;
end;

procedure TF_GSTool.AddObject(loc: Integer);
var
  TempObj: TMetaObject;
  TempBMP: TBitmap32;
  TempBMPs: TBitmaps;
begin
  TempObj := TMetaObject.Create;
  TempBMP := TBitmap32.Create;
  TempBMPs := TBitmaps.Create;
  TempBMP.SetSize(8, 8);
  TempBMP.Clear(0);
  TempBMPs.Add(TempBMP);

  if loc = -1 then
  begin
    GraphicSet.MetaObjects.Add(TempObj);
    GraphicSet.ObjectImages.Add(TempBMPs);
    loc := GraphicSet.MetaObjects.Count-1;
  end else begin
    GraphicSet.MetaObjects.Insert(loc, TempObj);
    GraphicSet.ObjectImages.Insert(loc, TempBMPs);
  end;

  UpdateLists;
  lbObjects.ItemIndex := loc;
  lbObjectsClick(lbObjects);
end;

procedure TF_GSTool.btnDelFrameClick(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  GraphicSet.ObjectImages[SelectedObjectNumber].Delete(FrameNumber);
  if FrameNumber = GraphicSet.ObjectImages[SelectedObjectNumber].Count then
    FrameNumber := FrameNumber - 1;
  RefreshObjectSettings;
  DisplayObjectImage;
end;

procedure TF_GSTool.AddFrame(loc: Integer);
var
  TempBMP: TBitmap32;
begin
  TempBMP := TBitmap32.Create;
  TempBMP.SetSize(8, 8);
  TempBMP.Clear(0);

  if loc = -1 then
  begin
    GraphicSet.ObjectImages[SelectedObjectNumber].Add(TempBMP);
    loc := GraphicSet.ObjectImages[SelectedObjectNumber].Count-1;
  end else
    GraphicSet.ObjectImages[SelectedObjectNumber].Insert(loc, TempBMP);

  UpdateLists(false);

  FrameNumber := loc;

  RefreshObjectSettings;
  DisplayObjectImage;
end;

procedure TF_GSTool.btnAddFrameClick(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  AddFrame(-1);
end;

procedure TF_GSTool.btnInsFrameClick(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  AddFrame(FrameNumber);
end;

procedure TF_GSTool.btnTerUpClick(Sender: TObject);
begin
  if SelectedTerrain = nil then Exit;
  GraphicSet.MetaTerrains.Exchange(SelectedTerrainNumber, SelectedTerrainNumber - 1);
  GraphicSet.TerrainImages.Exchange(SelectedTerrainNumber, SelectedTerrainNumber - 1);
  UpdateLists(false);
  lbTerrains.ItemIndex := SelectedTerrainNumber-1;
  lbTerrainsClick(lbTerrains);
end;

procedure TF_GSTool.btnTerDownClick(Sender: TObject);
begin
  if SelectedTerrain = nil then Exit;
  GraphicSet.MetaTerrains.Exchange(SelectedTerrainNumber, SelectedTerrainNumber + 1);
  GraphicSet.TerrainImages.Exchange(SelectedTerrainNumber, SelectedTerrainNumber + 1);
  UpdateLists(false);
  lbTerrains.ItemIndex := SelectedTerrainNumber+1;
  lbTerrainsClick(lbTerrains);
end;

procedure TF_GSTool.btnObjUpClick(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  GraphicSet.MetaObjects.Exchange(SelectedObjectNumber, SelectedObjectNumber - 1);
  GraphicSet.ObjectImages.Exchange(SelectedObjectNumber, SelectedObjectNumber - 1);
  UpdateLists(false);
  lbObjects.ItemIndex := SelectedObjectNumber-1;
  lbObjectsClick(lbObjects);
end;

procedure TF_GSTool.btnObjDownClick(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  GraphicSet.MetaObjects.Exchange(SelectedObjectNumber, SelectedObjectNumber + 1);
  GraphicSet.ObjectImages.Exchange(SelectedObjectNumber, SelectedObjectNumber + 1);
  UpdateLists(false);
  lbObjects.ItemIndex := SelectedObjectNumber+1;
  lbObjectsClick(lbObjects);
end;

procedure TF_GSTool.Exit1Click(Sender: TObject);
begin
  Close;
end;

procedure TF_GSTool.SaveAs1Click(Sender: TObject);
var
  SaveDlg: TSaveDialog;
  NeoLemmixLoader: TNeoLemmixGraphicSet;
begin
  SaveDlg := TSaveDialog.Create(self);
  SaveDlg.Title := 'Select output file...';
  SaveDlg.Filter := 'NeoLemmix Graphic Set|*.dat';
  SaveDlg.DefaultExt := '.dat';
  SaveDlg.Options := [ofOverwritePrompt];
  if fLastDirectory <> '' then SaveDlg.InitialDir := fLastDirectory;
  if not SaveDlg.Execute then
  begin
    SaveDlg.Free;
    Exit;
  end;

  fLastDirectory := ExtractFilePath(SaveDlg.FileName);

  NeoLemmixLoader := TNeoLemmixGraphicSet.Create(self);
  NeoLemmixLoader.SaveGraphicSet(SaveDlg.FileName, GraphicSet);
  NeoLemmixLoader.Free;

  fFilename := SaveDlg.Filename;

  fSaveCheckStream.Clear;
  GraphicSet.CreateSaveTest(fSaveCheckStream);

  SaveDlg.Free;
end;

procedure TF_GSTool.Load1Click(Sender: TObject);
var
  OpenDlg: TOpenDialog;
  NeoLemmixLoader: TNeoLemmixGraphicSet;
begin
  if not AskToSave then Exit;

  OpenDlg := TOpenDialog.Create(self);
  OpenDlg.Options := [ofFileMustExist];
  OpenDlg.Title := 'Select graphic set file...';
  OpenDlg.Filter := 'NeoLemmix Graphic Set|*.dat';
  if fLastDirectory <> '' then OpenDlg.InitialDir := fLastDirectory;
  if not OpenDlg.Execute then
  begin
    OpenDlg.Free;
    Exit;
  end;

  fLastDirectory := ExtractFilePath(OpenDlg.FileName);

  GraphicSet.Free;

  NeoLemmixLoader := TNeoLemmixGraphicSet.Create(self);
  GraphicSet := NeoLemmixLoader.LoadGraphicSet(OpenDlg.FileName);
  NeoLemmixLoader.Free;

  fFilename := OpenDlg.Filename;

  OpenDlg.Free;
  UpdateNewGS;
end;

procedure TF_GSTool.btnImportStripClick(Sender: TObject);
var
  SubForm: TF_ImportStrip;
  i: Integer;
  TempBMP: TBitmap32;
  BMPSet: TBitmaps;
begin
  if SelectedObject = nil then Exit;
  SubForm := TF_ImportStrip.Create(self);
  if SubForm.ImportStrip then
  begin
    if IsPositiveResult(SubForm.ShowModal) then
    begin
      FrameNumber := 0;
      BMPSet := GraphicSet.ObjectImages[SelectedObjectNumber];
      BMPSet.Clear;
      for i := 0 to SubForm.BitmapSet.Count-1 do
      begin
        TempBMP := TBitmap32.Create;
        TempBMP.Assign(SubForm.BitmapSet[i]);
        BMPSet.Add(TempBMP);
      end;
      RefreshObjectSettings;
      DisplayObjectImage;
      UpdateLists(false);
    end;
  end;
  SubForm.Free;
end;

procedure TF_GSTool.btnExportStripClick(Sender: TObject);
var
  TempBMP: TBitmap32;
  i, i2, x, y: Integer;
  mw, mh: Integer;
  SaveDialog: TSaveDialog;
  fn: String;
  SaveBMP: Boolean;
  FoundTrans: Boolean;
  TCol: TColor32;
begin
  if SelectedObject = nil then Exit;

  SaveDialog := TSaveDialog.Create(self);
  SaveDialog.Title := 'Select image file';
  SaveDialog.Filter := 'PNG File|*.png|BMP File|*.bmp';
  SaveDialog.DefaultExt := '.png';
  if fLastDirectory <> '' then SaveDialog.InitialDir := fLastDirectory;
  if not SaveDialog.Execute then
  begin
    SaveDialog.Free;
    Exit;
  end;
  fLastDirectory := ExtractFilePath(SaveDialog.FileName);
  fn := SaveDialog.FileName;
  SaveBMP := (SaveDialog.FilterIndex = 2);
  SaveDialog.Free;

  mw := 0;
  mh := 0;

  for i := 0 to GraphicSet.ObjectImages[SelectedObjectNumber].Count-1 do
  begin
    TempBMP := GraphicSet.ObjectImages[SelectedObjectNumber][i];
    if TempBMP.Width > mw then mw := TempBMP.Width;
    if TempBMP.Height > mh then mh := TempBMP.Height;
  end;

  TempBMP := TBitmap32.Create;
  TempBMP.SetSize(mw, mh*GraphicSet.ObjectImages[SelectedObjectNumber].Count);
  TempBMP.Clear(0);
  
  for i := 0 to GraphicSet.ObjectImages[SelectedObjectNumber].Count-1 do
    GraphicSet.ObjectImages[SelectedObjectNumber][i].DrawTo(TempBMP, 0, mh*i);
  if SaveBMP = true then
  begin
    FoundTrans := false;
    TCol := $FFFF00FF;
    i := 0;
    repeat
      if (i = 256) then
      begin
      i2 := MessageDlg('No suitable transparent color found yet. Keep trying?', mtcustom, [mbYes, mbNo], 0);
        if i2 = 7 then
        begin
          TempBMP.Free;
          Exit;
        end;
        i := 0;
      end;
      with TempBMP do
        for y := 0 to Height-1 do
          for x := 0 to Width-1 do
          begin
            if (Pixel[x, y] and $FFFFFF) = (TCol and $FFFFFF) then
              begin
                if TCol = $FFFF00FF then
                  TCol := $FF000000
                else begin
                  RandSeed := ((x+1) * (y+1) * (i+1) * (x+y+i+1)) + RandSeed; //the +1 is to avoid the result being zero
                  TCol := (Random(256) shl 16) + (Random(256) shl 8) + (Random(256) shl 2);
                  TCol := TCol + $FF000000;
                  Inc(i);
                end;
              end;
          end;
    until FoundTrans = false;

    for y := 0 to TempBMP.Height-1 do
      for x := 0 to TempBMP.Width-1 do
        if (TempBMP.Pixel[x, y] and $FF000000) = 0 then
          TempBMP.Pixel[x, y] := TCol;
    TempBMP.SaveToFile(fn);
  end else
    SavePngFile(fn, TempBMP);

  TempBMP.Free;
end;

procedure TF_GSTool.VGASPEC1Click(Sender: TObject);
var
  DosLoader: TDosGraphicSet;
  OpenDlg: TOpenDialog;
  fn: String;
begin
  if not AskToSave then Exit;
  
  OpenDlg := TOpenDialog.Create(self);
  OpenDlg.Title := 'Select VGASPEC file...';
  OpenDlg.Filter := 'VGASPEC file|vgaspec*.dat;x_*.dat';
  OpenDlg.Options := [ofFileMustExist];
  if fLastDirectory <> '' then OpenDlg.InitialDir := fLastDirectory;
  if not OpenDlg.Execute then
  begin
    OpenDlg.Free;
    Exit;
  end;
  fLastDirectory := ExtractFilePath(OpenDlg.FileName);
  fn := OpenDlg.FileName;
  OpenDlg.Free;

  DosLoader := TDosGraphicSet.Create(self);
  GraphicSet.Free;
  GraphicSet := DosLoader.LoadVgaspec(fn);
  DosLoader.Free;

  fFilename := '';

  UpdateNewGS;
end;

procedure TF_GSTool.SuperLemmini1Click(Sender: TObject);
var
  LemminiLoader: TLemminiGraphicSet;
  OpenDlg: TOpenDialog;
  fn: String;
begin
  if not AskToSave then Exit;
  
  if not fLemminiWarning then
    ShowMessage('Please note that the default graphic sets of Lemmini and SuperLemmini are protected' + #13 +
                'under their respective licence agreements. It is not permitted to redistribute these,' + #13 +
                'even in a different format. It is fine to convert them for your own personal usage,' + #13 +
                'or to use this tool for custom-made Lemmini / SuperLemmini graphic sets.');
  fLemminiWarning := true;
  OpenDlg := TOpenDialog.Create(self);
  OpenDlg.Title := 'Select style INI file...';
  OpenDlg.Filter := 'INI file|*.ini';
  OpenDlg.Options := [ofFileMustExist];
  if fLastDirectory <> '' then OpenDlg.InitialDir := fLastDirectory;
  if not OpenDlg.Execute then
  begin
    OpenDlg.Free;
    Exit;
  end;
  fLastDirectory := ExtractFilePath(OpenDlg.FileName);
  fn := OpenDlg.FileName;
  OpenDlg.Free;

  LemminiLoader := TLemminiGraphicSet.Create(self);
  GraphicSet.Free;
  GraphicSet := LemminiLoader.LoadGraphicSet(fn);
  LemminiLoader.Free;

  fFilename := '';

  UpdateNewGS; 
end;

procedure TF_GSTool.SuperLemmini2Click(Sender: TObject);
var
  LemminiLoader: TLemminiGraphicSet;
  SaveDlg: TSaveDialog;
  fn: String;
begin
  SaveDlg := TSaveDialog.Create(self);
  SaveDlg.Title := 'Select style INI file...';
  SaveDlg.Filter := 'INI file|*.ini';
  if fLastDirectory <> '' then SaveDlg.InitialDir := fLastDirectory;
  if not SaveDlg.Execute then
  begin
    SaveDlg.Free;
    Exit;
  end;
  fLastDirectory := ExtractFilePath(SaveDlg.FileName);
  fn := SaveDlg.FileName;
  SaveDlg.Free;

  LemminiLoader := TLemminiGraphicSet.Create(self);
  LemminiLoader.SaveGraphicSet(fn, GraphicSet);
  LemminiLoader.Free;
end;

procedure TF_GSTool.cbRandomFrameClick(Sender: TObject);
begin
  if SelectedObject = nil then Exit;
  SelectedObject.RandomFrame := cbRandomFrame.Checked;
end;

procedure TF_GSTool.CustomSoundManager1Click(Sender: TObject);
begin
  if CustomSoundManager1.Checked then
  begin
    fSoundPanel.Hide;
    CustomSoundManager1.Checked := false;
  end else begin
    fSoundPanel.Show;
    CustomSoundManager1.Checked := true;
  end;
end;

procedure TF_GSTool.OnHideSoundForm(Sender: TObject);
begin
  CustomSoundManager1.Checked := false;
end;

procedure TF_GSTool.CheapoSTYfile1Click(Sender: TObject);
var
  tarFile: String;
  OpenDlg: TOpenDialog;
  CheapoLoader: TCheapoGraphicSet;
begin
  if not AskToSave then Exit;
  
  OpenDlg := TOpenDialog.Create(self);
  OpenDlg.Title := 'Select STY file';
  OpenDlg.Filter := 'Cheapo Style|*.sty;';
  if fLastDirectory <> '' then OpenDlg.InitialDir := fLastDirectory;
  if not OpenDlg.Execute then
  begin
    OpenDlg.Free;
    Exit;
  end;
  fLastDirectory := ExtractFilePath(OpenDlg.FileName);
  tarFile := OpenDlg.FileName;

  CheapoLoader := TCheapoGraphicSet.Create(self);

  GraphicSet.Free;
  GraphicSet := CheapoLoader.LoadStyFile(tarFile);

  fFilename := '';

  UpdateNewGS;
  CheapoLoader.Free;
end;

procedure TF_GSTool.btnAnimPreviewClick(Sender: TObject);
var
  F: TFAnimPreview;
begin
  if lbObjects.ItemIndex = -1 then
  begin
    ShowMessage('Preview clicked without a selected object!');
    Exit;
    // This should never trigger, but chances are I've missed something. So,
    // better for it to fail neatly than possibly crash the tool and lose data.
  end;
  F := TFAnimPreview.Create(self);
  F.SetObject(GraphicSet, lbObjects.ItemIndex);
  F.ShowModal;
  F.Free;
end;

procedure TF_GSTool.ebPieceNameChange(Sender: TObject);
begin
  if lbTerrains.ItemIndex <> -1 then
    GraphicSet.MetaTerrains[lbTerrains.ItemIndex].Name := ebPieceName.Text;
  if lbObjects.ItemIndex <> -1 then
    GraphicSet.MetaObjects[lbObjects.ItemIndex].Name := ebPieceName.Text;
  UpdateLists(false);
end;

procedure TF_GSTool.ExperimentalFormat1Click(Sender: TObject);
var
  tarPath: String;
  SaveDlg: TSaveDialog;
  ExpLoader: TExperimentalGraphicSet;
begin
  SaveDlg := TSaveDialog.Create(self);
  SaveDlg.Title := 'Choose location for theme.nxtm file';
  SaveDlg.Filter := 'theme.nxtm File|theme.nxtm';
  if fLastDirectory <> '' then SaveDlg.InitialDir := fLastDirectory;
  if not SaveDlg.Execute then
  begin
    SaveDlg.Free;
    Exit;
  end;
  fLastDirectory := ExtractFilePath(SaveDlg.FileName);
  tarPath := SaveDlg.FileName;

  ExpLoader := TExperimentalGraphicSet.Create;
  ExpLoader.SaveGraphicSet(tarPath, GraphicSet);
  ExpLoader.Free;
  SaveDlg.Free;
end;

procedure TF_GSTool.MassStuffDoer1Click(Sender: TObject);
var
  Multi: TOpenMulti;
  i: Integer;
begin
  Multi := TOpenMulti.Create('NeoLemmix Graphic Sets|*.dat');
  try
    //if Multi.Count > 0 then
    //  DeleteDirectory(ExtractFilePath(Multi[0]) + 'special');
    for i := 0 to Multi.Count-1 do
      try
        //DoMassStuff(Multi[i]);
        DoMassConvert(Multi[i]);
      except
        on E: Exception do
        begin
          ShowMessage('Mass failed on ' + Multi[i]);
          ShowMessage(E.Message);
        end;
      end;
  finally
    Multi.Free;
  end;
  ShowMessage('Conversion complete.');
end;

procedure TF_GSTool.DoMassStuff(aFile: String);
var
  TGS: TBaseGraphicSet;
  NL: TNeoLemmixGraphicSet;

  i: Integer;
begin
  NL := TNeoLemmixGraphicSet.Create(self);
  TGS := NL.LoadGraphicSet(aFile);
  NL.Free;

  // Find the one-way arrows
  for i := 0 to TGS.ObjectImages.Count-1 do
    if TGS.MetaObjects[i].TriggerType in [7, 8, 19] then
    begin
      TGS.MetaObjects[i].ResizeHorizontal := true;
      TGS.MetaObjects[i].ResizeVertical := true;
    end;

  // Find the water, updrafts
  for i := 0 to TGS.ObjectImages.Count-1 do
    if TGS.MetaObjects[i].TriggerType in [5, 20] then
      TGS.MetaObjects[i].ResizeHorizontal := true;

  NL := TNeoLemmixGraphicSet.Create(self);
  NL.SaveGraphicSet(aFile, TGS);
  NL.Free;
  TGS.Free;
end;

procedure TF_GSTool.DoMassConvert(aFile: String);
var
  TGS: TBaseGraphicSet;
  NL: TNeoLemmixGraphicSet;
  EX: TExperimentalGraphicSet;
  S: String;
begin
  NL := TNeoLemmixGraphicSet.Create(self);
  EX := TExperimentalGraphicSet.Create;
  TGS := nil;
  try
    TGS := NL.LoadGraphicSet(aFile);
    S := ChangeFileExt(aFile, '');
    if DirectoryExists(S) then
      DeleteDirectory(S);

    if TGS.ObjectImages.Count > 0 then
    begin
      S := S + '\theme.nxtm';
      EX.SaveGraphicSet(S, TGS);
    end else begin
      ForceDirectories(ExtractFilePath(S) + 'special\terrain\');
      S := ExtractFilePath(S) + 'special\terrain\' + TGS.MetaTerrains[0].Name + '.png';
      EX.SaveSpecialGraphic(S, TGS);
    end;
  finally
    NL.Free;
    TGS.Free;
  end;
end;

procedure TF_GSTool.ExperimentalVGASPEC1Click(Sender: TObject);
var
  tarPath: String;
  SaveDlg: TSaveDialog;
  ExpLoader: TExperimentalGraphicSet;
begin
  SaveDlg := TSaveDialog.Create(self);
  SaveDlg.Title := 'Choose location and name for output file';
  SaveDlg.Filter := 'PNG File|*.png';
  SaveDlg.DefaultExt := '.png';
  if fLastDirectory <> '' then SaveDlg.InitialDir := fLastDirectory;
  if not SaveDlg.Execute then
  begin
    SaveDlg.Free;
    Exit;
  end;
  fLastDirectory := ExtractFilePath(SaveDlg.FileName);
  tarPath := SaveDlg.FileName;

  ExpLoader := TExperimentalGraphicSet.Create;
  ExpLoader.SaveSpecialGraphic(tarPath, GraphicSet);
  ExpLoader.Free;
  SaveDlg.Free;
end;

procedure DeleteDirectory(const Name: string);
var
  F: TSearchRec;
begin
  if FindFirst(Name + '\*', faAnyFile, F) = 0 then begin
    try
      repeat
        if (F.Attr and faDirectory <> 0) then begin
          if (F.Name <> '.') and (F.Name <> '..') then begin
            DeleteDirectory(Name + '\' + F.Name);
          end;
        end else begin
          DeleteFile(Name + '\' + F.Name);
        end;
      until FindNext(F) <> 0;
    finally
      FindClose(F);
    end;
    RemoveDir(Name);
  end;
end;

procedure TF_GSTool.Save1Click(Sender: TObject);
var
  NeoLemmixLoader: TNeoLemmixGraphicSet;
begin
  if fFilename = '' then
  begin
    SaveAs1Click(Sender);
    Exit;
  end;

  NeoLemmixLoader := TNeoLemmixGraphicSet.Create(self);
  NeoLemmixLoader.SaveGraphicSet(fFilename, GraphicSet);
  fSaveCheckStream.Clear;
  GraphicSet.CreateSaveTest(fSaveCheckStream);
  NeoLemmixLoader.Free;
end;

procedure TF_GSTool.cbResizeHorzClick(Sender: TObject);
begin
  if lbObjects.ItemIndex = -1 then Exit;
  GraphicSet.MetaObjects[lbObjects.ItemIndex].ResizeHorizontal := cbResizeHorz.Checked;
end;

procedure TF_GSTool.cbResizeVertClick(Sender: TObject);
begin
  if lbObjects.ItemIndex = -1 then Exit;
  GraphicSet.MetaObjects[lbObjects.ItemIndex].ResizeVertical := cbResizeVert.Checked;
end;

procedure TF_GSTool.AfterGSLoad;
begin
  GraphicSet.FixResolution;
end;

procedure TF_GSTool.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := AskToSave;
end;

function TF_GSTool.AskToSave: Boolean;
var
  MsgRes: Integer;
begin
  if CheckIfSaved then
  begin
    Result := true;
    Exit;
  end;

  MsgRes := MessageDlg('Do you want to save the current graphic set?', mtCustom, [mbYes, mbNo, mbCancel], 0);
  case MsgRes of
    mrYes: if fFilename = '' then
           begin
             SaveAs1Click(self);
             Result := fFilename <> '';
           end else begin
             Save1Click(self);
             Result := true;
           end;
    mrNo: Result := true;
    mrCancel: Result := false;
  end;
end;

procedure TF_GSTool.MassRename1Click(Sender: TObject);
var
  F: TFMassRename;
begin
  if (GraphicSet.MetaTerrains.Count = 0) and (GraphicSet.MetaObjects.Count = 0) then Exit;

  F := TFMassRename.Create(self);
  F.SetGraphicSet(GraphicSet);
  F.ShowModal;
  F.Free;
  UpdateLists(false);
end;

procedure TF_GSTool.cbLemmingSpritesChange(Sender: TObject);
begin
  GraphicSet.LemmingSprites := cbLemmingSprites.Text;
end;

procedure TF_GSTool.cbLemmingSpritesExit(Sender: TObject);
begin
  if cbLemmingSprites.Text = '' then cbLemmingSprites.Text := 'default';
end;

procedure TF_GSTool.StyleNames1Click(Sender: TObject);
var
  S: String;
  NL: TNeoLemmixGraphicSet;
  GS: TBaseGraphicSet;
  i: Integer;
begin
  S := OpenDialog('NeoLemmix Graphic Set|*.dat');
  if S = '' then Exit;
  NL := TNeoLemmixGraphicSet.Create(self);
  GS := NL.LoadGraphicSet(S);

  for i := 0 to GS.MetaTerrains.Count-1 do
    if i >= GraphicSet.MetaTerrains.Count then
      Break
    else
      GraphicSet.MetaTerrains[i].Name := GS.MetaTerrains[i].Name;

  for i := 0 to GS.MetaObjects.Count-1 do
    if i >= GraphicSet.MetaObjects.Count then
      Break
    else
      GraphicSet.MetaObjects[i].Name := GS.MetaObjects[i].Name;

  UpdateLists;

  NL.Free;
  GS.Free;
end;

end.
