unit GSLoadLemSet;

// Loads a LemSet graphic set source (INI files + BMPs or PNGs), or a native
// extension of such, into the editor's internal format. To use, create a
// TLemsetGraphicSet object, then call:
//
// <TBaseGraphicSet> := <TLemsetGraphicSet>.LoadGraphicSet(<path containing style source>);
//
// Giving the path of any file *within* the folder is also acceptable.
//
// The TBaseGraphicSet object is created during TLemsetGraphicSet.LoadGraphicSet, so you should
// not use <TBaseGraphicSet> := TBaseGraphicSet.Create; beforehand.
//
// Once TLemsetGraphicSet.LoadGraphicSet has been allowed to run, there is no need to keep the
// TLemsetGraphicSet object around, it can be freed.

// Saving LemSet graphic set source is very similar, but use this instead:
//
// <TLemsetGraphicSet>.SaveGraphicSet(<path to save to>, <TBaseGraphicSet>, <compatibility mode: True/False>);
//
// If compatibility mode is on, the output will be fully LemSet-compatible. Otherwise, it will contain some
// extensions to the usual LemSet format that are only recognised by the editor. The compatibility mode switch
// can be omitted, in which case it is assumed to be off.

interface

uses
  Dialogs,
  Classes, SysUtils, LemGraphicSet, GR32, PngInterface;

type
  TKeyArray = array[0..31] of TColor32;
  PKeyArray = ^TKeyArray;

  TLemsetGraphicSetClass = class of TLemsetGraphicSet;
  TLemsetGraphicSet = class(TComponent)
    private
      Ini: TStringList; // This actually works better than proper INI object
      procedure ClearSpaces;
      function LoadImage(fn: String): TBitmap32; // Smart-load, will load PNG or BMP in same priority order as LemSet; returns nil if none exist
      procedure PatchTransparent(Bmp: TBitmap32; KeyColor: TColor32; Img18Bit: Boolean = true);
      procedure PatchTransparentPalette(Bmp: TBitmap32; KeyArray: PKeyArray);
    public
      constructor Create(aOwner: TComponent); override;
      destructor Destroy; override;
      function LoadGraphicSet(aPath: String): TBaseGraphicSet;
      procedure SaveGraphicSet(aPath: String; GS: TBaseGraphicSet);
  end;

implementation

constructor TLemsetGraphicSet.Create(aOwner: TComponent);
begin
  inherited;
end;

destructor TLemsetGraphicSet.Destroy;
begin
  inherited;
end;

procedure TLemsetGraphicSet.SaveGraphicSet(aPath: String; GS: TBaseGraphicSet);
var
  TempBMP: TBitmap32;
  i, i2, i3: Integer;
  mw, mh: Integer;
  pf: String;
  x, y: Integer;
  LastCol, TCol: TColor32;
label
  BreakLabel;
begin
  aPath := ExtractFilePath(aPath);
  Ini := TStringList.Create;

  TCol := 0; // Avoiding a compiler warning. In practice, it would never matter anyway due to it being used and the possibility
             // of it not being initialized, being mutually exclusive.

  // Output palette data. In non-compatibility mode, only the key color actually needs to be added, but for
  // the sake of tidiness add the fixed palette too. (This is also true of compatibility mode where RGB graphics
  // are being used)
  Ini.Add('v147=1');
  Ini.Add('sprites=' + GS.LemmingSprites);
  TempBMP := TBitmap32.Create;
  TempBMP.SetSize(8, 1);
  TempBMP.Clear($FF000000);

  for i := 0 to 7 do
    TempBMP.Pixel[i, 0] := GS.KeyColors[i];

  SavePngFile(aPath + 'palette.png', TempBMP);

  Ini.Add('');
  Ini.Add('');

  // Output terrain data and images.
  for i := 0 to GS.MetaTerrains.Count-1 do
  begin
    TempBmp.Assign(GS.TerrainImages[i]);
    SavePngFile(aPath + 't' + LeadZeroStr(i, 2) + '.png', TempBmp);
    Ini.Add('terrain_' + IntToStr(i) + '_name=' + GS.MetaTerrains[i].Name);
    if GS.MetaTerrains[i].Steel then
      Ini.Add('terrain_' + IntToStr(i) + '_steel=1');
    Ini.Add('');
  end;

  Ini.Add('');

  // Output object data and images.
  for i := 0 to GS.MetaObjects.Count-1 do
  begin
    TempBmp := TBitmap32.Create;
    mw := 0;
    mh := 0;
    for i2 := 0 to GS.ObjectImages[i].Count-1 do
    begin
      if GS.ObjectImages[i][i2].Width > mw then mw := GS.ObjectImages[i][i2].Width;
      if GS.ObjectImages[i][i2].Height > mh then mh := GS.ObjectImages[i][i2].Height;
    end;
    TempBmp.SetSize(mw, mh*GS.ObjectImages[i].Count);
    TempBmp.Clear(0);
    for i2 := 0 to GS.ObjectImages[i].Count-1 do
    begin
      GS.ObjectImages[i][i2].DrawTo(TempBmp, 0, i2*mh, GS.ObjectImages[i][i2].BoundsRect);
    end;
    SavePngFile(aPath + 'o' + LeadZeroStr(i, 2) + '.png', TempBmp);
    TempBmp.Free;

    pf := 'object_' + IntToStr(i) + '_';
    with GS.MetaObjects[i] do
    begin
      Ini.Add(pf + 'name=' + Name);
      Ini.Add(pf + 'frames=' + IntToStr(GS.ObjectImages[i].Count));
      if TriggerType <> 0 then
      begin
        Ini.Add(pf + 'trigger_type=' + IntToStr(TriggerType));
        if ((TriggerType = 1) and TriggerAnim)
        or (TriggerType in [4, 11, 12, 15, 17, 21, 23, 24, 28, 29, 31]) then
          Ini.Add(pf + 'trigger_anim=1');

        if not (TriggerType in [25, 30]) then
        begin
          Ini.Add(pf + 'trigger_left=' + IntToStr(PTriggerX));
          Ini.Add(pf + 'trigger_top=' + IntToStr(PTriggerY));
        end;

        if not (TriggerType in [13, 23, 25, 30]) then
        begin
          Ini.Add(pf + 'trigger_width=' + IntToStr(PTriggerW));
          Ini.Add(pf + 'trigger_height=' + IntToStr(PTriggerH));
        end;

        if (TriggerType in [28, 29]) then
        begin
          Ini.Add(pf + 'trigger_point_x=' + IntToStr(STriggerX));
          Ini.Add(pf + 'trigger_point_y=' + IntToStr(STriggerY));
        end;

        if ((TriggerType in [4, 11, 24, 28, 29, 31])
        or ((TriggerType = 1) and TriggerAnim))
        and (TriggerSound <> 0) then
          Ini.Add(pf + 'trigger_sound=' + IntToStr(TriggerSound));

      end;

      if PreviewFrame <> 0 then Ini.Add(pf + 'preview_frame=' + IntToStr(PreviewFrame));
      if KeyFrame <> 0 then Ini.Add(pf + 'key_frame=' + IntToStr(KeyFrame));

      if ((TriggerType in [0, 2, 3, 5, 6, 7, 8, 9, 10, 18, 19, 20, 22, 25, 26, 27, 30])
      or ((TriggerType = 1) and (TriggerAnim = false)))
      and RandomFrame then
        Ini.Add(pf + 'random_frame=1');

    end;

    Ini.Add('');
  end;

    for i := 0 to 255 do
      if GS.Sounds[i] <> nil then GS.Sounds[i].SaveToFile(aPath + 's' + IntToStr(i) + '.wav');

  Ini.SaveToFile(aPath + 'style.ini');
  Ini.Free;
end;

function TLemsetGraphicSet.LoadGraphicSet(aPath: String): TBaseGraphicSet;
var
  i, i2, mi: Integer;
  TempBMP, TempObjBMP: TBitmap32;
  TempBitmaps: TBitmaps;
  s, pf: String;
  TempTer: TMetaTerrain;
  TempObj: TMetaObject;
  PaletteColors: TKeyArray;
  RgbColor: Boolean;
  ExtColor: Boolean;
  TempStringList: TStringList;
  HorzSplit: Boolean;
  fw, fh: Integer;
  NoPatch: Boolean;

  V147: Boolean;
begin
  aPath := ExtractFilePath(aPath);

  Ini := TStringList.Create;
  Ini.LoadFromFile(aPath + 'style.ini');
  ClearSpaces;

  Result := TBaseGraphicSet.Create;
  Result.Resolution := StrToIntDef(Ini.Values['resolution_factor'], 8); // This usually won't be defined, so 8 (normal) is default

  V147 := Ini.Values['v147'] = '1';

  //Load palette data, etc
  if V147 then
  begin
    TempBMP := LoadImage(aPath + 'palette');
    for i := 0 to 7 do
      PaletteColors[i+8] := TempBmp.Pixel[i, 0];
    ExtColor := true;
  end else if LowerCase(Ini.Values['palette_info']) = 'bmp' then
  begin
    TempBMP := LoadImage(aPath + 'palette');
    for i := 0 to TempBmp.Width-1 do
      PaletteColors[i] := TempBmp.Pixel[i, 0];
    for i := TempBmp.Width to 31 do
      PaletteColors[i] := PaletteColors[0];
    ExtColor := (TempBmp.Width = 32);
  end else begin
    ExtColor := (Ini.Values['palette_colors'] = '32');
    if ExtColor then
      mi := 32
    else
      mi := 16;
    TempStringList := TStringList.Create;
    for i := 0 to mi-1 do
    begin
      TempStringList.CommaText := Ini.Values['palette_' + IntToStr(i)];
      PaletteColors[i] := (StrToInt(TempStringList[0]) shl 16)
                        + (StrToInt(TempStringList[1]) shl 8)
                        + (StrToInt(TempStringList[2]));
      if Ini.Values['palette_info'] = '18' then
        PaletteColors[i] := PaletteColors[i] shl 2;
      PaletteColors[i] := PaletteColors[i] + $FF000000;
    end;
    for i := mi to 31 do
      PaletteColors[i] := PaletteColors[0];
    TempStringList.Free;
  end;

  if (Ini.Values['rgb_images'] = '1') or V147 then
  begin
    RgbColor := true;
    if Ini.Values['rgb_24'] = '1' then ExtColor := true;
  end else
    RgbColor := false;

  NoPatch := (Ini.Values['no_image_patching'] = '1') or V147;

  if Ini.Values['xmas_lemmings'] = '1' then
    Result.LemmingSprites := 'xlemming';

  if Ini.Values['sprites'] <> '' then
    Result.LemmingSprites := Ini.Values['sprites'];

  for i := 0 to 7 do
    Result.KeyColors[i] := PaletteColors[i+8];

  i := 0;
  while true do
  begin
    s := aPath + 't' + LeadZeroStr(i, 2);
    TempBMP := LoadImage(s);
    if TempBMP = nil then break;
    TempTer := TMetaTerrain.Create;
    TempTer.Name := Ini.Values['terrain_' + IntToStr(i) + '_name'];
    if TempTer.Name = '' then TempTer.Name := 'T' + IntToStr(i);
    if StrToIntDef(Ini.Values['terrain_' + IntToStr(i) + '_steel'], 0) <> 0 then
      TempTer.Steel := true;
    if not NoPatch then
    begin
      if RgbColor then
        PatchTransparent(TempBmp, PaletteColors[7], not ExtColor)
        else
        PatchTransparentPalette(TempBmp, @PaletteColors);
    end;
    Result.MetaTerrains.Add(TempTer);
    Result.TerrainImages.Add(TempBMP);
    i := i + 1;
  end;

  //Load the objects
  i := 0;
  while true do
  begin
    s := aPath + 'o' + LeadZeroStr(i, 2);
    pf := 'object_' + IntToStr(i) + '_';
    if StrToIntDef(Ini.Values[pf + 'frames'], 0) = 0 then break;
    TempObjBMP := LoadImage(s);
    if TempObjBMP = nil then break;

    TempObj := TMetaObject.Create;
    TempBitmaps := TBitmaps.Create;

    if not NoPatch then
    begin
      if RgbColor then
        PatchTransparent(TempObjBmp, PaletteColors[7], not ExtColor)
        else
        PatchTransparentPalette(TempObjBmp, @PaletteColors);
    end;

    HorzSplit := (Ini.Values[pf + 'horizontal_frames'] = '1');

    fw := TempObjBmp.Width;
    fh := TempObjBmp.Height;
    if HorzSplit then
      fw := fw div StrToInt(Ini.Values[pf + 'frames'])
      else
      fh := fh div StrToInt(Ini.Values[pf + 'frames']);

    for i2 := 0 to StrToInt(Ini.Values[pf + 'frames'])-1 do
    begin
      TempBmp := TBitmap32.Create;
      TempBmp.SetSize(fw, fh);
      if HorzSplit then
        TempObjBmp.DrawTo(TempBmp, 0, 0, Rect(i2*fw, 0, (i2+1)*fw, fh))
        else
        TempObjBmp.DrawTo(TempBmp, 0, 0, Rect(0, i2*fh, fw, (i2+1)*fh));
      TempBitmaps.Add(TempBmp);
    end;

    Result.ObjectImages.Add(TempBitmaps);

    TempObj.Name := Ini.Values[pf + 'name'];
    if TempObj.Name = '' then TempObj.Name := 'O' + IntToStr(i);

    TempObj.TriggerType := StrToIntDef(Ini.Values[pf + 'trigger_type'], 0);
    TempObj.PreviewFrame := StrToIntDef(Ini.Values[pf + 'preview_frame'], 0);
    TempObj.KeyFrame := StrToIntDef(Ini.Values[pf + 'key_frame'], 0);
    if TempObj.TriggerType = 1 then
      TempObj.TriggerAnim := (Ini.Values[pf + 'trigger_anim'] = '1')
      else
      TempObj.TriggerAnim := (TempObj.TriggerType in [4, 11, 12, 14, 15, 17, 21, 23, 24, 28, 29, 31]);
    TempObj.PTriggerX := StrToIntDef(Ini.Values[pf + 'trigger_left'], 0);
    TempObj.PTriggerY := StrToIntDef(Ini.Values[pf + 'trigger_top'], -4);
    TempObj.PTriggerW := StrToIntDef(Ini.Values[pf + 'trigger_width'], 1);
    TempObj.PTriggerH := StrToIntDef(Ini.Values[pf + 'trigger_height'], 1);
    TempObj.STriggerX := StrToIntDef(Ini.Values[pf + 'trigger_point_x'], 0);
    TempObj.STriggerY := StrToIntDef(Ini.Values[pf + 'trigger_point_y'], 0);
    TempObj.STriggerW := StrToIntDef(Ini.Values[pf + 'trigger_point_w'], 1);
    TempObj.STriggerH := StrToIntDef(Ini.Values[pf + 'trigger_point_h'], 1);
    TempObj.TriggerSound := StrToIntDef(Ini.Values[pf + 'trigger_sound'], 0);

    TempObj.RandomFrame := (Ini.Values[pf + 'random_frame'] = '1');

    if (i = 1) and not (Ini.Values['multi_windows'] = '1') then
      TempObj.TriggerType := 23;

    if (TempObj.TriggerType = 23) and (TempObj.PTriggerX = 0) and
       ((TempObj.PTriggerY = -4) or (TempObj.PTriggerY = 0)) then
    begin
      TempObj.PTriggerX := 24;
      TempObj.PTriggerY := 13;
    end;

    if TempObj.TriggerType in [15, 17, 23] then
      TempObj.PreviewFrame := 1;

    Result.MetaObjects.Add(TempObj);

    i := i + 1;
  end;

  for i := 0 to 255 do
    if FileExists(aPath + 's' + IntToStr(i) + '.wav') then
    begin
      Result.Sounds[i] := TMemoryStream.Create;
      Result.Sounds[i].LoadFromFile(aPath + 's' + IntToStr(i) + '.wav');
    end;

  Ini.Free;
end;

procedure TLemsetGraphicSet.ClearSpaces;
var
  i: Integer;
begin
  for i := 0 to Ini.Count-1 do
    Ini[i] := StringReplace(Ini[i], ' ', '', [rfReplaceAll]);
  // Why? Because this is how LemSet behaves too. Spaces are completely
  // ignored; in both keys and values.
end;

function TLemsetGraphicSet.LoadImage(fn: String): TBitmap32;
// CAN RETURN NIL (if file does not exist)! Be sure to account for this.
begin
  Result := nil;
  if FileExists(fn + '.png') then
    Result := LoadPngFile(fn + '.png')
  else if FileExists(fn + '.bmp') then
  begin
    Result := TBitmap32.Create;
    Result.LoadFromFile(fn + '.bmp');
  end;
end;

procedure TLemsetGraphicSet.PatchTransparent(Bmp: TBitmap32; KeyColor: TColor32; Img18Bit: Boolean = true);
// This routine is for RGB (18bit+alpha or 24bit+alpha) graphic sets only.
// For 4-bit or 5-bit ones, use PatchTransparentPalette.
var
  x, y: Integer;
  CheckCol: TColor32;
begin
  KeyColor := KeyColor and $FFFFFF;
  if Img18Bit then KeyColor := KeyColor and $FCFCFC;
  for y := 0 to Bmp.Height-1 do
    for x := 0 to Bmp.Width-1 do
    begin
      CheckCol := Bmp.Pixel[x, y] and $FFFFFF;
      if Img18Bit then CheckCol := CheckCol and $FCFCFC;
      if CheckCol = KeyColor then
        Bmp.Pixel[x, y] := 0
      else if Img18Bit then
        Bmp.Pixel[x, y] := Bmp.Pixel[x, y] and $FFFCFCFC;
    end;
end;

procedure TLemsetGraphicSet.PatchTransparentPalette(Bmp: TBitmap32; KeyArray: PKeyArray);
var
  x, y, i: Integer;
  KeyColors: TKeyArray;
  CheckCol: TColor32;
  FoundMatch: Boolean;
begin
  KeyColors := KeyArray^;
  for i := 0 to 23 do
    KeyColors[i] := KeyColors[i] and $FCFCFC;
  for y := 0 to Bmp.Height-1 do
    for x := 0 to Bmp.Width-1 do
    begin
      CheckCol := Bmp.Pixel[x, y] and $FCFCFC;
      FoundMatch := false;
      for i := 0 to 23 do
        if CheckCol = KeyColors[i] then
        begin
          FoundMatch := true;
          Break;
        end;
      if not FoundMatch then
        Bmp.Pixel[x, y] := 0
      else
        Bmp.Pixel[x, y] := Bmp.Pixel[x, y] and $FFFCFCFC;
    end;
end;

end.