object F_ImportStrip: TF_ImportStrip
  Left = 203
  Top = 222
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Import Strip'
  ClientHeight = 433
  ClientWidth = 337
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefault
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 120
    Top = 282
    Width = 37
    Height = 13
    Caption = 'Frames:'
  end
  object imgFrame: TImage32
    Left = 48
    Top = 24
    Width = 241
    Height = 241
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baCenter
    Scale = 1.000000000000000000
    ScaleMode = smNormal
    TabOrder = 0
    OnClick = imgFrameClick
  end
  object btnCancel: TButton
    Left = 176
    Top = 392
    Width = 97
    Height = 33
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object btnOK: TButton
    Left = 64
    Top = 392
    Width = 97
    Height = 33
    Caption = 'OK'
    ModalResult = 1
    TabOrder = 2
  end
  object ebFrameCount: TEdit
    Left = 168
    Top = 280
    Width = 49
    Height = 21
    TabOrder = 3
    Text = '1'
    OnChange = ebFrameCountChange
  end
  object rgTransparency: TRadioGroup
    Left = 24
    Top = 304
    Width = 145
    Height = 49
    Caption = 'Transparency'
    Ctl3D = True
    ItemIndex = 0
    Items.Strings = (
      'Use Alpha Channel'
      'Use Transparent Color')
    ParentCtl3D = False
    TabOrder = 4
    OnClick = rgTransparencyClick
  end
  object rgOrientation: TRadioGroup
    Left = 176
    Top = 304
    Width = 129
    Height = 49
    Caption = 'Orientation'
    ItemIndex = 0
    Items.Strings = (
      'Vertical'
      'Horizontal')
    TabOrder = 5
    OnClick = rgOrientationClick
  end
  object btnNextFrame: TButton
    Left = 296
    Top = 120
    Width = 25
    Height = 49
    Caption = '>'
    TabOrder = 6
    OnClick = btnNextFrameClick
  end
  object btnPrevFrame: TButton
    Left = 16
    Top = 120
    Width = 25
    Height = 49
    Caption = '<'
    TabOrder = 7
    OnClick = btnPrevFrameClick
  end
  object cbLemmini: TCheckBox
    Left = 96
    Top = 360
    Width = 161
    Height = 17
    Caption = 'Source Is Lemmini Resolution'
    TabOrder = 8
  end
end
