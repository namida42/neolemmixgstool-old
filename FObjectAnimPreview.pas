unit FObjectAnimPreview;

interface

uses
  LemGraphicSet, Math,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GR32_Image, StdCtrls, ExtCtrls;

type
  TFAnimPreview = class(TForm)
    btnOk: TButton;
    I32Frame: TImage32;
    btnZoomIn: TButton;
    btnZoomOut: TButton;
    FrameAdvanceTimer: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure btnZoomInClick(Sender: TObject);
    procedure btnZoomOutClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FrameAdvanceTimerTimer(Sender: TObject);
  private
    fCurrentFrame: Integer;
    fGraphicSet: TBaseGraphicSet;
    fObjectID: Integer;
    fZoom: Integer;
    fDelay: Integer;
  public
    procedure SetObject(aGraphicSet: TBaseGraphicSet; aObjectID: Integer);
    procedure PrepareDisplay;
    procedure UpdateImage;
    procedure AdvanceFrame;
  end;

var
  FAnimPreview: TFAnimPreview;

implementation

{$R *.dfm}

procedure TFAnimPreview.SetObject(aGraphicSet: TBaseGraphicSet; aObjectID: Integer);
begin
  fGraphicSet := aGraphicSet;
  fObjectID := aObjectID;
  fZoom := min(32 div aGraphicSet.Resolution, 1);
  PrepareDisplay;
end;

procedure TFAnimPreview.PrepareDisplay;
var
  //OldWidth, OldHeight: Integer;
  ViewWidth, ViewHeight: Integer;
  ButtonWidth, ButtonHeight: Integer;
  TargetBaseWidth, TargetBaseHeight: Integer;
const
  DESIRED_BORDER = 24;
  DESIRED_SPACING = 16; //between image and buttons
begin
  // Prepare stuff
  //OldWidth := Self.Width;
  //OldHeight := Self.Height;
  ViewWidth := fGraphicSet.ObjectImages[fObjectID][0].Width * fZoom;
  ViewHeight := fGraphicSet.ObjectImages[fObjectID][0].Height * fZoom;
  ButtonWidth := (btnOk.Left - btnZoomIn.Left) + btnOk.Width;
  ButtonHeight := btnZoomIn.Height;
  TargetBaseWidth := max(ViewWidth, ButtonWidth) + (DESIRED_BORDER * 2);
  TargetBaseHeight := ViewHeight + DESIRED_SPACING + ButtonHeight + (DESIRED_BORDER * 2);

  // Resize form
  ClientWidth := TargetBaseWidth;
  ClientHeight := TargetBaseHeight;

  // Image size and position - don't forget to set the size of the bitmap too
  I32Frame.Width := ViewWidth;
  I32Frame.Height := ViewHeight;
  I32Frame.Left := (ClientWidth - I32Frame.Width) div 2;
  I32Frame.Top := DESIRED_BORDER;
  I32Frame.Bitmap.SetSize(I32Frame.Width, I32Frame.Height);
  I32Frame.Bitmap.Clear;

  // Button positions
  btnZoomIn.Left := (ClientWidth - ButtonWidth) div 2;
  btnZoomOut.Left := btnZoomIn.Left + 80; //eh, probably shouldn't hardcode this, but it should work
  btnOk.Left := btnZoomOut.Left + 80;
  btnZoomIn.Top := DESIRED_BORDER + DESIRED_SPACING + I32Frame.Height;
  btnZoomOut.Top := btnZoomIn.Top;
  btnOk.Top := btnZoomOut.Top;

  UpdateImage;
end;

procedure TFAnimPreview.FormCreate(Sender: TObject);
begin
  fCurrentFrame := 0;
  fGraphicSet := nil;
  fObjectID := 0;
  fDelay := 0;
  fZoom := 1;
end;

procedure TFAnimPreview.btnZoomInClick(Sender: TObject);
begin
  fZoom := fZoom + 1;
  PrepareDisplay;
  if (ClientWidth > Screen.Width) or (ClientHeight > Screen.Height) then
  begin
    fZoom := fZoom - 1;
    PrepareDisplay;
  end;
end;

procedure TFAnimPreview.btnZoomOutClick(Sender: TObject);
begin
  if fZoom = 1 then Exit;
  fZoom := fZoom - 1;
  PrepareDisplay;
end;

procedure TFAnimPreview.UpdateImage;
begin
  fGraphicSet.ObjectImages[fObjectID][fCurrentFrame].DrawTo(I32Frame.Bitmap, I32Frame.Bitmap.BoundsRect);
end;

procedure TFAnimPreview.AdvanceFrame;
begin
  if fDelay > 0 then
  begin
    fDelay := fDelay - 1;
    Exit;
  end;

  fCurrentFrame := fCurrentFrame + 1;
  if fCurrentFrame >= fGraphicSet.ObjectImages[fObjectID].Count then
    fCurrentFrame := 0;

  if (fCurrentFrame = 0) and (fGraphicSet.MetaObjects[fObjectID].TriggerAnim) then
    fDelay := 17;

  if (fCurrentFrame = 1) and (fGraphicSet.MetaObjects[fObjectID].TriggerType = 31) then
    fDelay := 17;

  UpdateImage;
end;

procedure TFAnimPreview.FormShow(Sender: TObject);
begin
  FrameAdvanceTimer.Enabled := true;
end;

procedure TFAnimPreview.FrameAdvanceTimerTimer(Sender: TObject);
begin
  AdvanceFrame;
end;

end.
