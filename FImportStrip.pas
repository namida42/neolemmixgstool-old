unit FImportStrip;

interface

uses
  PngInterface, LemGraphicSet,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GR32, StdCtrls, GR32_Image, ExtCtrls;

type
  TF_ImportStrip = class(TForm)
    imgFrame: TImage32;
    btnCancel: TButton;
    btnOK: TButton;
    Label1: TLabel;
    ebFrameCount: TEdit;
    rgTransparency: TRadioGroup;
    rgOrientation: TRadioGroup;
    btnNextFrame: TButton;
    btnPrevFrame: TButton;
    cbLemmini: TCheckBox;
    procedure btnPrevFrameClick(Sender: TObject);
    procedure btnNextFrameClick(Sender: TObject);
    procedure ebFrameCountChange(Sender: TObject);
    procedure rgOrientationClick(Sender: TObject);
    procedure rgTransparencyClick(Sender: TObject);
    procedure imgFrameClick(Sender: TObject);
  private
    fAllowFrames: Boolean;
    fStripBMP: TBitmap32;
    fFrameImages: TBitmaps;
    fTransparentColor: TColor32;
    fDisplayFrame: Integer;
    procedure UpdateImageSet;
    procedure ShowDisplayFrame;
    procedure ScaleDisplayImage;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function ImportStrip: Boolean;
    property BitmapSet: TBitmaps read fFrameImages;
    property AllowFrames: Boolean read fAllowFrames write fAllowFrames;
  end;

var
  F_ImportStrip: TF_ImportStrip;

implementation

{$R *.dfm}

constructor TF_ImportStrip.Create(aOwner: TComponent);
begin
  inherited;
  fStripBMP := TBitmap32.Create;
  fFrameImages := TBitmaps.Create;
  fAllowFrames := true;
end;

destructor TF_ImportStrip.Destroy;
begin
  fStripBMP.Free;
  fFrameImages.Free;
  inherited;
end;

function TF_ImportStrip.ImportStrip: Boolean;
var
  OpenDialog: TOpenDialog;
begin
  Result := false;
  OpenDialog := TOpenDialog.Create(self);
  OpenDialog.Title := 'Select source image file';
  OpenDialog.Options := [ofFileMustExist];
  OpenDialog.Filter := 'Image (BMP / PNG / GIF) File|*.bmp;*.png;*.gif';
  if not OpenDialog.Execute then
  begin
    OpenDialog.Free;
    Exit;
  end;
  try
    if ExtractFileExt(OpenDialog.FileName) = '.png' then
    begin
      fStripBMP.Free;
      try
        fStripBMP := LoadPngFile(OpenDialog.FileName);
      except
        fStripBMP := nil;
      end;
    end else if ExtractFileExt(OpenDialog.FileName) = '.gif' then
    begin
      fStripBMP.Free;
      try
        fStripBMP := LoadGifFile(OpenDialog.FileName);
      except
        fStripBMP := nil;
      end;
    end else begin
      fStripBMP.LoadFromFile(OpenDialog.FileName);
    end;
  except
    if fStripBMP = nil then fStripBMP := TBitmap32.Create;
    ShowMessage('Error: The image could not be loaded.');
    Exit;
  end;
  rgTransparency.ItemIndex := 0;
  rgOrientation.ItemIndex := 0;
  rgOrientation.Enabled := fAllowFrames;
  ebFrameCount.Text := '1';
  ebFrameCount.Enabled := fAllowFrames;
  fTransparentColor := $FFFF00FF;
  UpdateImageSet;
  Result := true;
end;

procedure TF_ImportStrip.UpdateImageSet;
var
  TempBMP, TempBMP2: TBitmap32;
  FCount: Integer;
  i: Integer;
  x, y: Integer;
begin
  fFrameImages.Clear;
  FCount := StrToIntDef(ebFrameCount.Text, 1);
  if FCount < 1 then FCount := 1;
  for i := 0 to FCount-1 do
  begin
    TempBMP := TBitmap32.Create;
    if rgOrientation.ItemIndex = 1 then
    begin
      TempBMP.SetSize(fStripBMP.Width div FCount, fStripBmp.Height);
      fStripBMP.DrawTo(TempBmp, 0, 0, Rect(i * TempBMP.Width, 0, (i+1) * TempBMP.Width, TempBMP.Height));
    end else begin
      TempBMP.SetSize(fStripBMP.Width, fStripBmp.Height div FCount);
      fStripBMP.DrawTo(TempBmp, 0, 0, Rect(0, i * TempBMP.Height, TempBMP.Width, (i+1) * TempBMP.Height));
    end;
    if rgTransparency.ItemIndex = 1 then
      for y := 0 to TempBMP.Height-1 do
        for x := 0 to TempBMP.Width-1 do
          if (TempBMP.Pixel[x, y] and $FFFFFF) = (fTransparentColor and $FFFFFF) then
            TempBMP.Pixel[x, y] := 0
          else
            TempBMP.Pixel[x, y] := TempBMP.Pixel[x, y] or $FF000000;
    if cbLemmini.Checked then
    begin
      TempBMP2 := TBitmap32.Create;
      TempBMP2.SetSize(TempBMP.Width div 2, TempBMP.Height div 2);
      TempBMP.DrawTo(TempBMP2, TempBMP2.BoundsRect, TempBMP.BoundsRect);
      fFrameImages.Add(TempBMP2);
      TempBMP.Free;
    end else
      fFrameImages.Add(TempBMP);
  end;
  if fDisplayFrame > fFrameImages.Count-1 then fDisplayFrame := 0;
  ShowDisplayFrame;
end;

procedure TF_ImportStrip.btnPrevFrameClick(Sender: TObject);
begin
  if fDisplayFrame = 0 then Exit;
  fDisplayFrame := fDisplayFrame - 1;
  ShowDisplayFrame;
end;

procedure TF_ImportStrip.btnNextFrameClick(Sender: TObject);
begin
  if fDisplayFrame = fFrameImages.Count-1 then Exit;
  fDisplayFrame := fDisplayFrame + 1;
  ShowDisplayFrame;
end;

procedure TF_ImportStrip.ShowDisplayFrame;
begin
  imgFrame.Bitmap.Assign(fFrameImages[fDisplayFrame]);
  ScaleDisplayImage;
end;

procedure TF_ImportStrip.ScaleDisplayImage;
var
  TempBmp: TBitmap32;
  x, y, w, h: Integer;
  xx, yy: Integer;
begin
  TempBmp := TBitmap32.Create;
  TempBmp.Assign(imgFrame.Bitmap);
  w := imgFrame.Bitmap.Width;
  h := imgFrame.Bitmap.Height;
  while (w > imgFrame.Width)
     or (h > imgFrame.Height) do
  begin
    w := w div 2;
    h := h div 2;
  end;

  while (w * 2 < imgFrame.Width)
    and (h * 2 < imgFrame.Height) do
  begin
    w := w * 2;
    h := h * 2;
  end;

  imgFrame.Bitmap.SetSize(imgFrame.Width, imgFrame.Height);
  imgFrame.Bitmap.Clear(ColorToRGB(imgFrame.Color));

  x := (imgFrame.Bitmap.Width - w) div 2;
  y := (imgFrame.Bitmap.Height - h) div 2;

  for yy := 0 to TempBmp.Height-1 do
    for xx := 0 to TempBmp.Width-1 do
      if (TempBmp.Pixel[xx, yy] and $FF000000) = 0 then
        TempBmp.Pixel[xx, yy] := ColorToRGB(imgFrame.Color);


  TempBmp.DrawTo(imgFrame.Bitmap, Rect(x, y, x+w, y+h));
  imgFrame.Update;
  TempBmp.Free;
end;

procedure TF_ImportStrip.ebFrameCountChange(Sender: TObject);
begin
  UpdateImageSet;
end;

procedure TF_ImportStrip.rgOrientationClick(Sender: TObject);
begin
  UpdateImageSet;
end;

procedure TF_ImportStrip.rgTransparencyClick(Sender: TObject);
begin
  UpdateImageSet;
end;

procedure TF_ImportStrip.imgFrameClick(Sender: TObject);
var
  ClickPoint: TPoint;
begin
  if rgTransparency.ItemIndex = 0 then Exit;
  ClickPoint := Mouse.CursorPos;
  ClickPoint := imgFrame.ScreenToClient(ClickPoint);
  ClickPoint := imgFrame.ControlToBitmap(ClickPoint);
  fTransparentColor := imgFrame.Bitmap.Pixel[ClickPoint.X, ClickPoint.Y];
  UpdateImageSet;
end;

end.
