NeoLemmix Graphic Set Tool V10.13.14
by namida

Thanks to...
All developers / contributors of Graphics32 library
Anders Melander for GIFImage library
Brent Sherwood for Delphi zLib library
ccexplore for documentation of DOS Lemmings formats
EricLang and Mindless for DOS .DAT file routines
Gustavo Huffenbacher Daud for PngDelphi library
Stefan Haymann for XML Parser library

----------------------------------------------------

-=CHANGELOG=-

V10.13.14
> Piece names can be imported from another DAT graphic set.
> Receivers no longer ask for a trigger area size, in line with NL
  V10.13.XX mechanics where they only use a single point.

V10.12.13
> Added a "Lemmini Resolution" checkbox when importing images or strips.
  This will cause GSTool to resize them to NeoLemmix resolution.

V10.12.12
> Dropped leading zeros from version number
> Graphic sets can specify lemming sprites other than "default" or "xmas"

V10.010.011
> Added the ability to mass-rename pieces

V10.010.010
> Changed to new versioning scheme
> Asks the user before exiting / opening a new set without saving
> Hard-resizes all graphic sets to NeoLemmix resolution

V1.48n
> Added "Sketch" to the list of object types
> Allows editing sound effects for locked exits and unlock buttons
> Trigger area settings are disabled for background objects

V1.47n
> Exporting to SuperLemmini format is no longer supported
> Added / removed options to reflect what's supported in NeoLemmix V1.47n
> A prompt is now given when saving to avoid overwriting a file

V1.03
>  Adds the ability to preview an object's animation.

V1.02
>  Added the option to define whether a graphic set uses the regular lemmings or the
   Xmas ones.

V1.01
>  Removed the option to export to LemSet-compatible INI/PNG collections, as LemSet is
   entirely obsolete
>  Removed the option to export to the obsolete Ground / VGAGR format

V1.00
>  Added the "Single-Use Trap" object type
>  Fixed some bugs relating to transparency when loading GIF images, particularly in custom
   Lemmini graphic sets

V0.99
>  Fixed the bug where error messages would be generated if the frame count input in the
   Import Strip dialog was empty or not a number.
>  Fixed the bug where preview frame and key frame numbers weren't being saved in INI/PNG
   export.

V0.98
>  Cheapo graphic sets can now be loaded directly from STY files.
>  Added support for graphic sets that contain sound effects (supported only in Full INI/PNG,
   NeoLemmix and Cheapo formats).
>  Added support for loading custom sound effects from Cheapo graphic sets.
>  Added support for the "Scrolling Background" object type added in NeoLemmix V1.32n.
>  Added support for the "Pseudorandom Starting Frame" option added in NeoLemmix V1.32n.
>  Fixed the bug where terrain/object sizes in the listing wouldn't update following the
   import of new images until some other property that affects the list (usually the trigger
   type) was changed.
>  Fixed the bug where INI/PNG export (whether LemSet-Compatible or Full) would fail on
   graphic sets that don't contain at least two objects.

V0.9 Beta
>  Now supports loading Lemmini and SuperLemmini graphic sets.
>  Now supports saving SuperLemmini graphic sets.
>  Object image strips can now be imported from GIF files.
>  Transparency in palette-based PNG files is now supported.
>  Supports a few extra sound effects for triggered objects.

V0.8 Beta
>  Object graphics can now be imported and exported (in BMP or PNG) format as strips; ie: images
   that contain all frames of an object arranged horizontally or vertically. The buttons to do this
   are right below the single frame import/export buttons.
>  The new NeoLemmix graphic set format (supported in NeoLemmix from V1.31n onwards) is now
   supported. This format avoids many of the limitations associated with the old format, such as
   the maximum size of 248x255 per piece/frame, the requirement of widths divisible by 8, and so on.
>  Fixed a bug where all pixels were treated as non-transparent in imported Cheapo graphic sets.
>  Fixed a bug where the frame order of entrances in imported Cheapo graphic sets would remain in
   the Cheapo order (ie: first frame closed, middle frames opening sequence, last frame open) instead
   of being changed to the NeoLemmix order (ie: first frame open, second frame closed, remaining
   frames opening sequence).
>  Fixed a bug where the "black" background might be colors other than black (in particular, often
   it would be white) on graphics imported from PNG files.
>  Fixed a bug where if you alter the trigger areas while Display Trigger Areas is enabled, the
   displayed trigger area wouldn't update until you did something else that caused the image to
   be refreshed (such as viewing a different frame, or turning Display Trigger Areas off and on
   again).

----------------------------------------------------

This tool, intended as a more user-friendly replacement for LemSet, allows you to create
and modify NeoLemmix graphic sets. This is still an early version at the moment, so many
features are not yet implemented.

Most of the options are self-explanatory to those who know their way around Lemmings. A
brief overview of some questions that might come up:


Q:  What image file formats are currently supported?
A:  For loading/saving terrain pieces or image frames, *only* PNG is supported at the moment.
    This must already have transparency information in the file; the tool has no means of adding
    it.
    BMP (but not GIF yet), and PNG without transparency information, are supported only when
    importing LemSet source files or importing objects as strips (rather than individual frames).

Q:  When importing strips from BMP or non-transparent PNGs, how can I choose which parts are
    transparent?
A:  By default, it'll treat any "death" magenta (RGB 255, 0, 255) pixel in the source image as
    transparent. If you want to choose a different color instead, click on that color in the
    preview image.

Q:  What if I don't want *any* color to be transparent in these images, but I accidentally
    clicked one.
A:  Use the "alpha channel" transparency option. Because the imported image has no alpha
    channel, this will result in every pixel being treated as solid.

Q:  How can I have transparency when making a PNG file?
A:  Use something that isn't MS Paint. My personal favorite is Paint.Net (it's free!), another
    popular free alternative is GIMP. Most non-free graphics programs (such as Photoshop) will
    also support it. If given the option, be sure to select 32-bit depth when saving.

Q:  What graphic set formats are currently supported?
A:  For loading:
      - NeoLemmix graphic set (xxxx.DAT)
      - DOS graphic set (GROUNDxO.DAT / VGAGRx.DAT)
      - Lemmix graphic set (GROUNDxO.DAT / VGAGRx.DAT) ~ 4-bit, 5-bit or 19-bit
      - Old NeoLemmix graphic set (GROUNDxO.DAT / VGAGRx.DAT or G_xxxx.DAT / V_xxxx.DAT) ~ 4-bit, 5-bit or 19-bit
      - Old Lemmix/NeoLemmix special graphic (VGASPECx.DAT / X_xxxx.DAT) ~ 3-bit or 18-bit
      - Lemmini / SuperLemmini graphic sets (folder containing GIF/PNG and INI files)
      - Extracted Cheapo graphic set (folder containing PNG and XML files)
      - LemSet graphic set source code (folder containing BMP/PNG and INI files)
      - INI / PNG files (derivative of LemSet format, created by this tool)
    For saving:
      - NeoLemmix graphic set (xxxx.DAT)
      - Old NeoLemmix graphic set (GROUNDxO.DAT / VGAGRx.DAT or G_xxxx.DAT / V_xxxx.DAT) ~ 19-bit only
      - SuperLemmini graphic sets (folder containing PNG and INI files)
      - LemSet graphic set source code (folder containing PNG and INI files)
      - INI / PNG files (derivative of LemSet format)

Q:  Wait, LemSet source, SuperLemmini graphic sets and the new INI/PNG format are all just an INI file with
    some images, right? Why are they three seperate options?
A:  Because the structure of the INI file, as well as the filenaming scheme of the graphic files, differ
    between them. Likewise, the "Full" export option won't be handled too well by LemSet; using the specific
    LemSet option ensures LemSet-compatibility - LemSet will understand it to a point, but not 100%.

Q:  How can I load or save a format other than NeoLemmix graphic sets?
A:  Use the "Import" / "Export" menu options. (Conversely, if you wish to load/save NeoLemmix graphic
    sets, use File -> Load/Save)

Q:  Can I use this tool to make VGASPEC files?
A:  You can use it only to make new-format VGASPEC files, as these are actually just graphic sets that
    only contain a single terrain piece and no objects.

Q:  What graphic set formats are planned for future support?
A:  For loading:
      - Cheapo graphic set (STY file)

Q:  How do I load a DOS, Lemmix, or old-format NeoLemmix graphic set?
A:  Choose Import -> GROUND / VGAGR. You'll be asked to select the files, select the GROUND file first.
    You'll then be asked to choose the VGAGR file; if it can be deduced from the GROUND file name, the
    filename field will be autocompleted with the most likely VGAGR filename (but if this is incorrect,
    you can choose a correct one).

Q:  How do I load a LemSet source or exported INI / PNG graphic set?
A:  Choose Import -> INI/PNG Or LemSet. You'll be asked to select the graphic set's style.ini file.

Q:  How do I load a Cheapo style?
A:  At the moment, this tool does not support directly loading Cheapo STY files. They must first be
    extracted with Essman's Cheapo Style Extractor, which can be found here:
      http://www.neolemmix.com/lemtools/CheapStyle.zip
    Once extracted, choose Import -> Cheapo (Extracted), then select any XML file belonging to the
    style. Please note that gravity changer objects are not supported by any engine that this tool
    can save graphic sets for, and as such, they will be converted to no-effect objects. All other
    object types in Cheapo are supported in NeoLemmix, and thus supported by this tool.

Q:  How do I save an old format NeoLemmix graphic set?
A:  Choose Export -> GROUND / VGAGR. You'll be asked to enter filenames; first for the GROUND file.
    If you enter a filename in the pattern of either GROUNDxO.DAT or G_xxxx.DAT, then when it asks
    for a filename for the VGAGR file, it will autocomplete the filename field with the matching
    VGAGR name (eg. ground42o.dat will give vgagr42.dat; g_stuff.dat will give v_stuff.dat). Note
    that these exported sets are generally not compatible with anything other than NeoLemmix.

Q:  How do I save to INI files and images?
A:  Choose Export -> INI / PNG -> either option. The "LemSet Compatible" option will strip out anything
    that LemSet can't handle, and save the images with a color that is to be interpreted as transparent.
    The "Full" option is not compatible with LemSet; some extra data is included in the INI, and the
    images use the alpha channel for transparency rather than a transparent color. You'll be asked to
    choose a "style.ini" file (obviously this is more a matter of where you want to place it than what
    you want to call it); you can enter any filename though it will still save as style.ini.

Q:  Why support SuperLemmini? Isn't it just a text file and some image files anyway?
A:  Indeed, but editing it by hand can be quite tedious, especially for less-advanced users. This tool
    also will be able to act as a quick way to convert graphic sets between NeoLemmix and SuperLemmini,
    as well as from Cheapo to either of those.

Q:  How do I assign multiple sound effects to an object in SuperLemmini?
A:  This is currently not supported. You would have to manually edit the style's INI file after exporting
    it if you want to do this. The same goes for sound effects that can't be selected in the editor.
    Remember that this tool is primarily intended for NeoLemmix; SuperLemmini support is just a bonus.

Q:  Why are half the object data options disabled when I try to edit them?
A:  Because they're not used (or have a fixed value) for the type of object you're editing. For example,
    a trap always has Triggered Animation; and no object other than Two-Way Teleporters and Self-Contained
    Teleporters make use of the secondary trigger area (and even they currently only use the position,
    not the size).

Q:  I want a triggered exit, but I want the usual "Yippee" sound, but it isn't on the list. How can I get it?
A:  Just set the sound to "None". If triggered exits don't have any sound assigned, the Yippee sound will
    play when they're triggered.

Q:  Are special frame orders needed for any objects?
A:  Yes, a few do need the frames to be in specific orders:
      - Exit: Triggered animation exits, first frame should be while idle, the rest should be the animation
              when a lemming is exiting. Constant animation exits have no special requirements.
      - Trap: First frame should be while idle, the rest should be the animation of it killing the lemming.
      - Teleporter: First frame should be while idle, the rest should be the animation of the lemming being
                    teleported. Use the Keyframe to mark when the corresponding receiver should begin to
                    animate.
      - Receiver: First frame should be while idle, the rest should be the animation of the lemming being
                  received. Use the Keyframe to mark when the lemming should be released.
      - Pre-Placed Lemming: First frame should be facing right, second frame should be facing left. Do note
                            that the graphic on pre-placed lemmings is purely for the sake of level editors;
                            the NeoLemmix engine replaces them (even on the preview screen) with the actual
                            lemming graphics.
      - Pickup Skill: The first frame should be the "empty" state; ie: after the skill has been picked up.
                      The remaining frames should be, in order, Climber, Floater, Bomber, Blocker, Builder,
                      Basher, Miner, Digger, Walker, Swimmer, Glider, Mechanic, Stoner, Platformer, Stacker,
                      Cloner. Not having all 17 frames may cause crashes; if you don't wish to include some
                      of these, use a blank frame instead of skipping it or having less than 17 frames total.
      - Locked Exit: First frame should be fully open, second fully closed. The remainder is the opening animation.
      - Secret Level Trigger: No special requirements, but keep in mind that these are invisible in-game.
      - Unlock Button: First frame should be pressed state, second frame unpressed. Other frames are ignored.
      - Splitter: First frame should be pointing right, second frame pointing left. Other frames are ignored.
      - Entrance: First frame should be fully open, second fully closed. The remainder is the opening animation.
      - Triggered Animation: First frame should be while idle, the rest should be the animation.
      - Hint: These are currently not displayed, nor do they do anything, so no advice can be given on how to use
              them. The best answer is simply don't use them until they actually do something.
      - Two-Way Teleporter: First frame should be while idle, the remainder while working. The Keyframe denotes when
                            a receiver will start to animate (when acting as a teleporter) or when the lemming will
                            be released (when acting as a receiver).
      - Self-Contained Teleporter: First frame shoudl be while idle, the remainder while working. The Keyframe
                                   denotes when the lemming will be released.

Q:  Are the graphic set colors really important?
A:  The first one, yes. It determines the color of builder bridges, the minimap, and with some MAIN.DAT files
    (such as the Lemmings Plus III one), the color of certain parts of the skill panel. The other 7 colors are
    only used in explosion particles, and as such aren't too important to set.

Q:  Certain objects are showing up with a different preview frame than I set, why?
A:  There are two causes of this. Firstly is that some objects, the preview frame is overridden by NeoLemmix.
    Examples of this are the locked exits and the pickup skills. The second possibility - and this only applies
    to the old (GROUND / VGAGR) format - is that the object's graphics are not within the first 64KB of object
    graphic data once the file is decompressed. Due to file format limitations, when this happens, the preview
    frame will always be the first one (except in the specific cases where it's automatically overridden).
    The newer (single-file) NeoLemmix graphic set format does not have this limitation.

Q:  If I only save it in the old NeoLemmix format, rather than INI/PNG Full or the new format, what will I lose?
A:  Not much. The current NeoLemmix format is limited to 18-bit (plus 1-bit alpha) color depth, so a small bit
    of image quality may be lost, though the amount lost is virtually imperceptible (and it is not made worse
    by re-loading and re-saving the graphic set; it's only a one-off thing the first time). Aside from that,
    preview frame numbers may be lost in some cases; see the above question on these. One last effect of the
    old format is that all image sizes must be divisible by 8, so if any pieces are not, they'll be padded
    to the nearest multiple of 8. This will not result in any lost data, but may somewhat impact horizontally
    flipped pieces' positions.

Q:  What about the new NeoLemmix format? Is anything lost there, or is it completely lossless?
A:  Pretty much nothing. One thing that may be altered is, in the case that an object has animation frames
    that are not all the same size, the smaller ones will be padded to the same size as the largest one. This
    is not strictly speaking a limitation of the format - it is possible to store object frames of different
    sizes - but is done to prevent graphical oddities when the object is flipped/inverted. Another detail,
    also relatively unimportant, is that any pixel that is completely transparent will not have its RGB
    values saved; they'll instead be changed to 0, 0, 0. This *is* a format limitation; it is designed
    this way because NeoLemmix has no need to know the "color" of a 100%-transparent pixel.
    (It should be noted that even if you were to, via other tools or manual hex-editing, make a graphic set
    with objects that do have different sized frames; NeoLemmix itself also adjusts them to a standard size.)



Q:  I have a question that isn't covered here.
A:  Then, I can probably cover it here:
      http://www.lemmingsforums.net/index.php?topic=2070.0