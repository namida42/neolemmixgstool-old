object Form_SoundMgr: TForm_SoundMgr
  Left = 224
  Top = 282
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Sounds'
  ClientHeight = 257
  ClientWidth = 124
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object cbSound1: TCheckBox
    Left = 8
    Top = 8
    Width = 105
    Height = 25
    Caption = 'Custom Sound 1'
    TabOrder = 0
    OnClick = cbSoundClick
  end
  object cbSound2: TCheckBox
    Left = 8
    Top = 32
    Width = 105
    Height = 25
    Caption = 'Custom Sound 2'
    TabOrder = 1
    OnClick = cbSoundClick
  end
  object cbSound3: TCheckBox
    Left = 8
    Top = 56
    Width = 105
    Height = 25
    Caption = 'Custom Sound 3'
    TabOrder = 2
    OnClick = cbSoundClick
  end
  object cbSound4: TCheckBox
    Left = 8
    Top = 80
    Width = 105
    Height = 25
    Caption = 'Custom Sound 4'
    TabOrder = 3
    OnClick = cbSoundClick
  end
  object cbSound5: TCheckBox
    Left = 8
    Top = 104
    Width = 105
    Height = 25
    Caption = 'Custom Sound 5'
    TabOrder = 4
    OnClick = cbSoundClick
  end
  object cbSound6: TCheckBox
    Left = 8
    Top = 128
    Width = 105
    Height = 25
    Caption = 'Custom Sound 6'
    TabOrder = 5
    OnClick = cbSoundClick
  end
  object cbSound7: TCheckBox
    Left = 8
    Top = 152
    Width = 105
    Height = 25
    Caption = 'Custom Sound 7'
    TabOrder = 6
    OnClick = cbSoundClick
  end
  object cbSound8: TCheckBox
    Left = 8
    Top = 176
    Width = 105
    Height = 25
    Caption = 'Custom Sound 8'
    TabOrder = 7
    OnClick = cbSoundClick
  end
  object cbSound9: TCheckBox
    Left = 8
    Top = 200
    Width = 105
    Height = 25
    Caption = 'Custom Sound 9'
    TabOrder = 8
    OnClick = cbSoundClick
  end
  object cbSound10: TCheckBox
    Left = 8
    Top = 224
    Width = 105
    Height = 25
    Caption = 'Custom Sound 10'
    TabOrder = 9
    OnClick = cbSoundClick
  end
end
