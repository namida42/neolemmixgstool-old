unit FMassName;

interface

uses
  PngInterface, LemGraphicSet,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GR32, StdCtrls, GR32_Image, ExtCtrls;

type
  TFMassRename = class(TForm)
    lbPieces: TListBox;
    ebName: TEdit;
    Label1: TLabel;
    Img: TImage32;
    btnOk: TButton;
    btnCancel: TButton;
    lblType: TLabel;
    procedure btnOkClick(Sender: TObject);
    procedure lbPiecesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ebNameChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    fImages: TBitmaps;
    procedure SetDisplayedImage(aBMP: TBitmap32);
  public
    procedure SetGraphicSet(aGS: TBaseGraphicSet);
  end;

implementation

{$R *.dfm}

procedure TFMassRename.FormCreate(Sender: TObject);
begin
  fImages := TBitmaps.Create(false);
  Img.Bitmap.SetSize(200, 200);
end;

procedure TFMassRename.FormDestroy(Sender: TObject);
begin
  fImages.Free;
end;

procedure TFMassRename.SetGraphicSet(aGS: TBaseGraphicSet);
var
  i: Integer;
  f: Integer;
begin
  lbPieces.Clear;
  for i := 0 to aGS.MetaTerrains.Count-1 do
  begin
    lbPieces.AddItem(aGS.MetaTerrains[i].Name, aGS.MetaTerrains[i]);
    fImages.Add(aGS.TerrainImages[i]);
  end;
  for i := 0 to aGS.MetaObjects.Count-1 do
  begin
    lbPieces.AddItem(aGS.MetaObjects[i].Name, aGS.MetaObjects[i]);
    if aGS.MetaObjects[i].PreviewFrame < aGS.ObjectImages[i].Count then
      fImages.Add(aGS.ObjectImages[i][aGS.MetaObjects[i].PreviewFrame])
    else
      fImages.Add(aGS.ObjectImages[i][0]);
  end;
end;

procedure TFMassRename.btnOkClick(Sender: TObject);
var
  i: Integer;
  Item: TObject;
  T: TMetaTerrain absolute Item;
  O: TMetaObject absolute Item;
begin
  for i := 0 to lbPieces.Items.Count-1 do
  begin
    Item := lbPieces.Items.Objects[i];
    if Item is TMetaTerrain then
      T.Name := lbPieces.Items[i]
    else
      O.Name := lbPieces.Items[i];
  end;
  self.ModalResult := mrOk;
end;

procedure TFMassRename.lbPiecesClick(Sender: TObject);
var
  Item: TObject;
  T: TMetaTerrain absolute Item;
  O: TMetaObject absolute Item;
begin
  Item := lbPieces.Items.Objects[lbPieces.ItemIndex];
  if Item is TMetaTerrain then
  begin
    if T.Steel then
      lblType.Caption := 'Terrain (Steel)'
    else
      lblType.Caption := 'Terrain';
  end else begin
    case O.TriggerType of
      0: lblType.Caption := 'No Trigger';
      1: lblType.Caption := 'Exit';
      2: lblType.Caption := 'One-Way Field Left';
      3: lblType.Caption := 'One-Way Field Right';
      4: lblType.Caption := 'Trap';
      5: lblType.Caption := 'Water';
      6: lblType.Caption := 'Fire';
      7: lblType.Caption := 'One-Way Arrows Left';
      8: lblType.Caption := 'One-Way Arrows Right';
      9: lblType.Caption := 'Steel Area';
      10: lblType.Caption := 'Anti-Blocker Field';
      11: lblType.Caption := 'Teleporter';
      12: lblType.Caption := 'Receiver';
      13: lblType.Caption := 'Pre-Placed Lemming';
      14: lblType.Caption := 'Pickup Skill';
      15: lblType.Caption := 'Locked Exit';
      16: lblType.Caption := 'Sketch';
      17: lblType.Caption := 'Unlock Button';
      18: lblType.Caption := 'Radiation';
      19: lblType.Caption := 'One-Way Arrows Down';
      20: lblType.Caption := 'Updraft';
      21: lblType.Caption := 'Splitter';
      22: lblType.Caption := 'Slowfreeze';
      23: lblType.Caption := 'Entrance';
      24: lblType.Caption := 'Triggered Animation';
      25: lblType.Caption := 'Hint';
      26: lblType.Caption := 'Anti-Splat Pad';
      27: lblType.Caption := 'Splat Pad';
      28: lblType.Caption := '(removed)';
      29: lblType.Caption := '(removed)';
      30: lblType.Caption := 'Moving Background';
      31: lblType.Caption := 'Single-Use Trap';
      32: lblType.Caption := 'Background Image';
    end;
  end;

  lblType.Left := (Img.Left + (Img.Width div 2)) - (lblType.Width div 2);

  ebName.Text := lbPieces.Items[lbPieces.ItemIndex];
  SetDisplayedImage(fImages[lbPieces.ItemIndex]);

  ebName.SetFocus;
  ebName.SelectAll;
end;

procedure TFMassRename.SetDisplayedImage(aBMP: TBitmap32);
var
  UseSize: Integer;
  SizeMod: Integer;
const
  ImgSize = 217;
begin
  if (aBMP = nil) or (aBMP.Width = 0) or (aBMP.Height = 0) then
  begin
    Img.Bitmap.SetSize(ImgSize, ImgSize);
    Img.Bitmap.Clear($FF000000);
    Exit;
  end;

  if aBMP.Width > aBMP.Height then
    UseSize := aBMP.Width
  else
    UseSize := aBMP.Height;

  if UseSize > ImgSize then
  begin
    SizeMod := ((UseSize - 1) div ImgSize) + 1;
    Img.Bitmap.SetSize(aBMP.Width div SizeMod, aBMP.Height div SizeMod);
  end else begin
    SizeMod := ImgSize div UseSize;
    Img.Bitmap.SetSize(aBMP.Width * SizeMod, aBMP.Height * SizeMod);
  end;

  Img.Bitmap.Clear($FF000000);
  aBMP.DrawMode := dmBlend;
  aBMP.CombineMode := cmMerge;

  aBMP.DrawTo(Img.Bitmap, Img.Bitmap.BoundsRect, aBMP.BoundsRect);

end;

procedure TFMassRename.ebNameChange(Sender: TObject);
begin
  lbPieces.Items[lbPieces.ItemIndex] := ebName.Text;
end;

procedure TFMassRename.FormShow(Sender: TObject);
begin
  lbPieces.ItemIndex := 0;
  lbPiecesClick(lbPieces);
end;

procedure TFMassRename.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  OldIndex: Integer;
begin
  OldIndex := lbPieces.ItemIndex;

  if (Key = VK_UP) and (lbPieces.ItemIndex > 0) then
    lbPieces.ItemIndex := lbPieces.ItemIndex - 1;

  if (Key = VK_DOWN) and (lbPieces.ItemIndex < lbPieces.Count-1) then
    lbPieces.ItemIndex := lbPieces.ItemIndex + 1;

  if lbPieces.ItemIndex <> OldIndex then
  begin
    Key := 0;
    lbPiecesClick(lbPieces);
  end;
end;

end.
