object FMassRename: TFMassRename
  Left = 203
  Top = 222
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Mass Rename'
  ClientHeight = 409
  ClientWidth = 449
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 208
    Top = 56
    Width = 208
    Height = 13
    Caption = 'Use the Up and Down keys to switch piece.'
  end
  object lblType: TLabel
    Left = 296
    Top = 336
    Width = 3
    Height = 13
  end
  object lbPieces: TListBox
    Left = 16
    Top = 16
    Width = 153
    Height = 377
    ItemHeight = 13
    TabOrder = 0
    OnClick = lbPiecesClick
  end
  object ebName: TEdit
    Left = 256
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 1
    OnChange = ebNameChange
  end
  object Img: TImage32
    Left = 200
    Top = 112
    Width = 217
    Height = 217
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baCenter
    Scale = 1.000000000000000000
    ScaleMode = smScale
    TabOrder = 2
  end
  object btnOk: TButton
    Left = 240
    Top = 360
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 3
    OnClick = btnOkClick
  end
  object btnCancel: TButton
    Left = 328
    Top = 360
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
end
