object FAnimPreview: TFAnimPreview
  Left = 218
  Top = 292
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = 'Object Preview'
  ClientHeight = 636
  ClientWidth = 1289
  Color = clBlack
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btnOk: TButton
    Left = 184
    Top = 592
    Width = 75
    Height = 25
    Caption = 'Exit'
    ModalResult = 1
    TabOrder = 0
  end
  object I32Frame: TImage32
    Left = 80
    Top = 232
    Width = 192
    Height = 192
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baTopLeft
    Scale = 1.000000000000000000
    ScaleMode = smNormal
    TabOrder = 1
  end
  object btnZoomIn: TButton
    Left = 24
    Top = 592
    Width = 75
    Height = 25
    Caption = 'Zoom In'
    TabOrder = 2
    OnClick = btnZoomInClick
  end
  object btnZoomOut: TButton
    Left = 104
    Top = 592
    Width = 75
    Height = 25
    Caption = 'Zoom Out'
    TabOrder = 3
    OnClick = btnZoomOutClick
  end
  object FrameAdvanceTimer: TTimer
    Enabled = False
    Interval = 60
    OnTimer = FrameAdvanceTimerTimer
    Left = 8
    Top = 8
  end
end
