unit FSoundMgr;

interface

uses
  LemGraphicSet,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm_SoundMgr = class(TForm)
    cbSound1: TCheckBox;
    cbSound2: TCheckBox;
    cbSound3: TCheckBox;
    cbSound4: TCheckBox;
    cbSound5: TCheckBox;
    cbSound6: TCheckBox;
    cbSound7: TCheckBox;
    cbSound8: TCheckBox;
    cbSound9: TCheckBox;
    cbSound10: TCheckBox;
    procedure cbSoundClick(Sender: TObject);
  private
    cbArray: Array[0..255] of TCheckBox;
    fGS: TBaseGraphicSet;
    procedure SetGraphicSet(aValue: TBaseGraphicSet);
  public
    constructor Create(aOwner: TComponent); override;
    property GS: TBaseGraphicSet read fGS write SetGraphicSet;
  end;

var
  Form_SoundMgr: TForm_SoundMgr;

implementation

{$R *.dfm}

constructor TForm_SoundMgr.Create(aOwner: TComponent);
var
  i: Integer;
begin
  inherited;
  for i := 0 to 255 do
    cbArray[i] := nil; //just in case

  // meh. have to do this manually. there's probably some leet delphi haxx0r way of doing it
  // but I am not a leet delphi haxx0r
  cbArray[23] := cbSound1;
  cbArray[24] := cbSound2;
  cbArray[25] := cbSound3;
  cbArray[26] := cbSound4;
  cbArray[27] := cbSound5;
  cbArray[28] := cbSound6;
  cbArray[29] := cbSound7;
  cbArray[30] := cbSound8;
  cbArray[31] := cbSound9;
  cbArray[32] := cbSound10;

  // this part I can automate. kinda futureproofed too.
  for i := 0 to 255 do
    if cbArray[i] <> nil then cbArray[i].Tag := i;
end;

procedure TForm_SoundMgr.SetGraphicSet(aValue: TBaseGraphicSet);
var
  i: Integer;
begin
  fGS := aValue;
  for i := 23 to 32 do
  begin
    cbArray[i].OnClick := nil;
    cbArray[i].Checked := (fGS.Sounds[i] <> nil);
    cbArray[i].OnClick := cbSoundClick;
  end;
end;

procedure TForm_SoundMgr.cbSoundClick(Sender: TObject);
var
  OpenDlg: TOpenDialog;
  SendCB: TCheckBox;
  i: Integer;
  fn: String;
begin
  SendCB := TCheckBox(Sender);
  if SendCB.Checked then
  begin
    OpenDlg := TOpenDialog.Create(self);
    OpenDlg.Title := 'Select sound file';
    OpenDlg.Filter := 'WAV files|*.wav';
    OpenDlg.Options := [ofFileMustExist, ofHideReadOnly];
    if not OpenDlg.Execute then
    begin
      SendCB.OnClick := nil;
      SendCB.Checked := false;
      SendCB.OnClick := cbSoundClick;
      OpenDlg.Free;
      Exit;
    end;
    fn := OpenDlg.FileName;
    OpenDlg.Free;
    if fGS.Sounds[SendCB.Tag] <> nil then fGS.Sounds[SendCB.Tag].Free;
    fGS.Sounds[SendCB.Tag] := TMemoryStream.Create;
    fGS.Sounds[SendCB.Tag].LoadFromFile(fn);
  end else begin
    i := MessageDlg('Are you sure you want to delete this custom sound?', mtCustom, [mbYes, mbNo], 0);
    if i = 7 {mrNo} then
    begin
      SendCB.OnClick := nil;
      SendCB.Checked := true;
      SendCB.OnClick := cbSoundClick;
      Exit;
    end;
    fGS.Sounds[SendCB.Tag].Free;
    fGS.Sounds[SendCB.Tag] := nil;
  end;
end;

end.
