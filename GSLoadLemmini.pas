unit GSLoadLemmini;

// Loads a Lemmini or SuperLemmini graphic set into the editor's internal format.
// To use, create a TLemminiGraphicSet object, then call:
//
// <TBaseGraphicSet> := <TLemminiGraphicSet>.LoadGraphicSet(<INI file>);
//
// The TBaseGraphicSet object is created during TDosGraphicSet.LoadGraphicSet, so you should
// not use <TBaseGraphicSet> := TBaseGraphicSet.Create; beforehand.
//
// Once TDosGraphicSet.LoadGraphicSet has been allowed to run, there is no need to keep the
// TDosGraphicSet object around, it can be freed.

// Saving graphic sets is similar, but use:
//
// <TLemminiGraphicSet>.SaveGraphicSet(<INI file>, <TBaseGraphicSet>);
//
// Saving is only supported for SuperLemmini.

interface

uses
  Dialogs,
  Classes, SysUtils, LemGraphicSet, GR32, PngInterface;

type
  TLemminiGraphicSetClass = class of TLemminiGraphicSet;
  TLemminiGraphicSet = class(TComponent)
    private
      fSuperLemmini: Boolean;
      fTargetPath: String;
      fTargetName: String;
      fINI: TStringList;
      procedure DetectSuperLemmini;
      procedure ClearSpaces;
    public
      constructor Create(aOwner: TComponent); override;
      destructor Destroy; override;
      procedure SaveGraphicSet(fn: String; GS: TBaseGraphicSet);
      function LoadGraphicSet(fn: String): TBaseGraphicSet;
  end;

implementation

constructor TLemminiGraphicSet.Create(aOwner: TComponent);
begin
  inherited;
  fINI := TStringList.Create;
end;

destructor TLemminiGraphicSet.Destroy;
begin
  fINI.Free;
  inherited;
end;

procedure TLemminiGraphicSet.SaveGraphicSet(fn: String; GS: TBaseGraphicSet);
var
  ts: String;
  i, i2: Integer;
  x, y: Integer;
  TempBMP, TempBMP2: TBitmap32;
  mw, mh: Integer;
begin
  fTargetPath := ExtractFilePath(fn);
  fTargetName := ChangeFileExt(ExtractFileName(fn), '');
  fINI.Add('# SuperLemmini graphic set');
  fINI.Add('# Created with NeoLemmix Graphic Set Tool');
  fINI.Add('bgColor = 0x000030');
  fINI.Add('debrisColor = 0x' + IntToHex((GS.KeyColors[0] and $FFFFFF), 6));
  ts := 'particleColor = 0xffffff,0x4040e0,0x00b000,0xf0d0d0,0xf0f000,0xf02020,0x808080,' + IntToHex((GS.KeyColors[0] and $FFFFFF), 6) + ',' + IntToHex((GS.KeyColors[0] and $FFFFFF), 6);
  for i := 1 to 7 do
    ts := ts + ',' + IntToHex((GS.KeyColors[i] and $FFFFFF), 6);
  fINI.Add('');
  fINI.Add('# Terrain');
  fINI.Add('tiles = ' + IntToStr(GS.MetaTerrains.Count));
  ts := '';
  for i := 0 to GS.MetaTerrains.Count-1 do
  begin
    TempBMP := TBitmap32.Create;
    TempBMP.SetSize((GS.TerrainImages[i].Width * 16) div GS.Resolution, (GS.TerrainImages[i].Height * 16) div GS.Resolution);
    GS.TerrainImages[i].DrawTo(TempBMP, TempBMP.BoundsRect);
    SavePngFile(fTargetPath + fTargetName + '_' + IntToStr(i) + '.png', TempBMP);
    for y := 0 to TempBMP.Height-1 do
      for x := 0 to TempBMP.Width-1 do
        if (TempBMP.Pixel[x, y] and $FF000000) = 0 then
          TempBMP.Pixel[x, y] := 0
        else
          TempBMP.Pixel[x, y] := $FFFF00FF;
    SavePngFile(fTargetPath + fTargetName + 'm_' + IntToStr(i) + '.png', TempBMP);
    TempBMP.Free;
    if GS.MetaTerrains[i].Steel then
    begin
      if Length(ts) > 0 then ts := ts + ',';
      ts := ts + IntToStr(i);
    end;
  end;
  if ts <> '' then fIni.Add('steelTiles = ' + ts);

  fINI.Add('');
  fINI.Add('# Objects');

  for i := 0 to GS.MetaObjects.Count-1 do
  begin
    TempBMP := TBitmap32.Create;
    mw := 0;
    mh := 0;
    for i2 := 0 to GS.ObjectImages[i].Count-1 do
    begin
      if GS.ObjectImages[i][i2].Width > mw then mw := GS.ObjectImages[i][i2].Width;
      if GS.ObjectImages[i][i2].Height > mh then mh := GS.ObjectImages[i][i2].Height;
    end;
    if GS.MetaObjects[i].TriggerType = 23 then
      GS.ObjectImages[i].Move(0, GS.ObjectImages[i].Count-1); // Fully open entrance is final frame in SuperLemmini
    TempBMP.SetSize((mw * 16) div GS.Resolution, ((mh * 16) div GS.Resolution) * GS.ObjectImages[i].Count);
    for i2 := 0 to GS.ObjectImages[i].Count-1 do
    begin
      TempBMP2 := TBitmap32.Create;
      TempBMP2.SetSize((GS.ObjectImages[i][i2].Width * 16) div GS.Resolution, (GS.ObjectImages[i][i2].Height * 16) div GS.Resolution);
      GS.ObjectImages[i][i2].DrawTo(TempBMP2, TempBMP2.BoundsRect);
      TempBMP2.DrawTo(TempBMP, 0, ((mh * 16) div GS.Resolution)*i2);
      TempBMP2.Free;
    end;

    SavePngFile(fTargetPath + fTargetName + 'o_' + IntToStr(i) + '.png', TempBMP);

    TempBMP.SetSize((GS.MetaObjects[i].PTriggerW * 16) div GS.Resolution, (GS.MetaObjects[i].PTriggerH * 16) div GS.Resolution);
    TempBMP.Clear($FFFF00FF);

    if GS.MetaObjects[i].TriggerType in [1, 2, 3, 4, 5, 6, 7, 8, 9] then
      SavePngFile(fTargetPath + fTargetName + 'om_' + IntToStr(i) + '.png', TempBMP);

    TempBMP.Free;

    if GS.MetaObjects[i].TriggerType = 23 then
      GS.ObjectImages[i].Move(GS.ObjectImages[i].Count-1, 0); // Move it back, for obvious reasons

    case GS.MetaObjects[i].TriggerType of
      0: begin
           fINI.Add('# Object ' + IntToStr(i) + ': No effect');
           ts := '0';
         end;
      1: begin
           fINI.Add('# Object ' + IntToStr(i) + ': Exit');
           ts := '8';
         end;
      2: begin
           fINI.Add('# Object ' + IntToStr(i) + ': One-way left field');
           ts := '1';
         end;
      3: begin
           fINI.Add('# Object ' + IntToStr(i) + ': One-way right field');
           ts := '2';
         end;
      4: begin
           fINI.Add('# Object ' + IntToStr(i) + ': Triggered trap');
           ts := '6';
         end;
      5: begin
           fINI.Add('# Object ' + IntToStr(i) + ': Water');
           ts := '5';
         end;
      6: begin
           fINI.Add('# Object ' + IntToStr(i) + ': Fire');
           ts := '7';
         end;
      7: begin
           fINI.Add('# Object ' + IntToStr(i) + ': One-way wall left');
           ts := '4';
         end;
      8: begin
           fINI.Add('# Object ' + IntToStr(i) + ': One-way wall right');
           ts := '3';
         end;
      9: begin
           fINI.Add('# Object ' + IntToStr(i) + ': Steel area');
           ts := '9';
         end;
     23: begin
           fINI.Add('# Object ' + IntToStr(i) + ': Window');
           ts := '32';
         end;
      else begin
             fINI.Add('# Object ' + IntToStr(i) + ': Unsupported type, changed to no-effect');
             ts := '0';
           end;
    end;

    fINI.Add('frames_' + IntToStr(i) + ' = ' + IntToStr(GS.ObjectImages[i].Count));
    fINI.Add('speed_' + IntToStr(i) + ' = 2'); // NeoLemmix doesn't support this property
    if GS.MetaObjects[i].TriggerType = 4 then
      fINI.Add('anim_' + IntToStr(i) + ' = 2')
    else if GS.MetaObjects[i].TriggerType = 23 then
      fINI.Add('anim_' + IntToStr(i) + ' = 3')
    else if GS.ObjectImages[i].Count = 1 then
      fINI.Add('anim_' + IntToStr(i) + ' = 0')
    else
      fINI.Add('anim_' + IntToStr(i) + ' = 1');
    fINI.Add('type_' + IntToStr(i) + ' = ' + ts);
    if GS.MetaObjects[i].TriggerType = 23 then
    begin // special calculation for windows
      fINI.Add('maskOffsetX_' + IntToStr(i) + ' = ' + IntToStr((((GS.MetaObjects[i].PTriggerX * 16) div GS.Resolution) - 2)
                                                             - (((mw * 16) div GS.Resolution) div 2))); 
      fINI.Add('maskOffsetY_' + IntToStr(i) + ' = ' + IntToStr(((GS.MetaObjects[i].PTriggerY * 16) div GS.Resolution) - 20));
    end else begin
      fINI.Add('maskOffsetX_' + IntToStr(i) + ' = ' + IntToStr((GS.MetaObjects[i].PTriggerX * 16) div GS.Resolution));
      fINI.Add('maskOffsetY_' + IntToStr(i) + ' = ' + IntToStr((GS.MetaObjects[i].PTriggerY * 16) div GS.Resolution));
    end;
    case GS.MetaObjects[i].TriggerSound of
       6: ts := '6';
       7: ts := '20';
       9: ts := '21';
      13: ts := '8';
      14: ts := '19';
      15: ts := '11';
      20: ts := '24';
      21: ts := '25';
      22: ts := '26';
      else if (GS.MetaObjects[i].TriggerSound > 22) and (GS.MetaObjects[i].TriggerSound < 33) then
             ts := IntToStr(GS.MetaObjects[i].TriggerSound + 4)
           else
             ts := '-1';
    end;
    case GS.MetaObjects[i].TriggerType of // overrides on the sound for specific type, since SuperLemmini doesn't hardcode any
      1: ts := '14';
      5: ts := '17';
      6: ts := '8';
      7: ts := '3';
      8: ts := '3';
      9: ts := '3';
     23: ts := '5';
    end;
    fINI.Add('sound_' + IntToStr(i) + ' = ' + ts);
    fINI.Add('');
  end;

  fINI.SaveToFile(fTargetPath + fTargetName + '.ini') 
end;

procedure TLemminiGraphicSet.DetectSuperLemmini; // not foolproof!
var
  i: Integer;
begin
  fSuperLemmini := true; // exit on confirmation, otherwise turn off at the end
  // First - check the files. SuperLemmini can be detected by:
  //     > Any PNG file
  //     > Terrain mask files
  i := 0;
  repeat
    if FileExists(fTargetPath + fTargetName + '_' + IntToStr(i) + '.png')
    or FileExists(fTargetPath + fTargetName + 'o_' + IntToStr(i) + '.png')
    or FileExists(fTargetPath + fTargetName + 'm_' + IntToStr(i) + '.png')
    or FileExists(fTargetPath + fTargetName + 'om_' + IntToStr(i) + '.png')
    or FileExists(fTargetPath + fTargetName + 'm_' + IntToStr(i) + '.gif') then Exit;
    i := i + 1;
  until not FileExists(fTargetName + '_' + IntToStr(i) + '.gif');
  // If we get this far, we can say almost certianly that we're dealing with a
  // Lemmini graphic set, but let's check a few more-subtle things to make sure.
  if fINI.Values['steelTiles'] <> '' then Exit;
  i := 0;
  repeat
    if (fINI.Values['speed_' + IntToStr(i)] <> '')
    or (fINI.Values['maskOffsetX_' + IntToStr(i)] <> '')
    or (fINI.Values['maskOffsetY_' + IntToStr(i)] <> '') then Exit;
    i := i + 1;
  until fINI.Values['frames_' + IntToStr(i)] = '';
  // The chance of a SuperLemmini graphic set making it to this point is virtually
  // nothing. We can probably safely assume we're dealing with regular Lemmini here.

  // But let's ask the user. Extra safe.
  i := MessageDlg('This appears to be a standard Lemmini (not SuperLemmini) graphic set.' + #13 +
                  'Is this correct?', mtCustom, [mbYes, mbNo], 0);
  if i = 6 {mrYes} then fSuperLemmini := false;
end;

function TLemminiGraphicSet.LoadGraphicSet(fn: String): TBaseGraphicSet;
var
  i, i2: Integer;
  ts: String;
  tsl: TStringList;
  TempBMP, TempBMP2: TBitmap32;
  TempBMPs: TBitmaps;
  x, y: Integer;
  TempTerrain: TMetaTerrain;
  TempObject: TMetaObject;
begin
  Result := TBaseGraphicSet.Create;
  tsl := TStringList.Create;
  TempBMP := nil; // avoid the MEBE NUT INNITIALIZED EVN THO DIS WILL NEVA ACKSHULI HAPPEN LULULUL warning
  fTargetPath := ExtractFilePath(fn);
  fTargetName := ChangeFileExt(ExtractFileName(fn), '');
  fINI.LoadFromFile(fn);
  ClearSpaces;
  DetectSuperLemmini;

  Result.Resolution := 16;

  ts := fINI.Values['debrisColor'];
  Result.KeyColors[0] := $FF000000 or (TColor32(StrToIntDef(ts, $E08020)) and $FFFFFF); //Typecast not needed but avoids a compiler warning

  tsl.CommaText := fINI.Values['particleColor'];
  for i := 9 to 15 do
    Result.KeyColors[i-8] := $FF000000 or (TColor32(StrToIntDef(tsl[i], $E08020)) and $FFFFFF);


  // Load terrain
  if fSuperLemmini then
    tsl.CommaText := fINI.Values['steelTiles']
  else
    tsl.Clear;
  i := 0;
  repeat
    if FileExists(fTargetPath + fTargetName + '_' + inttostr(i) + '.png') then
      TempBMP := LoadPngFile(fTargetPath + fTargetName + '_' + inttostr(i) + '.png')
    else if FileExists(fTargetPath + fTargetName + '_' + inttostr(i) + '.gif') then
      TempBMP := LoadGifFile(fTargetPath + fTargetName + '_' + inttostr(i) + '.gif')
    else Break;

    TempBMP2 := nil;
    if FileExists(fTargetPath + fTargetName + 'm_' + inttostr(i) + '.png') then
      TempBMP2 := LoadPngFile(fTargetPath + fTargetName + 'm_' + inttostr(i) + '.png')
    else if FileExists(fTargetPath + fTargetName + 'm_' + inttostr(i) + '.gif') then
      TempBMP2 := LoadGifFile(fTargetPath + fTargetName + 'm_' + inttostr(i) + '.gif');

    if (TempBMP2 <> nil) and fSuperLemmini then
    begin
      for y := 0 to TempBMP.Height-1 do
        for x := 0 to TempBMP.Width-1 do
          if (TempBMP2.Pixel[x, y] and $FFFFFF) = 0 then TempBMP.Pixel[x, y] := 0;
      TempBMP2.Free;
    end;

    TempTerrain := TMetaTerrain.Create;
    for i2 := 0 to tsl.Count-1 do
      if StrToInt(tsl[i2]) = i then TempTerrain.Steel := true;
    Result.MetaTerrains.Add(TempTerrain);
    Result.TerrainImages.Add(TempBMP);
    i := i + 1;
  until false;

  //Load Objects
  i := 0;
  repeat
    if FileExists(fTargetPath + fTargetName + 'o_' + inttostr(i) + '.png') then
      TempBMP := LoadPngFile(fTargetPath + fTargetName + 'o_' + inttostr(i) + '.png')
    else if FileExists(fTargetPath + fTargetName + 'o_' + inttostr(i) + '.gif') then
      TempBMP := LoadGifFile(fTargetPath + fTargetName + 'o_' + inttostr(i) + '.gif')
    else Break;

    TempBMPs := TBitmaps.Create;
    for i2 := 0 to StrToInt(fINI.Values['frames_' + IntToStr(i)])-1 do
    begin
      TempBMP2 := TBitmap32.Create;
      TempBMP2.SetSize(TempBMP.Width, TempBMP.Height div StrToInt(fINI.Values['frames_' + IntToStr(i)]));
      TempBMP.DrawTo(TempBMP2, 0, 0, Rect(0, (TempBMP.Height div StrToInt(fINI.Values['frames_' + IntToStr(i)])) * i2, TempBMP.Width, (TempBMP.Height div StrToInt(fINI.Values['frames_' + IntToStr(i)])) * (i2+1)) );
      TempBMPs.Add(TempBMP2);
    end;

    TempBMP.Free;

    TempObject := TMetaObject.Create;
    case StrToInt(fINI.Values['type_' + IntToStr(i)]) of
      1: TempObject.TriggerType := 2;
      2: TempObject.TriggerType := 3;
      3: TempObject.TriggerType := 8;
      4: TempObject.TriggerType := 7;
      5: TempObject.TriggerType := 5;
      6: TempObject.TriggerType := 4;
      7: TempObject.TriggerType := 6;
      8: TempObject.TriggerType := 1;
      9: TempObject.TriggerType := 9;
     32: TempObject.TriggerType := 23;
      else TempObject.TriggerType := 0;
    end;

    TempBMP := nil;
    if FileExists(fTargetPath + fTargetName + 'om_' + inttostr(i) + '.png') then
      TempBMP := LoadPngFile(fTargetPath + fTargetName + 'om_' + inttostr(i) + '.png')
    else if FileExists(fTargetPath + fTargetName + 'om_' + inttostr(i) + '.gif') then
      TempBMP := LoadGifFile(fTargetPath + fTargetName + 'om_' + inttostr(i) + '.gif');

    if TempBMP <> nil then
    begin
      TempObject.PTriggerX := TempBMP.Width;
      TempObject.PTriggerY := TempBMP.Height;
      for y := 0 to TempBMP.Height-1 do
        for x := 0 to TempBMP.Width-1 do
          if (TempBMP.Pixel[x, y] and $FF000000) <> 0 then
          begin
            if x < TempObject.PTriggerX then TempObject.PTriggerX := x;
            if y < TempObject.PTriggerY then TempObject.PTriggerY := y;
            if (x - TempObject.PTriggerX + 1) > TempObject.PTriggerW then TempObject.PTriggerW := (x - TempObject.PTriggerX + 1);
            if (y - TempObject.PTriggerY + 1) > TempObject.PTriggerH then TempObject.PTriggerH := (y - TempObject.PTriggerY + 1);
          end;
      if TempObject.PTriggerX = TempBMP.Width then TempObject.PTriggerX := 0;
      if TempObject.PTriggerY = TempBMP.Height then TempObject.PTriggerY := 0;

      if fSuperLemmini then
      begin
        TempObject.PTriggerX := TempObject.PTriggerX + StrToIntDef(fINI.Values['maskOffsetX_' + IntToStr(i)], 0);
        TempObject.PTriggerY := TempObject.PTriggerY + StrToIntDef(fINI.Values['maskOffsetY_' + IntToStr(i)], 0);
      end else
        TempObject.PTriggerY := TempObject.PTriggerY + 8;
      if TempObject.TriggerType in [7, 8] then
      begin
        TempObject.PTriggerX := 0;
        TempObject.PTriggerY := 0;
        TempObject.PTriggerW := TempBMP.Width;
        TempObject.PTriggerH := TempBMP.Height;
      end;
    end else if Tempobject.TriggerType = 23 then
    begin
      TempObject.PTriggerX := (TempBMPs[0].Width div 2) + 2;
      TempObject.PTriggerY := 20;
      if fSuperLemmini then
      begin
        TempObject.PTriggerX := TempObject.PTriggerX + StrToIntDef(fINI.Values['maskOffsetX_' + IntToStr(i)], 0);
        TempObject.PTriggerY := TempObject.PTriggerY + StrToIntDef(fINI.Values['maskOffsetY_' + IntToStr(i)], 0);
      end;
    end;

    if TempObject.TriggerType in [4, 23] then TempObject.TriggerAnim := true;
    if TempObject.TriggerType = 23 then
      TempBMPs.Move(TempBMPs.Count-1, 0);

    tsl.CommaText := fINI.Values['sound_' + IntToStr(i)];
    for i2 := 0 to tsl.Count-1 do
      case StrToInt(tsl[i2]) of
        8: TempObject.TriggerSound := 13;
        6: TempObject.TriggerSound := 6;
        11: TempObject.TriggerSound := 15;
        19: TempObject.TriggerSound := 14;
        20: TempObject.TriggerSound := 7;
        21: TempObject.TriggerSound := 9;
        24: TempObject.TriggerSound := 20;
        25: TempObject.TriggerSound := 21;
        26: TempObject.TriggerSound := 22;
        else if (StrToInt(tsl[i2]) > 26) and (StrToInt(tsl[i2]) < 37) then
          TempObject.TriggerSound := StrToInt(tsl[i2]) - 4;
      end;

    Result.MetaObjects.Add(TempObject);
    Result.ObjectImages.Add(TempBMPs);

    i := i + 1;
  until false;

  tsl.Free;
end;

procedure TLemminiGraphicSet.ClearSpaces;
var
  i: Integer;
begin
  for i := 0 to fIni.Count-1 do
    fIni[i] := StringReplace(fIni[i], ' ', '', [rfReplaceAll]);
  // Why? Simplifies things. All the values we need to worry about
  // are non-text fields.
end;

end.