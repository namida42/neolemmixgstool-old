unit GSLoadExperimental;

interface

uses
  Dialogs, PngInterface,
  Classes, StrUtils, SysUtils, LemGraphicSet, GR32;

type
  TExperimentalGraphicSet = class
    private
      fDstPath: String;
      procedure SaveObject(GS: TBaseGraphicSet; SL: TStringList; i: Integer); overload;
      procedure SaveTerrain(GS: TBaseGraphicSet; SL: TStringList; i: Integer); overload;
    public
      procedure SaveSpecialGraphic(fn: String; GS: TBaseGraphicSet);
      procedure SaveGraphicSet(fn: String; GS: TBaseGraphicSet);
      procedure SaveObject(GS: TBaseGraphicSet; i: Integer); overload;
      procedure SaveTerrain(GS: TBaseGraphicSet; i: Integer); overload;
      property DstPath: String read fDstPath write fDstPath;
  end;

implementation

procedure TExperimentalGraphicSet.SaveSpecialGraphic(fn: String; GS: TBaseGraphicSet);
var
  BaseBmp, SteelBmp, OneWayBmp: TBitmap32;
  HasSteel, HasOneWay: Boolean;
  x, y: Integer;
  SL, TerSL: TStringList;
  OldName, NewName: String;

  function IsSolid(aColor: TColor32): Boolean;
  begin
    Result := (aColor and $FF000000) <> 0;
  end;

  procedure ClipSize(aBmp: TBitmap32);
  var
    TempBmp: TBitmap32;
  begin
    TempBmp := TBitmap32.Create;
    TempBmp.Assign(aBmp);
    aBmp.SetSizeFrom(BaseBmp);
    aBmp.Clear(0);
    TempBmp.DrawTo(aBmp, 0, 0);
    TempBmp.Free;
  end;
begin
  BaseBmp := TBitmap32.Create;
  SteelBmp := TBitmap32.Create;
  OneWayBmp := TBitmap32.Create;

  HasSteel := false;
  HasOneWay := false;

  BaseBmp.Assign(GS.TerrainImages[0]);
  BaseBmp.OuterColor := 0;
  SteelBmp.Clear(0);
  OneWayBmp.Clear(0);

  if GS.TerrainImages.Count > 2 then
  begin
    OneWayBmp.Assign(GS.TerrainImages[2]);
    for y := 0 to OneWayBmp.Height-1 do
      for x := 0 to OneWayBmp.Width-1 do
        if IsSolid(OneWayBmp.Pixel[x, y]) then
          OneWayBmp.Pixel[x, y] := BaseBmp.PixelS[x, y];
  end;

  if GS.TerrainImages.Count > 1 then
  begin
    SteelBmp.Assign(GS.TerrainImages[1]);
    for y := 0 to SteelBmp.Height-1 do
      for x := 0 to SteelBmp.Width-1 do
        if IsSolid(SteelBmp.Pixel[x, y]) then
          SteelBmp.Pixel[x, y] := BaseBmp.PixelS[x, y];
  end;

  ClipSize(SteelBmp);
  ClipSize(OneWayBmp);

  for y := 0 to BaseBmp.Height-1 do
    for x := 0 to BaseBmp.Width-1 do
      if IsSolid(SteelBmp.Pixel[x, y]) then
      begin
        BaseBmp.Pixel[x, y] := 0;
        OneWayBmp.Pixel[x, y] := 0;
        HasSteel := true;
      end else if IsSolid(OneWayBmp.Pixel[x, y]) then
      begin
        BaseBmp.Pixel[x, y] := 0;
        HasOneWay := true;
      end;

  SL := TStringList.Create;

  OldName := GS.MetaTerrains[0].Name; // SOURCE name
  NewName := ChangeFileExt(ExtractFileName(fn), '');

  SL.Add('# Translation table for special graphic set');
  SL.Add('');
  SL.Add('SPECIAL');

  SavePngFile(fn, BaseBmp);
  if HasSteel then
  begin
    SavePngFile(ChangeFileExt(fn, '_steel.png'), SteelBmp);
    TerSL := TStringList.Create;
    TerSL.Add('# Steel section of special graphic');
    TerSL.Add('STEEL');
    TerSL.SaveToFile(ChangeFileExt(fn, '_steel.nxtp'));
    TerSL.Free;
    SL.Add('  SET special');
    SL.Add('  NAME ' + NewName + '_steel');
    SL.Add('AND');
  end;

  if HasOneWay then
  begin
    SavePngFile(ChangeFileExt(fn, '_oneway.png'), OneWayBmp);
    SL.Add('  SET special');
    SL.Add('  NAME ' + NewName + '_oneway');
    SL.Add('  ONEWAY');
    SL.Add('AND');
  end;

  SL.Add('  SET special');
  SL.Add('  NAME ' + NewName);

  SL.SaveToFile(ExtractFilePath(fn) + '..\..\..\translation\x_' + OldName + '.nxtt');
  SL.Free;

  BaseBmp.Free;
  SteelBmp.Free;
  OneWayBmp.Free;
end;

procedure TExperimentalGraphicSet.SaveObject(GS: TBaseGraphicSet; i: Integer);
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  try
    SaveObject(GS, SL, i);
  finally
    SL.Free;
  end;
end;

procedure TExperimentalGraphicSet.SaveTerrain(GS: TBaseGraphicSet; i: Integer);
var
  SL: TStringList;
begin
  SL := TStringList.Create;
  try
    SaveTerrain(GS, SL, i);
  finally
    SL.Free;
  end;
end;

procedure TExperimentalGraphicSet.SaveGraphicSet(fn: String; GS: TBaseGraphicSet);
var
  SetName: String;
  SL: TStringList;
  i: Integer;
  DoObjFlags: array of Boolean;
  DoTerFlags: array of Boolean;
  IsForeignSet: Boolean;

  procedure MakeTerrainConversion(T: TMetaTerrain; var Flag: Boolean);
  var
    Name: String;
    LocalSetName: String;

    procedure ChangeNames;
    var
      SplitPos: Integer;
    begin
      SplitPos := Pos(':', Name);
      LocalSetName := LeftStr(Name, SplitPos-1);
      Name := RightStr(Name, Length(Name)-SplitPos);
      IsForeignSet := true; //(LocalSetName <> SetName);
    end;
  begin
    Flag := false;
    Name := T.Name;

    LocalSetName := SetName;
    try
      if Pos(':', Name) > 0 then
        ChangeNames
      else
        IsForeignSet := false;

      // Set and name
      if Lowercase(Name) <> '*nil' then
        SL.Add('  SET ' + LocalSetName);
      SL.Add('  NAME ' + Name);

      Name := Lowercase(Name);

      // No further info needed if it's a nil piece
      if Name = '*nil' then Exit;

      // All remaining cases need the flag on
      Flag := true;

      // Flag off if it's from another set
      if IsForeignSet then
        Flag := false;

      // In all other cases
      if T.OffsetL <> 0 then SL.Add('  OFFSET_LEFT ' + IntToStr(T.OffsetL));
      if T.OffsetT <> 0 then SL.Add('  OFFSET_TOP ' + IntToStr(T.OffsetT));
      if T.OffsetR <> 0 then SL.Add('  OFFSET_RIGHT ' + IntToStr(T.OffsetR));
      if T.OffsetB <> 0 then SL.Add('  OFFSET_BOTTOM ' + IntToStr(T.OffsetB));

      if T.ConvertFlip then
        SL.Add('  FLIP');
      if T.ConvertInvert then
        SL.Add('  INVERT');
      if T.ConvertRotate then
        SL.Add('  ROTATE');
    finally
      // Finishing blank line. It's in a "finally" so it'll always be executed
      // even if Exit is called; errors shouldn't happen.
      SL.Add('');
    end;
  end;

  procedure MakeObjectConversion(O: TMetaObject; var Flag: Boolean; W, H: Integer);
  var
    Name: String;
    LocalSetName: String;

    procedure ChangeNames;
    var
      SplitPos: Integer;
    begin
      SplitPos := Pos(':', Name);
      LocalSetName := LeftStr(Name, SplitPos-1);
      Name := RightStr(Name, Length(Name)-SplitPos);
      IsForeignSet := true; //(LocalSetName <> SetName);
    end;
  begin
    Flag := false;
    LocalSetName := SetName;
    Name := O.Name;

    if Pos(':', Name) > 0 then
      ChangeNames
    else
      IsForeignSet := false;

    try
      // If the object type is preplaced lemming...
      if O.TriggerType = 13 then
      begin
        SL.Add('  LEMMING');
        SL.Add('  OFFSET_X ' + IntToStr(O.PTriggerX));
        SL.Add('  OFFSET_Y ' + IntToStr(O.PTriggerY));
        Exit;
      end;

      if Name = '*bg' then
        Name := '*nil';

      if Name <> '*nil' then
        SL.Add('  SET ' + LocalSetName);
      SL.Add('  NAME ' + Name);

      Name := LowerCase(Name);

      // No further info needed if it's a nil piece
      if Name = '*nil' then Exit;

      // If it's a lemming, we need to set the offsets based on trigger area
      if O.TriggerType = 13 then
      begin
        if O.PTriggerX <> 2 then
          SL.Add('  OFFSET_LEFT ' + IntToStr(O.PTriggerX - 2));
        if O.PTriggerY <> 10 then
          SL.Add('  OFFSET_TOP ' + IntToStr(O.PTriggerY - 10));
        Exit;
      end;

      // All remaining cases need the flag on
      Flag := true;

      // ... We need the flag off if it's from a different set, though.
      if IsForeignSet then
        Flag := false;

      // Resizability info if applicable
      if O.ResizeHorizontal then
        SL.Add('  WIDTH ' + IntToStr(W));
      if O.ResizeVertical then
        SL.Add('  HEIGHT ' + IntToStr(H));

      // In all other cases
      if O.OffsetL <> 0 then SL.Add('  OFFSET_LEFT ' + IntToStr(O.OffsetL));
      if O.OffsetT <> 0 then SL.Add('  OFFSET_TOP ' + IntToStr(O.OffsetT));
      if O.OffsetR <> 0 then SL.Add('  OFFSET_RIGHT ' + IntToStr(O.OffsetR));
      if O.OffsetB <> 0 then SL.Add('  OFFSET_BOTTOM ' + IntToStr(O.OffsetB));

      if O.ConvertFlip then
        SL.Add('  FLIP');
      if O.ConvertInvert then
        SL.Add('  INVERT');
    finally
      // Same deal here as with terrain.
      SL.Add('');
    end;
  end;
begin
  SL := TStringList.Create;
  SetLength(DoObjFlags, GS.MetaObjects.Count);
  SetLength(DoTerFlags, GS.MetaTerrains.Count);
  try
    fn := ExtractFilePath(fn);
    ForceDirectories(fn);
    SetCurrentDir(fn);

    SetName := ExtractFileName(ExcludeTrailingPathDelimiter(fn));

    // Theme info
    SL.Add('# Theme information');
    SL.Add('');

    if GS.LemmingSprites = 'xlemming' then
      SL.Add('LEMMINGS xmas')
    else
      SL.Add('LEMMINGS default');

    SL.Add('');
    SL.Add('COLORS');
    SL.Add('  MASK ' + IntToHex(GS.KeyColors[0] and $FFFFFF, 6));
    SL.Add('  MINIMAP ' + IntToHex(GS.KeyColors[1] and $FFFFFF, 6));
    SL.Add('  BACKGROUND ' + IntToHex(GS.KeyColors[2] and $FFFFFF, 6));
    SL.Add('  PICKUP_BORDER ' + IntToHex(GS.KeyColors[3] and $FFFFFF, 6));
    SL.Add('  PICKUP_INSIDE ' + IntToHex(GS.KeyColors[4] and $FFFFFF, 6));
    SL.Add('  ONE_WAYS ' + IntToHex(GS.KeyColors[5] and $FFFFFF, 6));
    //for i := 0 to 7 do
    //  SL.Add('COLOR_PARTICLES_' + IntToStr(i) + ' ' + IntToHex(GS.KeyColors[i], 6));

    SL.SaveToFile(fn + '..\..\themes\' + SetName + '.nxtm');
    SL.Clear;

    // Translation table
    SL.Add('# Translation table');
    SL.Add('');
    SL.Add('THEME ' + SetName);
    SL.Add('');

    for i := 0 to GS.MetaTerrains.Count-1 do
    begin
      if LeftStr(GS.MetaTerrains[i].Name, 1) = '/' then
      begin
        DoTerFlags[i] := true;
        Continue;
      end;
      SL.Add('TERRAIN ' + IntToStr(i));
      MakeTerrainConversion(GS.MetaTerrains[i], DoTerFlags[i]);
    end;

    for i := 0 to GS.MetaObjects.Count-1 do
    begin
      if LeftStr(GS.MetaObjects[i].Name, 1) = '/' then
      begin
        DoObjFlags[i] := true;
        Continue;
      end;
      SL.Add('OBJECT ' + IntToStr(i));
      MakeObjectConversion(GS.MetaObjects[i], DoObjFlags[i], GS.ObjectImages[i][0].Width, GS.ObjectImages[i][0].Height);
    end;

    SL.SaveToFile(fn + '..\..\translation\' + SetName + '.nxtt');
    SL.Clear;

    ForceDirectories(fn + 'terrain\');
    SetCurrentDir(fn + 'terrain\');

    for i := 0 to GS.MetaTerrains.Count-1 do
      if DoTerFlags[i] then
        SaveTerrain(GS, SL, i);

    ForceDirectories(fn + 'objects\');
    SetCurrentDir(fn + 'objects\');

    for i := 0 to GS.MetaObjects.Count-1 do
    begin
      if Lowercase(GS.MetaObjects[i].Name) = '*bg' then
      begin
        SavePngFile(fn + '..\..\themes\' + SetName + '_bg.png', GS.ObjectImages[i][0]);
        Continue;
      end;

      if DoObjFlags[i] then
        SaveObject(GS, SL, i);
    end;
  finally
    SL.Free;
  end;
end;

procedure TExperimentalGraphicSet.SaveTerrain(GS: TBaseGraphicSet; SL: TStringList; i: Integer);
var
  Name: String;
begin
  SL.Clear;

  Name := GS.MetaTerrains[i].Name;
  if LeftStr(Name, 1) = '/' then Name := RightStr(Name, Length(Name)-1);  // why did I put this here?

  SavePngFile(Name + '.png', GS.TerrainImages[i]);
  if GS.MetaTerrains[i].Steel then
    SL.Add('STEEL');

  if SL.Count > 0 then
  begin
    SL.Insert(0, '# Terrain metainfo');
    SL.Insert(0, '');
    SL.SaveToFile(Name + '.nxtp');
  end;
end;

procedure TExperimentalGraphicSet.SaveObject(GS: TBaseGraphicSet; SL: TStringList; i: Integer);
var
  Name: String;
  MO: TMetaObject;
  OBmp: TBitmaps;
  Bmp: TBitmap32;
  S: String;
begin
  Name := GS.MetaObjects[i].Name;
  if LeftStr(Name, 1) = '/' then Name := RightStr(Name, Length(Name)-1);  // it's here too

  if Lowercase(Name) = '*bg' then Exit;

  if Lowercase(Name) = '*nil' then Exit;

  SL.Clear;
  SL.Add('# Object metainfo');
  SL.Add('');

  MO := GS.MetaObjects[i];
  OBmp := GS.ObjectImages[i];

  S := '';
  case MO.TriggerType of
     1: S := 'EXIT';
     2: S := 'OWL_FIELD';
     3: S := 'OWR_FIELD';
     4: S := 'TRAP';
     5: S := 'WATER';
     6: S := 'FIRE';
     7: S := 'OWL_ARROW';
     8: S := 'OWR_ARROW';
    11: S := 'TELEPORTER';
    12: S := 'RECEIVER';
    13: S := 'LEMMING';
    14: S := 'PICKUP';
    15: S := 'LOCKED_EXIT';
    17: S := 'BUTTON';
    18: S := 'RADIATION';
    19: S := 'OWD_ARROW';
    20: S := 'UPDRAFT';
    21: S := 'SPLITTER';
    22: S := 'SLOWFREEZE';
    23: S := 'WINDOW';
    24: S := 'ANIMATION';
    25: S := 'HINT';
    26: S := 'ANTISPLAT';
    27: S := 'SPLAT';
    30: S := 'BACKGROUND';
    31: S := 'TRAP_ONCE';
  end;
  if S <> '' then SL.Add(S);

  SL.Add('FRAMES ' + IntToStr(OBmp.Count));
  SL.Add('VERTICAL');

  if MO.ResizeHorizontal then
  begin
    if MO.ResizeVertical then
      SL.Add('RESIZE BOTH')
    else
      SL.Add('RESIZE HORIZONTAL');
  end else if MO.ResizeVertical then
    SL.Add('RESIZE VERTICAL');

  if not (MO.TriggerType in [0, 30]) then
  begin
    SL.Add('TRIGGER_X ' + IntToStr(MO.PTriggerX));
    SL.Add('TRIGGER_Y ' + IntToStr(MO.PTriggerY));
    if not (MO.TriggerType in [13, 23, 25]) then
    begin
      SL.Add('TRIGGER_W ' + IntToStr(MO.PTriggerW));
      SL.Add('TRIGGER_H ' + IntToStr(MO.PTriggerH));
    end;
  end;

  if (MO.TriggerType in [4, 11, 24, 31]) then
  begin
    SL.Add('SOUND ' + IntToStr(MO.TriggerSound));
  end;

  if MO.PreviewFrame <> 0 then
    SL.Add('PREVIEW ' + IntToStr(MO.PreviewFrame));

  if MO.KeyFrame <> 0 then
    SL.Add('KEYFRAME ' + IntToStr(MO.KeyFrame));

  if MO.RandomFrame and (MO.TriggerType in [0, 1, 2, 3, 5, 6, 7, 8, 18, 19, 20, 22, 26, 27, 30]) then
    SL.Add('RANDOM_FRAME');

  BMP := TBitmap32.Create;
  try
    BMP.SetSize(OBmp[0].Width, OBmp[0].Height * OBmp.Count);
    for i := 0 to OBmp.Count-1 do
      OBmp[i].DrawTo(BMP, 0, i * OBmp[0].Height);
    SavePngFile(Name + '.png', BMP);
  finally
    BMP.Free;
  end;

  SL.SaveToFile(Name + '.nxob');
end;

end.