unit GSLoadCheapo;

// Loads an extracted (via Essman's tool) Cheapo graphic set into the editor's
// internal format. To use, create a TCheapoGraphicSet object, then call:
//
// <TBaseGraphicSet> := <TCheapoGraphicSet>.LoadGraphicSet(<path containing extracted style>);
//
// Giving the path of any file *within* the folder is also acceptable.
//
// The TBaseGraphicSet object is created during TCheapoGraphicSet.LoadGraphicSet, so you should
// not use <TBaseGraphicSet> := TBaseGraphicSet.Create; beforehand.
//
// Once TCheapoGraphicSet.LoadGraphicSet has been allowed to run, there is no need to keep the
// TCheapoGraphicSet object around, it can be freed.

// This unit does not have any saving capability.

interface

uses
  Dialogs, // NOT DEBUG! It's actually used in this one.
  Classes, SysUtils, LemGraphicSet, GR32, LibXmlParser, PngInterface, ZLibEx;

type

  //<Sprite Index='0' StartX='5' StartY='7' Width='48' Height='32' HandleX='0' HandleY='0' Metal='False' />
  CheapoSpriteRecord = record
    StartX: Integer;
    StartY: Integer;
    Width: Integer;
    Height: Integer;
    Metal: Boolean;
  end;

  //Rectangular trigger:
  //  <Object Index='0' Type='Window' SubType='None' SpriteStart='0' SpriteCount='6' HitRectX='0' HitRectY='0' HitRectWidth='0'
  //          HitRectHeight='0' HitPointX='36' HitPointY='24' Sound='0' />

  //Circular trigger:
  //  <Object Index='5' Type='Activate' SubType='Teleporter' SpriteStart='34' SpriteCount='6' HitCircleX='8' HitCircleY='21'
  //          HitCircleRadius='2' HitPointX='0' HitPointY='0' Sound='0' />
  CheapoObjectData = record
    MainType: String;
    SubType: String;
    SpriteStart: Integer;
    SpriteCount: Integer;
    TriggerX: Integer;
    TriggerY: Integer;
    TriggerWidth: Integer;
    TriggerHeight: Integer;
    TriggerCircle: Boolean;
    HotspotX: Integer;
    HotspotY: Integer;
    SoundEffect: Integer;
  end;

  CheapoLocationsData = packed record
    posHeader     : LongWord;
    posStyle      : LongWord;
    posAuthor     : LongWord;
    posStandards  : LongWord;
    posSketches   : LongWord;
    posErasers    : LongWord;
    posLemmings   : LongWord;
    posObjects    : LongWord;
    posGraphics   : LongWord;
    posFooter     : LongWord;
    posMusic      : LongWord;
    posSounds     : LongWord;
    posImages     : LongWord;
    posFiles      : LongWord;
  end;

  CheapoStyObjectData = packed record
    SubType       : Byte;
    StartFrame    : Word;
    EndFrame      : Word;
    TriggerX      : SmallInt;
    TriggerY      : SmallInt;
    TriggerX2     : SmallInt; // the trigger area is stored as Left Top Right Bottom,
    TriggerY2     : SmallInt; // not Left Top Width Height
    HitpointX     : SmallInt;
    HitpointY     : SmallInt;
    Sound         : Byte;
  end;

  TCheapoGraphicSetClass = class of TCheapoGraphicSet;
  TCheapoGraphicSet = class(TComponent)
    private
      function CheapoSoundToLemmixSound(aValue: Byte): Byte;
      procedure PatchTransparency(aBitmap: TBitmap32);
    public
      //constructor Create(aOwner: TComponent); override;
      //destructor Destroy; override;
      function LoadGraphicSet(aPath: String): TBaseGraphicSet;
      function LoadStyFile(aFile: string): TBaseGraphicSet;
  end;


implementation

{constructor TCheapoGraphicSet.Create(aOwner: TComponent);
begin
  inherited;
end;} // custom constructor not needed

{destructor TCheapoGraphicSet.Destroy;
begin
  inherited;
end;} // custom destructor not needed

function TCheapoGraphicSet.LoadStyFile(aFile: String): TBaseGraphicSet;
// This function is a fucking iceberg of mess at the moment (albeit a fucking iceberg of mess that works).
// Splitting it up into smaller, tidier subfunctions is on the todo list.
var
  StyFileStream: TMemoryStream;
  lw: LongWord; //technically cheapo uses signed, not unsigned, but negative values would cause errors anyway
  x, y, w, h, wrd, oc: Word;
  ef, sf: Word;
  b: Byte;
  Locations: CheapoLocationsData;
  i, i2, i3, si: Integer;
  yp, xp: ShortInt;
  s: String;

  StyObject: CheapoStyObjectData;

  TempBMP, TempBMP2: TBitmap32;
  TempBMPs, TempBMPs2: TBitmaps;

  TempTerrain: TMetaTerrain;
  TempObject: TMetaObject;

  TempGS: TBaseGraphicSet; // in case another, external graphic set needs to be loaded
  TempCheapoGS: TCheapoGraphicSet;

  SoundMap: Array[1..40] of Integer;
  GravWarning: Boolean;

  function CheapoDecompress: TMemoryStream;
  var
    csize, dsize: LongWord;
    //TempStream: TMemoryStream;
  begin
    Result := TMemoryStream.Create;
    Result.Seek(0, soFromBeginning);

    //TempStream := TMemoryStream.Create;
    //TempStream.Seek(0, soFromBeginning);

    StyFileStream.Read(csize, 4);
    StyFileStream.Read(dsize, 4);

    //TempStream.CopyFrom(StyFileStream, csize);
    //TempStream.Seek(0, soFromBeginning);

    //ZDecompressStream(TempStream, Result);
    ZDecompressStream(StyFileStream, Result);
    //Result.SaveToFile('decomp.xxx');
    Result.Seek(0, soFromBeginning);
  end;

  function GetCheapoString: String;
  var
    sl: Byte;
    si: Integer;
    c: Char;
  begin
    StyFileStream.Read(sl, 1);
    Result := '';
    for si := 1 to sl-1 do
    begin
      StyFileStream.Read(c, 1);
      Result := Result + c;
    end;
    StyFileStream.Read(sl, 1);
    Assert(sl = 0);
  end;

  function Color16ToColor32(Incol: Word): TColor32;
  var
    r, g, b: byte;
  begin
    //Incol := ((Incol and $FF) shl 8) + ((Incol and $FF00) shr 8);

    b := (Incol and $001F) shl 3;
    g := (Incol and $07E0) shr 3;
    r := (Incol and $F800) shr 8;

    Result := ((r shl 16) + (g shl 8) + b) + $FF000000;
    if Result = $FF000000 then Result := 0; // standard Cheapo behaviour; treat RGB(0,0,0) as transparent
    // $F800 blue
    // $07E0 green
    // $001F red
  end;

  function LoadCheapoImage: TBitmap32;
  var
    w, h, c: Word;
    BgColor: TColor32;
    TempStream: TMemoryStream;

    b: Byte;
    cnt: Byte;
    wcnt: Word;
    i: Integer;
    x, y: Word;
    TempCol: TColor32;
  begin
    StyFileStream.Read(b, 1);
    Assert(b = 1);
    StyFileStream.Read(w, 2);
    StyFileStream.Read(h, 2);
    StyFileStream.Read(c, 2);
    BgColor := Color16ToColor32(c);
    Result := TBitmap32.Create;
    Result.SetSize(w, h);
    Result.Clear(BgColor);
    TempStream := CheapoDecompress;

    repeat
      b := 0;
      TempStream.Read(b, 1);
      case b of
        1: begin
             TempStream.Read(y, 2);
             TempStream.Read(x, 2);
             TempStream.Read(cnt, 1);
             //ShowMessage('Type1: ' + IntToStr(x) + ',' + IntToStr(y));
             while cnt <> 0 do
             begin
               //ShowMessage('Count ' + IntToStr(cnt));
               TempStream.Read(c, 2);
               TempCol := Color16ToColor32(c);
               for i := 0 to cnt-1 do
               begin
                 while x >= Result.Width do
                 begin
                   x := x - Result.Width;
                   y := y + 1;
                 end;
                 Result.Pixel[x, y] := TempCol;
                 x := x + 1;
               end;
               cnt := 0;
               TempStream.Read(cnt, 1);
             end;
           end;
        2: begin
             TempStream.Read(y, 2);
             TempStream.Read(x, 2);
             TempStream.Read(wcnt, 2);
             wcnt := wcnt - x;
             //ShowMessage('Type2: ' + IntToStr(x) + ',' + IntToStr(y) + ' :: ' + IntToStr(wcnt));
             for i := 0 to wcnt do
             begin
               TempStream.Read(c, 2);
               TempCol := Color16ToColor32(c);
               while x >= Result.Width do
               begin
                 x := x - Result.Width;
                 y := y + 1;
               end;
               Result.Pixel[x, y] := TempCol;
               x := x + 1;
             end;
           end;
        else Break;
      end;
    until b = 0;

    TempStream.Free;
  end;

begin
  Result := TBaseGraphicSet.Create;
  StyFileStream := TMemoryStream.Create;
  StyFileStream.LoadFromFile(aFile);
  StyFileStream.Seek(0, soFromBeginning);

  GravWarning := false;

  for i := 1 to 40 do
    SoundMap[i] := -1;

  Result.Resolution := 8;
  Result.KeyColors[0] := $FF4040E0;
  Result.KeyColors[1] := $FF00B000;
  Result.KeyColors[2] := $FFF0D0D0;
  Result.KeyColors[3] := $FFB0B000;
  Result.KeyColors[4] := $FFF02020;
  Result.KeyColors[5] := $FF808080;
  Result.KeyColors[6] := $FF000000;
  Result.KeyColors[7] := $FF4040E0;

  StyFileStream.Read(lw, 4); //gets the location of the... list of locations xD
  StyFileStream.Seek(lw, soFromBeginning);
  StyFileStream.Read(Locations, SizeOf(Locations));

  // Header: We don't need to worry about this. It contains info that isn't used in NeoLemmix Graphic Sets.
  //         However, let's just quickly check it to verify we're dealing with a proper style here.

  StyFileStream.Seek(Locations.posHeader, soFromBeginning);
  if GetCheapoString <> 'Cheapo Copycat Level Editor' then
    ShowMessage('Invalid style file.');

  if Locations.posGraphics <> 0 then
  begin
    StyFileStream.Seek(Locations.posGraphics, soFromBeginning);
    StyFileStream.Read(b, 1);
    case b of
      1: begin
           TempBMP := LoadCheapoImage;

           StyFileStream.Read(b, 1); //should always be 1 but if we got this far we probably have a valid style file
           StyFileStream.Read(wrd, 2);

           for i := 0 to wrd-1 do
           begin
             StyFileStream.Read(x, 2);
             StyFileStream.Read(x, 2); // Not a mistake. The first word is data we don't need.
             StyFileStream.Read(y, 2);
             StyFileStream.Read(w, 2);
             StyFileStream.Read(h, 2);
             TempBMP2 := TBitmap32.Create;
             TempBMP2.SetSize(w - x, h - y);
             TempBMP.DrawTo(TempBMP2, 0, 0, Rect(x, y, w, h));
             Result.TerrainImages.Add(TempBMP2);
             TempTerrain := TMetaTerrain.Create;
             Result.MetaTerrains.Add(TempTerrain);
           end;

           StyFileStream.Read(b, 1);
           StyFileStream.Read(wrd, 2);
           StyFileStream.Read(x, 2); //unneeded data for terrain pieces, it's only used in objects
           for i := 0 to wrd-1 do
           begin
             StyFileStream.Read(b, 1);
             case b of     // Although we don't need to pay attention to most types, we need to move ahead right number of bytes
               1: StyFileStream.Position := StyFileStream.Position + 8;  // "Handle" (eg: lemming graphics)
               2: StyFileStream.Position := StyFileStream.Position + 8;  // Window
               3: StyFileStream.Position := StyFileStream.Position + 18; // Activation object
               4: StyFileStream.Position := StyFileStream.Position + 18; // Constant object
              20: begin                                                  // THIS is the one we need (Metal)
                    StyFileStream.Read(x, 2); // not actually coordinates. Just bad coding via variable re-use
                    StyFileStream.Read(y, 2);
                    for i2 := x to y do
                      Result.MetaTerrains[i2].Steel := true;
                  end;
             end;
           end;

           TempBMP.Free;
         end;
      2: begin
           s := GetCheapoString;
           if FileExists(ExtractFilePath(aFile) + s) then
           begin
             TempCheapoGS := TCheapoGraphicSet.Create(self);
             TempGS := TempCheapoGS.LoadStyFile(ExtractFilePath(aFile) + s);
             TempCheapoGS.Free;
             for i := 0 to TempGS.MetaTerrains.Count-1 do
             begin
               TempBMP := TBitmap32.Create;
               TempTerrain := TMetaTerrain.Create;
               TempBMP.Assign(TempGS.TerrainImages[i]);
               TempTerrain.Assign(TempGS.MetaTerrains[i]);
               Result.TerrainImages.Add(TempBMP);
               Result.MetaTerrains.Add(TempTerrain);
             end;
             TempGS.Free;
           end else
             ShowMessage('This style refers to ' + s + ' for terrain pieces; ' + s + ' was not found.');
         end;
    end;
  end;

  // We should handle sounds next, before tackling objects.
  if Locations.posSounds <> 0 then
  begin
    StyFileStream.Seek(Locations.posSounds, soFromBeginning);
    StyFileStream.Read(b, 1); // this byte should always be 1, but we've long since stopped checking these
    StyFileStream.Read(b, 1);
    wrd := b; // not sure if reading a 1-byte value into a 2-byte variable from a stream will give glitchy results
              // and we need to re-use b
    si := 23;
    for i := 0 to wrd-1 do
    begin
      StyFileStream.Read(b, 1);
      SoundMap[b] := si;
      StyFileStream.Read(b, 1); //another should-always-be-1 byte
      if Result.Sounds[si] <> nil then Result.Sounds[si].Free; //should literally never happen, but just in case
      Result.Sounds[si] := CheapoDecompress;
      si := si + 1;
    end;
  end;

  // Okay, time to handle objects now
  if Locations.posObjects <> 0 then
  begin
    StyFileStream.Seek(Locations.posObjects, soFromBeginning);
    StyFileStream.Read(b, 1);
    case b of
      1: begin
           // Yay. The most complicated part of this.
           // Let's start by loading the images. This can be done roughly the same way as terrain. Yay for almost-copy-and-paste code.
           TempBMP := LoadCheapoImage;
           TempBMPs := TBitmaps.Create;

           StyFileStream.Read(b, 1); //should always be 1 but if we got this far we probably have a valid style file
           StyFileStream.Read(wrd, 2);

           for i := 0 to wrd-1 do
           begin
             StyFileStream.Read(x, 2);
             StyFileStream.Read(x, 2); // Not a mistake. The first word is data we don't need.
             StyFileStream.Read(y, 2);
             StyFileStream.Read(w, 2);
             StyFileStream.Read(h, 2);
             TempBMP2 := TBitmap32.Create;
             TempBMP2.SetSize(w - x, h - y);
             TempBMP.DrawTo(TempBMP2, 0, 0, Rect(x, y, w, h));
             TempBMPs.Add(TempBMP2);
           end;

           TempBMP.Free; // we don't need it anymore, free it now rather than later

           // And that's the easy part done.
           StyFileStream.Read(b, 1); // another throwaway "always 1" byte

           StyFileStream.Read(wrd, 2); // number of entries
           StyFileStream.Read(oc, 2); // number of objects. I have no idea why these would ever be different, but that's the format.

           {for i := 0 to oc-1 do
           begin
             TempObject := TMetaObject.Create;
             Result.MetaObjects.Add(TempObject);
           end;}

           //i2 := 0;
           for i := 0 to wrd-1 do
           begin
             //if i2 >= oc then Break;
             StyFileStream.Read(b, 1);
             case b of                                                   // This time, we need to pay attention to every valid value except 1 and 20
               1: StyFileStream.Position := StyFileStream.Position + 8;  // "Handle" (eg: lemming graphics)
               2: begin                                                  // Window
                    TempBMPs2 := TBitmaps.Create;
                    
                    StyFileStream.Read(sf, 2);
                    StyFileStream.Read(ef, 2);

                    StyFileStream.Read(xp, 2);
                    StyFileStream.Read(yp, 2);

                    TempBMP := TBitmap32.Create;
                    TempBMP.Assign(TempBMPs[ef]);
                    TempBMPs2.Add(TempBMP);
                    for i3 := sf to (ef-1) do
                    begin
                      TempBMP := TBitmap32.Create;
                      TempBMP.Assign(TempBMPs[i3]);
                      TempBMPs2.Add(TempBMP);
                    end;
                    Result.ObjectImages.Add(TempBMPs2);

                    TempObject := TMetaObject.Create;
                    with TempObject do
                    begin
                      TriggerType := 23;
                      TriggerAnim := true;
                      PTriggerX := xp;
                      PTriggerY := yp - 1;
                      PTriggerW := 1;
                      PTriggerH := 1;
                    end;

                    Result.MetaObjects.Add(TempObject);

                    //i2 := i2 + 1;
                  end;
             3,4: begin                                                  // Constant or Activate. These can be handled similarly.
                    // This code was filler when I had only coded the windows
                    {TempBMP := TBitmap32.Create;
                    TempBMP.Assign(TempBMPs[0]);
                    TempBMPs2 := TBitmaps.Create;
                    TempBMPs2.Add(TempBMP);
                    Result.ObjectImages.Add(TempBMPs2);

                    TempObject := TMetaObject.Create;
                    Result.MetaObjects.Add(TempObject);}

                    StyFileStream.Read(StyObject, SizeOf(StyObject));

                    TempObject := TMetaObject.Create;

                    case b of
                      3: begin // Activate
                           TempObject.TriggerAnim := true;
                           if SoundMap[StyObject.Sound] <> -1 then
                             TempObject.TriggerSound := SoundMap[StyObject.Sound]
                           else
                             TempObject.TriggerSound := CheapoSoundToLemmixSound(StyObject.Sound);

                           TempObject.STriggerX := StyObject.HitpointX;
                           TempObject.STriggerY := StyObject.HitpointY + 1;
                           TempObject.STriggerW := 1;
                           TempObject.STriggerH := 1;

                           if StyObject.TriggerY2 = -9999 then
                           begin
                             TempObject.PTriggerX := StyObject.TriggerX - (StyObject.TriggerX2 - 1);
                             TempObject.PTriggerY := StyObject.TriggerY - (StyObject.TriggerX2 - 1);
                             TempObject.PTriggerW := (StyObject.TriggerX2 * 2) - 1;
                             TempObject.PTriggerH := (StyObject.TriggerX2 * 2) - 1;
                           end else begin
                             TempObject.PTriggerX := StyObject.TriggerX;
                             TempObject.PTriggerY := StyObject.TriggerY + 1;
                             TempObject.PTriggerW := (StyObject.TriggerX2 - StyObject.TriggerX) + 1;
                             TempObject.PTriggerH := (StyObject.TriggerY2 - StyObject.TriggerY) + 1;
                           end;

                           case StyObject.SubType of
                             1: TempObject.TriggerType := 1;
                             4: TempObject.TriggerType := 24;
                             5: TempObject.TriggerType := 4;
                             6: TempObject.TriggerType := 11;
                             7: begin
                                  TempObject.TriggerType := 12;
                                  TempObject.PTriggerX := TempObject.STriggerX;
                                  TempObject.PTriggerY := TempObject.STriggerY;
                                  TempObject.PTriggerW := 1;
                                  TempObject.PTriggerH := 1;
                                  // Cheapo defines receiver's point on the secondary trigger (or hit point, in Cheapo terms)
                                  // but NeoLemmix defines it on the primary trigger; the secondary is only used for objects that
                                  // need two trigger areas/points
                                end;
                             8: TempObject.TriggerType := 28;
                          9,10: begin
                                  TempObject.TriggerType := 24;
                                  if GravWarning = false then
                                  begin
                                    GravWarning := true;
                                    ShowMessage('Gravity changers are not supported in NeoLemmix. They will be loaded as no-effect objects.');
                                  end;
                                end;
                            12: TempObject.TriggerType := 29;
                             else ShowMessage('Unrecognized object subtype: ACTIVATE:' + IntToStr(StyObject.SubType));
                           end;
                         end;
                      4: begin // Constant

                           if StyObject.TriggerY2 = -9999 then
                           begin
                             TempObject.PTriggerX := StyObject.TriggerX - (StyObject.TriggerX2 - 1);
                             TempObject.PTriggerY := StyObject.TriggerY - (StyObject.TriggerX2 - 1);
                             TempObject.PTriggerW := (StyObject.TriggerX2 * 2) - 1;
                             TempObject.PTriggerH := (StyObject.TriggerX2 * 2) - 1;
                           end else begin
                             TempObject.PTriggerX := StyObject.TriggerX;
                             TempObject.PTriggerY := StyObject.TriggerY + 1;
                             TempObject.PTriggerW := (StyObject.TriggerX2 - StyObject.TriggerX) + 1;
                             TempObject.PTriggerH := (StyObject.TriggerY2 - StyObject.TriggerY) + 1;
                           end;

                           // Secondary trigger area isn't relevant for any Cheapo-supported constant animation objects
                           
                           case StyObject.SubType of
                             1: TempObject.TriggerType := 1;
                             2: TempObject.TriggerType := 5;
                             3: TempObject.TriggerType := 6;
                             4: TempObject.TriggerType := 0;
                          9,10: begin
                                  TempObject.TriggerType := 0;
                                  if GravWarning = false then
                                  begin
                                    GravWarning := true;
                                    ShowMessage('Gravity changers are not supported in NeoLemmix. They will be loaded as no-effect objects.');
                                  end;
                                end;
                            11: TempObject.TriggerType := 25;
                            13: TempObject.TriggerType := 27;
                            14: TempObject.TriggerType := 26;
                             else ShowMessage('Unrecognized object subtype: CONSTANT:' + IntToStr(StyObject.SubType));
                           end;
                         end;
                    end;

                    TempBMPs2 := TBitmaps.Create;
                    for i3 := StyObject.StartFrame to StyObject.EndFrame do
                    begin
                      TempBMP := TBitmap32.Create;
                      TempBMP.Assign(TempBMPs[i3]);
                      TempBMPs2.Add(TempBMP);
                    end;

                    Result.MetaObjects.Add(TempObject);
                    Result.ObjectImages.Add(TempBMPs2);

                    //i2 := i2 + 1;
                  end;
              20: StyFileStream.Position := StyFileStream.Position + 4;
             end;
           end;

           TempBMPs.Free;
         end;
      2: begin
           s := GetCheapoString;
           if FileExists(ExtractFilePath(aFile) + s) then
           begin
             TempCheapoGS := TCheapoGraphicSet.Create(self);
             TempGS := TempCheapoGS.LoadStyFile(ExtractFilePath(aFile) + s);
             TempCheapoGS.Free;

             for i := 23 to 62 do // use referred style's sounds
               if TempGS.Sounds[i] <> nil then
               begin
                 if Result.Sounds[i] = nil then Result.Sounds[i] := TMemoryStream.Create;
                 Result.Sounds[i].LoadFromStream(TempGS.Sounds[i]);
               end else if Result.Sounds[i] <> nil then
               begin
                 Result.Sounds[i].Free;
                 Result.Sounds[i] := nil;
               end;

             for i := 0 to TempGS.MetaObjects.Count-1 do
             begin
               TempBMPs := TBitmaps.Create;
               TempObject := TMetaObject.Create;

               for i2 := 0 to TempGS.ObjectImages[i].Count-1 do
               begin
                 TempBMP := TBitmap32.Create;
                 TempBMP.Assign(TempGS.ObjectImages[i][i2]);
                 TempBMPs.Add(TempBMP);
               end;

               TempObject.Assign(TempGS.MetaObjects[i]);

               Result.ObjectImages.Add(TempBMPs);
               Result.MetaObjects.Add(TempObject);
             end;
             TempGS.Free;
           end else
             ShowMessage('This style refers to ' + s + ' for objects; ' + s + ' was not found.');
         end;
    end;
  end;


  // Now generate one-way arrows
  if (Result.MetaObjects.Count = 0)
  or (Result.MetaObjects[Result.MetaObjects.Count-1].TriggerType <> 19) then
  begin
    StyFileStream.Seek(Locations.posStandards, soFromBeginning);
    StyFileStream.Read(b, 1);

    case b of
      1: begin
           TempBMP := LoadCheapoImage;
           TempBMPs := TBitmaps.Create;

           StyFileStream.Read(b, 1); //should always be 1 but if we got this far we probably have a valid style file
           StyFileStream.Read(wrd, 2);

           for i := 0 to wrd-1 do
           begin
             StyFileStream.Read(x, 2);
             StyFileStream.Read(x, 2); // Not a mistake. The first word is data we don't need.
             StyFileStream.Read(y, 2);
             StyFileStream.Read(w, 2);
             StyFileStream.Read(h, 2);
             TempBMP2 := TBitmap32.Create;
             TempBMP2.SetSize(w - x, h - y);
             TempBMP.DrawTo(TempBMP2, 0, 0, Rect(x, y, w, h));
             TempBMPs.Add(TempBMP2);
           end;

           TempBMP.Free;

           // One-way left
           TempBMP := TBitmap32.Create;
           TempBMPs2 := TBitmaps.Create;
           TempBMP.Assign(TempBMPs[58]);
           TempBMPs2.Add(TempBMP);
           Result.ObjectImages.Add(TempBMPs2);

           TempObject := TMetaObject.Create;
           TempObject.TriggerType := 7;
           TempObject.PTriggerX := 0;
           TempObject.PTriggerY := 0;
           TempObject.PTriggerW := TempBMP.Width;
           TempObject.PTriggerH := TempBMP.Height;
           Result.MetaObjects.Add(TempObject);

           // One-way right
           TempBMP := TBitmap32.Create;
           TempBMPs2 := TBitmaps.Create;
           TempBMP.Assign(TempBMPs[59]);
           TempBMPs2.Add(TempBMP);
           Result.ObjectImages.Add(TempBMPs2);

           TempObject := TMetaObject.Create;
           TempObject.TriggerType := 8;
           TempObject.PTriggerX := 0;
           TempObject.PTriggerY := 0;
           TempObject.PTriggerW := TempBMP.Width;
           TempObject.PTriggerH := TempBMP.Height;
           Result.MetaObjects.Add(TempObject);

           // One-way down
           TempBMP := TBitmap32.Create;
           TempBMPs2 := TBitmaps.Create;
           TempBMP.Assign(TempBMPs[59]);
           TempBMP.Rotate90;
           TempBMPs2.Add(TempBMP);
           Result.ObjectImages.Add(TempBMPs2);

           TempObject := TMetaObject.Create;
           TempObject.TriggerType := 19;
           TempObject.PTriggerX := 0;
           TempObject.PTriggerY := 0;
           TempObject.PTriggerW := TempBMP.Width;
           TempObject.PTriggerH := TempBMP.Height;
           Result.MetaObjects.Add(TempObject);


         end;
      2: begin
           s := GetCheapoString;
           if FileExists(ExtractFilePath(aFile) + s) then
           begin
             TempCheapoGS := TCheapoGraphicSet.Create(self);
             TempGS := TempCheapoGS.LoadStyFile(ExtractFilePath(aFile) + s);
             TempCheapoGS.Free;

             if (TempGS.MetaObjects.Count > 0)
             and (TempGS.MetaObjects[TempGS.MetaObjects.Count-1].TriggerType = 19) then
               for i := TempGS.MetaObjects.Count-3 to TempGS.MetaObjects.Count-1 do
               begin
                 TempBMPs := TBitmaps.Create;
                 TempBMP := TBitmap32.Create;
                 TempBMP.Assign(TempGS.ObjectImages[i][0]);
                 TempBMPs.Add(TempBMP);
                 TempObject := TMetaObject.Create;
                 TempObject.Assign(TempGS.MetaObjects[i]);
                 Result.ObjectImages.Add(TempBMPs);
                 Result.MetaObjects.Add(TempObject);
               end;

             TempGS.Free;
           end else
             ShowMessage('This style refers to ' + s + ' for standards; ' + s + ' was not found.');
         end;
    end;

  end;

  StyFileStream.Free;
end;

function TCheapoGraphicSet.LoadGraphicSet(aPath: String): TBaseGraphicSet;
var
  XML: TXmlParser;
  i, i2: Integer;
  SecBmp, TempBmp: TBitmap32;
  TempBmpList, TempObjBmps: TBitmaps;
  TempSpriteRec: CheapoSpriteRecord;
  TempObjRec: CheapoObjectData;

  TempTer: TMetaTerrain;
  TempObj: TMetaObject;

  GravWarning: Boolean;

  SoundMap: Array[1..40] of Integer;
begin
  aPath := ExtractFilePath(aPath);

  XML := TXmlParser.Create;
  Result := TBaseGraphicSet.Create;

  Result.Resolution := 8;
  Result.KeyColors[0] := $FF4040E0;
  Result.KeyColors[1] := $FF00B000;
  Result.KeyColors[2] := $FFF0D0D0;
  Result.KeyColors[3] := $FFB0B000;
  Result.KeyColors[4] := $FFF02020;
  Result.KeyColors[5] := $FF808080;
  Result.KeyColors[6] := $FF000000;
  Result.KeyColors[7] := $FF4040E0;

  SecBmp := LoadPngFile(aPath + 'Graphics.png');

  GravWarning := false;

  XML.LoadFromFile(aPath + 'graphics.sprites.xml');
  XML.StartScan;
  while XML.Scan do
    if (XML.CurPartType = ptEmptyTag) then
    begin
      with TempSpriteRec do
      begin
        StartX := StrToInt(XML.CurAttr.Value('StartX'));
        StartY := StrToInt(XML.CurAttr.Value('StartY'));
        Width := StrToInt(XML.CurAttr.Value('Width'));
        Height := StrToInt(XML.CurAttr.Value('Height'));
        Metal := (XML.CurAttr.Value('Metal') = 'True');

        TempBmp := TBitmap32.Create;
        TempBmp.SetSize(Width, Height);
        SecBmp.DrawTo(TempBmp, 0, 0, Rect(StartX, StartY, StartX+Width, StartY+Height));
        PatchTransparency(TempBmp);

        TempTer := TMetaTerrain.Create;
        TempTer.Steel := Metal;
        Result.MetaTerrains.Add(TempTer);
        Result.TerrainImages.Add(TempBmp);
      end;

    end;

  SecBmp.Free;
  SecBmp := LoadPngFile(aPath + 'Objects.png');
  XML.LoadFromFile(aPath + 'objects.sprites.xml');
  TempBmpList := TBitmaps.Create;

  XML.StartScan;
  while XML.Scan do
    if (XML.CurPartType = ptEmptyTag) then
    begin
      with TempSpriteRec do
      begin
        StartX := StrToInt(XML.CurAttr.Value('StartX'));
        StartY := StrToInt(XML.CurAttr.Value('StartY'));
        Width := StrToInt(XML.CurAttr.Value('Width'));
        Height := StrToInt(XML.CurAttr.Value('Height'));

        TempBmp := TBitmap32.Create;
        TempBmp.SetSize(Width, Height);
        SecBmp.DrawTo(TempBmp, 0, 0, Rect(StartX, StartY, StartX+Width, StartY+Height));

        PatchTransparency(TempBmp);

        TempBmpList.Add(TempBmp);
      end;

    end;

  SecBmp.Free;

  // Load sounds before objects. This way, objects with custom sounds can be mapped to them correctly.
  i2 := 23;
  for i := 1 to 40 do
  begin
    SoundMap[i] := -1;
    if FileExists(aPath + 'Sound' + IntToStr(i) + '.wav') then
    begin
      SoundMap[i] := i2;
      Result.Sounds[i2] := TMemoryStream.Create;
      Result.Sounds[i2].LoadFromFile(aPath + 'Sound' + IntToStr(i) + '.wav');
    end;
  end;

  XML.LoadFromFile(aPath + 'objects.objects.xml');
  XML.StartScan;
  while XML.Scan do
    if (XML.CurPartType = ptEmptyTag) then
    begin
      with TempObjRec do
      begin
        MainType := XML.CurAttr.Value('Type');
        SubType := XML.CurAttr.Value('SubType');
        SpriteStart := StrToInt(XML.CurAttr.Value('SpriteStart'));
        SpriteCount := StrToInt(XML.CurAttr.Value('SpriteCount'));
        if StrToIntDef(XML.CurAttr.Value('HitRectWidth'), -1) <> -1 then // Hopefully no Cheapo styles actually have a negative hitrect? xD
        begin
          TriggerX := StrToInt(XML.CurAttr.Value('HitRectX'));
          TriggerY := StrToInt(XML.CurAttr.Value('HitRectY'));
          TriggerWidth := StrToInt(XML.CurAttr.Value('HitRectWidth'));
          TriggerHeight := StrToInt(XML.CurAttr.Value('HitRectHeight'));
          TriggerCircle := false;
        end else begin
          TriggerWidth := StrToInt(XML.CurAttr.Value('HitCircleRadius'));
          TriggerX := StrToInt(XML.CurAttr.Value('HitCircleX')) - TriggerWidth + 1;
          TriggerY := StrToInt(XML.CurAttr.Value('HitCircleY')) - TriggerWidth + 1;
          TriggerWidth := (TriggerWidth * 2) - 1;
          TriggerHeight := TriggerWidth;
          TriggerCircle := true;
        end;
        HotspotX := StrToInt(XML.CurAttr.Value('HitPointX'));
        HotspotY := StrToInt(XML.CurAttr.Value('HitPointY'));
        SoundEffect := StrToInt(XML.CurAttr.Value('Sound'));

        TempObjBmps := TBitmaps.Create;

        if MainType = 'Window' then // Special handling, as fully-open comes last in Cheapo, but first in NeoLemmix
        begin
          TempBmp := TBitmap32.Create;
          TempBmp.Assign(TempBmpList[SpriteStart+SpriteCount-1]);
          TempObjBmps.Add(TempBmp);
          SpriteCount := SpriteCount - 1;
        end;

        for i := SpriteStart to (SpriteStart+SpriteCount-1) do
        begin
          TempBmp := TBitmap32.Create;
          TempBmp.Assign(TempBmpList[i]);
          TempObjBmps.Add(TempBmp);
        end;
        Result.ObjectImages.Add(TempObjBmps);

        TempObj := TMetaObject.Create;

        // Probably the only two values we can tell for sure. Freaking Cheapo.
        TempObj.PreviewFrame := 0;
        TempObj.KeyFrame := 0;

        if MainType = 'Window' then
        begin
          TempObj.TriggerType := 23;
          TempObj.TriggerAnim := true;
          TempObj.PTriggerX := HotspotX;
          TempObj.PTriggerY := HotspotY - 1;
          TempObj.PreviewFrame := 1; // even one exception to this one... xD
        end else begin
          // These are the same for both Activate and Constant, so easier to put them
          // in an ELSE clause here than duplicate them later.
          TempObj.PTriggerX := TriggerX;
          TempObj.PTriggerY := TriggerY + 1; // Because Cheapo checks one pixel higher than other engines
          TempObj.PTriggerW := TriggerWidth;
          TempObj.PTriggerH := TriggerHeight;
          TempObj.STriggerX := HotspotX;
          TempObj.STriggerY := HotspotY + 1; // Same
          TempObj.STriggerW := 1; // Cheapo never has a size for secondary trigger area,
          TempObj.STriggerY := 1; // it only has a single pixel point
        end;

        if MainType = 'Activate' then
        begin
          // These values hold constant for all Activate objects.
          TempObj.TriggerAnim := true;
          if SoundMap[SoundEffect] = -1 then
            TempObj.TriggerSound := CheapoSoundToLemmixSound(SoundEffect)
          else
            TempObj.TriggerSound := SoundMap[SoundEffect];

          if SubType = 'TwoWayTeleporter' then TempObj.TriggerType := 28;
          if SubType = 'SingleTeleporter' then TempObj.TriggerType := 29;
          if SubType = 'Exit' then TempObj.TriggerType := 1;
          if SubType = 'Death' then TempObj.TriggerType := 4;
          if SubType = 'Teleporter' then TempObj.TriggerType := 11;
          if SubType = 'Harmless' then TempObj.TriggerType := 24;
          if SubType = 'Receiver' then
          begin
            TempObj.TriggerType := 12;
            TempObj.PTriggerX := HotSpotX;
            TempObj.PTriggerY := HotSpotY;
            TempObj.PTriggerW := 1;
            TempObj.PTriggerH := 1;
            // Cheapo defines standalone receivers using the secondary trigger area (/ point), but
            // NeoLemmix only uses the secondary on objects that need *two* trigger areas, so for
            // these, the primary trigger area is used
          end;
        end;

        if MainType = 'Constant' then
        begin
          // These values hold constant for all Constant objects.
          TempObj.TriggerAnim := false;
          TempObj.TriggerSound := 0;

          if SubType = 'Exit' then TempObj.TriggerType := 1;
          if SubType = 'Water' then TempObj.TriggerType := 5;
          if SubType = 'Fire' then TempObj.TriggerType := 6;
          if SubType = 'Splat' then TempObj.TriggerType := 27;
          if SubType = 'NoSplat' then TempObj.TriggerType := 26;
          if SubType = 'Hint' then TempObj.TriggerType := 25;
        end;

        if (TempObj.TriggerType = 0) and (SubType <> 'Harmless') then
        begin
          if ((SubType = 'GravityUp') or (SubType = 'GravityDown')) then
          begin
            if not GravWarning then
              MessageDlg('This graphic set contains gravity changer objects. These are not supported' + #13 +
                         'by NeoLemmix. They will be converted as no-effect objects.', mtcustom, [mbok], 0);
            GravWarning := true;
            if TempObj.TriggerAnim then TempObj.TriggerType := 24;
          end else
            MessageDlg('Warning: Unknown object type: "' + MainType + '" : "' + SubType + '"', mtCustom, [mbOk], 0);
        end;

        Result.MetaObjects.Add(TempObj);
      end;
    end;

  TempBmpList.Free;
  XML.Free;
end;

procedure TCheapoGraphicSet.PatchTransparency(aBitmap: TBitmap32);
var
  x, y: Integer;
begin
  for y := 0 to aBitmap.Height-1 do
    for x := 0 to aBitmap.Width-1 do
      if (aBitmap.Pixel[x, y] and $FFFFFF) = 0 then aBitmap.Pixel[x, y] := 0;
end;

function TCheapoGraphicSet.CheapoSoundToLemmixSound(aValue: Byte): Byte;
begin
  case aValue of
    14: Result := 15;
    15: Result := 9;
    16: Result := 7;
    17: Result := 14;
    else Result := 6;
  end;
end;

end.