object F_GSTool: TF_GSTool
  Left = 454
  Top = 230
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Anchors = []
  BorderStyle = bsSingle
  Caption = 'NeoLemmix Graphic Set Tool'
  ClientHeight = 523
  ClientWidth = 777
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDefault
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 68
    Height = 13
    Caption = 'Terrain Pieces'
  end
  object Label2: TLabel
    Left = 8
    Top = 264
    Width = 89
    Height = 13
    Caption = 'Interactive Objects'
  end
  object Label3: TLabel
    Left = 264
    Top = 80
    Width = 91
    Height = 13
    Caption = 'Terrain Settings'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 264
    Top = 136
    Width = 88
    Height = 13
    Caption = 'Object Settings'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 280
    Top = 208
    Width = 91
    Height = 13
    Caption = 'Resizability Options'
    Visible = False
  end
  object Label6: TLabel
    Left = 280
    Top = 160
    Width = 58
    Height = 13
    Caption = 'Object Type'
  end
  object txtFrame: TLabel
    Left = 616
    Top = 234
    Width = 41
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = '0/0'
  end
  object Label7: TLabel
    Left = 280
    Top = 264
    Width = 58
    Height = 13
    Caption = 'Trigger Area'
  end
  object Label9: TLabel
    Left = 304
    Top = 284
    Width = 40
    Height = 13
    Caption = 'Position:'
  end
  object Label10: TLabel
    Left = 399
    Top = 286
    Width = 3
    Height = 13
    Caption = ','
  end
  object Label11: TLabel
    Left = 304
    Top = 308
    Width = 23
    Height = 13
    Caption = 'Size:'
  end
  object Label12: TLabel
    Left = 398
    Top = 310
    Width = 5
    Height = 13
    Caption = 'x'
  end
  object Label18: TLabel
    Left = 280
    Top = 384
    Width = 81
    Height = 13
    Caption = 'Important Frames'
  end
  object Label19: TLabel
    Left = 304
    Top = 404
    Width = 41
    Height = 13
    Caption = 'Preview:'
  end
  object Label20: TLabel
    Left = 304
    Top = 428
    Width = 47
    Height = 13
    Caption = 'Keyframe:'
  end
  object Label21: TLabel
    Left = 280
    Top = 472
    Width = 62
    Height = 13
    Caption = 'Sound Effect'
  end
  object Label22: TLabel
    Left = 552
    Top = 376
    Width = 92
    Height = 13
    Caption = 'General Options'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label24: TLabel
    Left = 568
    Top = 424
    Width = 29
    Height = 13
    Caption = 'Colors'
  end
  object Label25: TLabel
    Left = 571
    Top = 468
    Width = 11
    Height = 13
    Caption = 'R:'
  end
  object Label26: TLabel
    Left = 627
    Top = 468
    Width = 11
    Height = 13
    Caption = 'G:'
  end
  object Label27: TLabel
    Left = 683
    Top = 468
    Width = 10
    Height = 13
    Caption = 'B:'
  end
  object Label28: TLabel
    Left = 208
    Top = 16
    Width = 95
    Height = 13
    Caption = 'General Settings'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label29: TLabel
    Left = 208
    Top = 35
    Width = 31
    Height = 13
    Caption = 'Name:'
  end
  object Label8: TLabel
    Left = 560
    Top = 400
    Width = 35
    Height = 13
    Caption = 'Sprites:'
  end
  object lbTerrains: TListBox
    Left = 8
    Top = 32
    Width = 169
    Height = 201
    ItemHeight = 13
    TabOrder = 0
    OnClick = lbTerrainsClick
  end
  object lbObjects: TListBox
    Left = 8
    Top = 288
    Width = 169
    Height = 201
    ItemHeight = 13
    TabOrder = 1
    OnClick = lbObjectsClick
  end
  object btnAddTerrain: TButton
    Left = 8
    Top = 240
    Width = 49
    Height = 17
    Caption = 'Add'
    TabOrder = 2
    OnClick = btnAddTerrainClick
  end
  object btnDelTerrain: TButton
    Left = 128
    Top = 240
    Width = 49
    Height = 17
    Caption = 'Delete'
    TabOrder = 3
    OnClick = btnDelTerrainClick
  end
  object btnAddObject: TButton
    Left = 8
    Top = 496
    Width = 49
    Height = 17
    Caption = 'Add'
    TabOrder = 4
    OnClick = btnAddObjectClick
  end
  object btnDelObject: TButton
    Left = 128
    Top = 496
    Width = 49
    Height = 17
    Caption = 'Delete'
    TabOrder = 5
    OnClick = btnDelObjectClick
  end
  object cbSteel: TCheckBox
    Left = 280
    Top = 104
    Width = 209
    Height = 17
    Caption = 'Steel'
    TabOrder = 6
    OnClick = cbSteelClick
  end
  object cmbObjectType: TComboBox
    Left = 280
    Top = 176
    Width = 217
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
    OnChange = cmbObjectTypeChange
    Items.Strings = (
      'No Trigger'
      'Exit'
      'One-Way Field Left'
      'One-Way Field Right'
      'Trap'
      'Water'
      'Fire'
      'One-Way Arrows Left'
      'One-Way Arrows Right'
      'Steel Area'
      'Anti-Blocker Field'
      'Teleporter'
      'Receiver'
      'Pre-Placed Lemming'
      'Pickup Skill'
      'Locked Exit'
      'Sketch'
      'Unlock Button'
      'Radiation'
      'One-Way Arrows Down'
      'Updraft'
      'Splitter'
      'Slowfreeze'
      'Entrance'
      'Triggered Animation'
      'Hint'
      'Anti-Splat Pad'
      'Splat Pad'
      '(removed)'
      '(removed)'
      'Moving Background'
      'Single-Use Trap'
      'Background Image')
  end
  object btnPrevFrame: TButton
    Left = 552
    Top = 232
    Width = 25
    Height = 17
    Caption = '<'
    TabOrder = 8
    OnClick = btnPrevFrameClick
  end
  object btnNextFrame: TButton
    Left = 744
    Top = 232
    Width = 25
    Height = 17
    Caption = '>'
    TabOrder = 9
    OnClick = btnNextFrameClick
  end
  object btnDelFrame: TButton
    Left = 584
    Top = 232
    Width = 25
    Height = 17
    Caption = '-'
    TabOrder = 10
    OnClick = btnDelFrameClick
  end
  object btnAddFrame: TButton
    Left = 712
    Top = 232
    Width = 25
    Height = 17
    Caption = '+'
    TabOrder = 11
    OnClick = btnAddFrameClick
  end
  object btnLoadFrame: TButton
    Left = 552
    Top = 256
    Width = 105
    Height = 17
    Caption = 'Import'
    TabOrder = 12
    OnClick = btnLoadFrameClick
  end
  object btnSaveFrame: TButton
    Left = 664
    Top = 256
    Width = 105
    Height = 17
    Caption = 'Export'
    TabOrder = 13
    OnClick = btnSaveFrameClick
  end
  object cbDisplayTrigger: TCheckBox
    Left = 552
    Top = 304
    Width = 121
    Height = 17
    Caption = 'Show Trigger Area'
    TabOrder = 14
    OnClick = cbDisplayTriggerClick
  end
  object ebPtrigX: TEdit
    Left = 352
    Top = 280
    Width = 41
    Height = 21
    TabOrder = 15
    Text = '0'
    OnChange = ebPtrigXChange
  end
  object ebPtrigY: TEdit
    Left = 408
    Top = 280
    Width = 41
    Height = 21
    TabOrder = 16
    Text = '0'
    OnChange = ebPtrigYChange
  end
  object ebPtrigW: TEdit
    Left = 352
    Top = 304
    Width = 41
    Height = 21
    TabOrder = 17
    Text = '0'
    OnChange = ebPtrigWChange
  end
  object ebPtrigH: TEdit
    Left = 408
    Top = 304
    Width = 41
    Height = 21
    TabOrder = 18
    Text = '0'
    OnChange = ebPtrigHChange
  end
  object ebPreviewFrame: TEdit
    Left = 360
    Top = 400
    Width = 41
    Height = 21
    TabOrder = 19
    Text = '0'
    OnChange = ebPreviewFrameChange
  end
  object ebKeyFrame: TEdit
    Left = 360
    Top = 424
    Width = 41
    Height = 21
    TabOrder = 20
    Text = '0'
    OnChange = ebKeyFrameChange
  end
  object btnPrevFrameCurrent: TButton
    Left = 408
    Top = 400
    Width = 89
    Height = 17
    Caption = 'Use Current'
    TabOrder = 21
    OnClick = btnPrevFrameCurrentClick
  end
  object btnKeyframeCurrent: TButton
    Left = 408
    Top = 424
    Width = 89
    Height = 17
    Caption = 'Use Current'
    TabOrder = 22
    OnClick = btnKeyframeCurrentClick
  end
  object cmbSoundEffect: TComboBox
    Left = 280
    Top = 488
    Width = 217
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 23
    OnChange = cmbSoundEffectChange
    Items.Strings = (
      'None'
      'Rope Trap'
      'Squasher Trap'
      'Ten Ton Squasher'
      'Bear Trap'
      'Zapper Trap'
      'Fire Trap'
      'Vacuum'
      'Slurp'
      'Screech'
      'Custom Sound 1'
      'Custom Sound 2'
      'Custom Sound 3'
      'Custom Sound 4'
      'Custom Sound 5'
      'Custom Sound 6'
      'Custom Sound 7'
      'Custom Sound 8'
      'Custom Sound 9'
      'Custom Sound 10')
  end
  object imgPiece: TImage32
    Left = 560
    Top = 24
    Width = 201
    Height = 201
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baTopLeft
    Color = clBtnFace
    ParentColor = False
    Scale = 1.000000000000000000
    ScaleMode = smScale
    TabOrder = 24
  end
  object cmbKeyColors: TComboBox
    Left = 568
    Top = 440
    Width = 201
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 25
    Text = 'Mask / Bridge Color'
    OnChange = cmbKeyColorsChange
    Items.Strings = (
      'Mask / Bridge Color'
      'Minimap Color'
      'Background Color'
      'Color 3 (Unused)'
      'Color 4 (Unused)'
      'Color 5 (Unused)'
      'Color 6 (Unused)'
      'Color 7 (Unused)')
  end
  object imgKeyColor: TImage32
    Left = 744
    Top = 464
    Width = 25
    Height = 21
    Bitmap.ResamplerClassName = 'TNearestResampler'
    BitmapAlign = baTopLeft
    Color = clMaroon
    ParentColor = False
    Scale = 1.000000000000000000
    ScaleMode = smNormal
    TabOrder = 26
  end
  object ebKCR: TEdit
    Tag = 16
    Left = 584
    Top = 464
    Width = 41
    Height = 21
    TabOrder = 27
    Text = '0'
    OnChange = ebKCxChange
  end
  object ebKCG: TEdit
    Tag = 8
    Left = 640
    Top = 464
    Width = 41
    Height = 21
    TabOrder = 28
    Text = '0'
    OnChange = ebKCxChange
  end
  object ebKCB: TEdit
    Left = 696
    Top = 464
    Width = 41
    Height = 21
    TabOrder = 29
    Text = '0'
    OnChange = ebKCxChange
  end
  object cbBlackBg: TCheckBox
    Left = 552
    Top = 328
    Width = 113
    Height = 17
    Caption = 'Black Background'
    Checked = True
    State = cbChecked
    TabOrder = 30
    OnClick = cbBlackBgClick
  end
  object btnInsTerrain: TButton
    Left = 68
    Top = 240
    Width = 49
    Height = 17
    Caption = 'Insert'
    TabOrder = 31
    OnClick = btnInsTerrainClick
  end
  object btnInsertObject: TButton
    Left = 68
    Top = 496
    Width = 49
    Height = 17
    Caption = 'Insert'
    TabOrder = 32
    OnClick = btnInsertObjectClick
  end
  object btnInsFrame: TButton
    Left = 664
    Top = 232
    Width = 41
    Height = 17
    Caption = 'Insert'
    TabOrder = 33
    OnClick = btnInsFrameClick
  end
  object btnTerUp: TButton
    Left = 184
    Top = 104
    Width = 41
    Height = 25
    Caption = 'Up'
    TabOrder = 34
    OnClick = btnTerUpClick
  end
  object btnTerDown: TButton
    Left = 184
    Top = 136
    Width = 41
    Height = 25
    Caption = 'Down'
    TabOrder = 35
    OnClick = btnTerDownClick
  end
  object btnObjUp: TButton
    Left = 184
    Top = 360
    Width = 41
    Height = 25
    Caption = 'Up'
    TabOrder = 36
    OnClick = btnObjUpClick
  end
  object btnObjDown: TButton
    Left = 184
    Top = 392
    Width = 41
    Height = 25
    Caption = 'Down'
    TabOrder = 37
    OnClick = btnObjDownClick
  end
  object cbZoomImage: TCheckBox
    Left = 552
    Top = 352
    Width = 113
    Height = 17
    Caption = 'Zoom Small Images'
    Checked = True
    State = cbChecked
    TabOrder = 38
    OnClick = cbBlackBgClick
  end
  object btnImportStrip: TButton
    Left = 552
    Top = 280
    Width = 105
    Height = 17
    Caption = 'Import Strip'
    TabOrder = 39
    OnClick = btnImportStripClick
  end
  object btnExportStrip: TButton
    Left = 664
    Top = 280
    Width = 105
    Height = 17
    Caption = 'Export Strip'
    TabOrder = 40
    OnClick = btnExportStripClick
  end
  object cbRandomFrame: TCheckBox
    Left = 304
    Top = 448
    Width = 193
    Height = 17
    Caption = 'Pseudorandom Start Frame'
    TabOrder = 41
    OnClick = cbRandomFrameClick
  end
  object btnAnimPreview: TButton
    Left = 680
    Top = 304
    Width = 89
    Height = 25
    Caption = 'Preview'
    TabOrder = 42
    OnClick = btnAnimPreviewClick
  end
  object ebPieceName: TEdit
    Left = 240
    Top = 32
    Width = 169
    Height = 21
    TabOrder = 43
    OnChange = ebPieceNameChange
  end
  object cbResizeHorz: TCheckBox
    Left = 296
    Top = 224
    Width = 97
    Height = 17
    Caption = 'Horizontal'
    TabOrder = 44
    Visible = False
    OnClick = cbResizeHorzClick
  end
  object cbResizeVert: TCheckBox
    Left = 296
    Top = 240
    Width = 97
    Height = 17
    Caption = 'Vertical'
    TabOrder = 45
    Visible = False
    OnClick = cbResizeVertClick
  end
  object cbLemmingSprites: TComboBox
    Left = 600
    Top = 397
    Width = 145
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 46
    Text = 'default'
    OnChange = cbLemmingSpritesChange
    OnExit = cbLemmingSpritesExit
    Items.Strings = (
      'default'
      'xmas')
  end
  object MainMenu1: TMainMenu
    object File1: TMenuItem
      Caption = '&File'
      object New1: TMenuItem
        Caption = '&New'
        OnClick = New1Click
      end
      object Load1: TMenuItem
        Caption = '&Load...'
        OnClick = Load1Click
      end
      object Save1: TMenuItem
        Caption = 'Save'
        OnClick = Save1Click
      end
      object SaveAs1: TMenuItem
        Caption = '&Save As...'
        OnClick = SaveAs1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = '&Exit'
        OnClick = Exit1Click
      end
    end
    object ools2: TMenuItem
      Caption = 'Tools'
      object CustomSoundManager1: TMenuItem
        Caption = 'Custom Sound Manager'
        OnClick = CustomSoundManager1Click
      end
      object MassStuffDoer1: TMenuItem
        Caption = 'Mass Convert'
        Visible = False
        OnClick = MassStuffDoer1Click
      end
      object MassRename1: TMenuItem
        Caption = 'Mass Rename'
        OnClick = MassRename1Click
      end
    end
    object ools1: TMenuItem
      Caption = '&Import'
      object ImportFromOldFormat1: TMenuItem
        Caption = 'Ground / VGAGR...'
        OnClick = ImportFromOldFormat1Click
      end
      object VGASPEC1: TMenuItem
        Caption = 'DOS VGASPEC...'
        OnClick = VGASPEC1Click
      end
      object SuperLemmini1: TMenuItem
        Caption = '(Super)Lemmini...'
        OnClick = SuperLemmini1Click
      end
      object LemSet2: TMenuItem
        Caption = 'INI / PNG or LemSet...'
        OnClick = LemSet2Click
      end
      object CheapoSTYfile1: TMenuItem
        Caption = 'Cheapo...'
        OnClick = CheapoSTYfile1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object StyleNames1: TMenuItem
        Caption = 'Style Names...'
        OnClick = StyleNames1Click
      end
    end
    object Export1: TMenuItem
      Caption = 'Export'
      object Full1: TMenuItem
        Caption = 'INI / PNG Collection'
        GroupIndex = 1
        OnClick = Full1Click
      end
      object ExperimentalFormat1: TMenuItem
        Caption = 'Experimental Format'
        GroupIndex = 1
        Visible = False
        OnClick = ExperimentalFormat1Click
      end
      object ExperimentalVGASPEC1: TMenuItem
        Caption = 'Experimental (VGASPEC)'
        GroupIndex = 1
        Visible = False
        OnClick = ExperimentalVGASPEC1Click
      end
    end
    object Help1: TMenuItem
      Caption = 'Help'
      object About1: TMenuItem
        Caption = 'About...'
        OnClick = About1Click
      end
    end
  end
end
